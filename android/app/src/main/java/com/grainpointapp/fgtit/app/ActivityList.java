package com.grainpointapp.fgtit.app;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import java.util.LinkedList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

//public class ActivityList extends Application{
public class ActivityList {
	private List<AppCompatActivity> activityList = new LinkedList<AppCompatActivity>();
	private static ActivityList instance;
	private Context pcontext;
	private Context ccontext;

	public boolean IsUseNFC = true;
	public boolean IsPad = false;
	public String PassWord = "";
	public String DeviceSN = "";
	public String WebAddr = "";
	public String UpdateUrl = "";
	public String WebService = "";
	public boolean IsOnline = false;
	public boolean IsUpImage = true;
	public float MapZoom = 18.0f;
	public double MapLat = 0.0;
	public double MapLng = 0.0;

	private ActivityList() {
	}

	public static ActivityList getInstance() {
		if (null == instance) {
			instance = new ActivityList();
		}
		return instance;
	}

	public void setMainContext(Context context) {
		pcontext = context;
	}

	public void setCurrContext(Context context) {
		ccontext = context;
	}

	public void Relogon() {
		for (AppCompatActivity activity : activityList) {
			activity.finish();
		}
	}

	public Context getCurrContext() {
		return ccontext;
	}

	public void addActivity(AppCompatActivity activity) {
		ccontext = activity;
		activityList.add(activity);
	}

	public void removeActivity(AppCompatActivity activity) {
		activityList.remove(activity);
	}

	public void exit() {
		for (AppCompatActivity activity : activityList) {
			activity.finish();
		}
		System.exit(0);
	}

	public void SetConfigByVal(String name, String val) {
		SharedPreferences sp;
		sp = PreferenceManager.getDefaultSharedPreferences(pcontext);
		Editor edit = sp.edit();
		edit.putString(name, val);
		edit.commit();
	}

	public String GetConfigByVal(String name) {
		SharedPreferences sp;
		sp = PreferenceManager.getDefaultSharedPreferences(pcontext);
		return sp.getString(name, "");
	}

	public void SetConfigByVal(String name, float val) {
		SharedPreferences sp;
		sp = PreferenceManager.getDefaultSharedPreferences(pcontext);
		Editor edit = sp.edit();
		edit.putFloat(name, val);
		edit.commit();
	}

	public void SaveConfig() {
		SharedPreferences sp;
		sp = PreferenceManager.getDefaultSharedPreferences(pcontext);
		Editor edit = sp.edit();
		edit.putString("WebAddr", WebAddr);
		edit.putString("UpdateUrl", UpdateUrl);
		edit.putString("PassWord", PassWord);
		edit.putBoolean("IsOnline", IsOnline);
		//edit.putBoolean("IsUpImage", IsUpImage);
		edit.putFloat("MapZoom", MapZoom);
		edit.putString("MapLat", String.valueOf(MapLat));
		edit.putString("MapLng", String.valueOf(MapLng));
		edit.commit();
	}

	public void LoadConfig() {
		SharedPreferences sp;
		sp = PreferenceManager.getDefaultSharedPreferences(pcontext);
		WebAddr = sp.getString("WebAddr", "http://www.biofgt.con/OnePass/");
		UpdateUrl = WebAddr + "apk/update.xml";
		WebService = WebAddr + "OnePassService.asmx";
		PassWord = sp.getString("PassWord", "1010");
		IsOnline = sp.getBoolean("IsOnline", false);
		//IsUpImage=sp.getBoolean("IsUpImage", false);
		MapZoom = sp.getFloat("MapZoom", 18.0f);
		MapLat = Double.parseDouble(sp.getString("MapLat", "22.653393"));
		MapLng = Double.parseDouble(sp.getString("MapLng", "114.057853"));

		if (ActivityCompat.checkSelfPermission(getCurrContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		DeviceSN = ((TelephonyManager) pcontext.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
	}
}
