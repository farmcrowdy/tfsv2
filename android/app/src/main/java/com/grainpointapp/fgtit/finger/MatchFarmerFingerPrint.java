package com.grainpointapp.fgtit.finger;//package com.platformpratice.fgtit.finger;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Base64;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.farmcrowdy.farmcrowdydata.R;
//import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
//import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
//import com.fgtit.device.Constants;
//import com.fgtit.device.FPModule;
//import com.fgtit.fpcore.FPMatch;
//import com.raizlabs.android.dbflow.sql.language.SQLite;
//
//import java.io.ByteArrayOutputStream;
//
//import androidx.appcompat.app.AlertDialog;
//import androidx.appcompat.app.AppCompatActivity;
//
//public class MatchFarmerFingerPrint extends AppCompatActivity {
//
//    String dm="empty";
//    String em = "empty";
//
//    private FPModule fpm ;
//    private EditText editText1, editText2, editText6, editText7, editText8, editText9, editText10,editText11;
//    private ImageView imgPhoto, imgFinger1, imgFinger2;
//
//    private byte[] jpgbytes = null;
//
//    private byte refdata[] = new byte[Constants.TEMPLATESIZE * 2];
//    private int refsize = 0;
//
//    private byte bmpdata[] = new byte[Constants.RESBMP_SIZE];
//    private int bmpsize = 0;
//
//    private byte[] model1 = new byte[512];
//    private byte[] model2 = new byte[512];
//
//    private ImageView fpImage;
//    private TextView tvFpStatus;
//    private Dialog fpDialog = null;
//    private int iFinger = 0;
//    //Barcode
//    private byte[] databuf = new byte[1024];
//
//    private boolean bIsCancel = false;
//    private boolean bCapture = false;
//    private boolean isenrol1;
//    private boolean isenrol2;
//    private TextView text1;
//    private TextView text2;
//    private TextView text3;
//    String farmerId;
//    StoreProfileOffline storeProfileOffline;
//
//    String farmerStoredPrint = "";
//
//    @SuppressLint("NewApi")
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_enroll);
//
//        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        farmerId = getIntent().getExtras().getString("data");
//        storeProfileOffline = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.farmerId.is(farmerId)).querySingle();
//
//        imgFinger2 = (ImageView) findViewById(R.id.imageView3);
//        imgFinger2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                FPDialog(1);
//            }
//        });
//        farmerStoredPrint = getIntent().getExtras().getString("fprint1");
//
//
//
//
//        //FP
//        fpm = new FPModule();
//        fpm.InitMatch();
//        fpm.SetTimeOut(Constants.TIMEOUT_LONG);
//        fpm.SetLastCheckLift(false);
//        FPDialog(1);
//
//    }
//
//    @SuppressLint("HandlerLeak")
//    private Handler mHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case Constants.FPM_DEVICE:
//                    switch (msg.arg1) {
//                        case Constants.DEV_OK:
//                            tvFpStatus.setText("Open Device OK");
//                            fpm.GenerateTemplate(2);
//                            break;
//                        case Constants.DEV_FAIL:
//                            tvFpStatus.setText("Open Device Fail");
//                            break;
//                        case Constants.DEV_ATTACHED:
//                            tvFpStatus.setText("USB Device Attached");
//                            break;
//                        case Constants.DEV_DETACHED:
//                            tvFpStatus.setText("USB Device Detached");
//                            break;
//                        case Constants.DEV_CLOSE:
//                            tvFpStatus.setText("Device Close");
//                            break;
//                    }
//                    break;
//                case Constants.FPM_PLACE:
//                    tvFpStatus.setText("Place Left thumb");
//                    break;
//                case Constants.FPM_LIFT:
//                    tvFpStatus.setText("Lift your thumb");
//                    break;
//                case Constants.FPM_GENCHAR: {
//                    if (msg.arg1 == 1) {
//                        tvFpStatus.setText("Enrol Template OK");
//                        refsize = fpm.GetTemplateByGen(refdata);
//                        //TODO Insert data into the database
//
//                        if (iFinger == 1) {
//                            Log.d("enrol_version","Enrolling version: 1");
//
//                            //String init = decidePojo.getFileLink();
//                            byte[] sks = Base64.decode(farmerStoredPrint, 1);
//                            int score = FPMatch.getInstance().MatchTemplate(sks, refdata);
//                            Intent intent = new Intent();
//                            intent.putExtra("score", (score>35) ? 200 : 400);
//                            setResult(Activity.RESULT_OK, intent);
//                            finish();
//
//
//                        } else {
//                            iFinger = 1;
//                            Log.d("enrol_version","Enrolling version: 2");
//                            em = Base64.encodeToString(refdata, 1);
//                            Log.d("emis: ", em);
//                            System.arraycopy(refdata, 0, MatchFarmerFingerPrint.this.model2, 0, 512);
//                            isenrol2 = true;
//                        }
//                        tvFpStatus.setText(getString(R.string.txt_fpenrolok));
//                        fpDialog.cancel();
//
//                    } else {
//                        tvFpStatus.setText("Generate Template Fail");
//                    }
//                }
//                break;
//                case Constants.FPM_NEWIMAGE: {
//                    bmpsize = fpm.GetBmpImage(bmpdata);
//                    Bitmap bm1 = BitmapFactory.decodeByteArray(bmpdata, 0, bmpsize);
//                    fpImage.setImageBitmap(bm1);
//                }
//                break;
//                case Constants.FPM_TIMEOUT:
//                    tvFpStatus.setText("Time Out");
//                    break;
//            }
//        }
//    };
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (resultCode) {
//            case 1: {
//                bCapture = false;
//                Bundle bl = data.getExtras();
//                String barcode = bl.getString("barcode");
//                editText9.setText(barcode);
//            }
//            break;
//            case 2:
//                break;
//            case 3: {
//                bCapture = false;
//                Bundle bl = data.getExtras();
//                String id = bl.getString("id");
//                Toast.makeText(MatchFarmerFingerPrint.this, "Pictures Finish", Toast.LENGTH_SHORT).show();
//                byte[] photo = bl.getByteArray("photo");
//                if (photo != null) {
//                    try {
//                        Matrix matrix = new Matrix();
//                        Bitmap bm = BitmapFactory.decodeByteArray(photo, 0, photo.length);
//                        matrix.preRotate(-90);
//                        Bitmap nbm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(),
//                                matrix, true);
//
//
//                        ByteArrayOutputStream out = new ByteArrayOutputStream();
//                        nbm.compress(Bitmap.CompressFormat.JPEG, 80, out);
//                        jpgbytes = out.toByteArray();
//
//                        Bitmap bitmap = BitmapFactory.decodeByteArray(jpgbytes, 0, jpgbytes.length);
//                        imgPhoto.setImageBitmap(bitmap);
//                    } catch (Exception e) {
//                    }
//                }
//            }
//            break;
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }
//
//
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Back");
//            builder.setMessage("Data not save, back?");
//            //builder.setCancelable(false);
//            builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//            builder.setNegativeButton("Back", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                    finish();
//                }
//            });
//            builder.create().show();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//
//
//    private void FPDialog(int i) {
//       iFinger = i;
//        AlertDialog.Builder builder = new AlertDialog.Builder(MatchFarmerFingerPrint.this);
//        builder.setTitle("Registration fingerprint");
//        final LayoutInflater inflater = LayoutInflater.from(MatchFarmerFingerPrint.this);
//        View vl = inflater.inflate(R.layout.dialog_enrolfinger, null);
//        fpImage = (ImageView) vl.findViewById(R.id.imageView1);
//        tvFpStatus = (TextView) vl.findViewById(R.id.textview1);
//        builder.setView(vl);
//        builder.setCancelable(false);
//        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                //SerialPortManager.getInstance().closeSerialPort();
//                dialog.dismiss();
//            }
//        });
//        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                //SerialPortManager.getInstance().closeSerialPort();
//                dialog.dismiss();
//            }
//        });
//
//        fpDialog = builder.create();
//        fpDialog.setCanceledOnTouchOutside(false);
//        fpDialog.show();
//
//        fpm.SetContextHandler(this, mHandler);
//        fpm.ResumeRegister();
//        fpm.OpenDevice();
//    }
//
//
//
//
//
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        fpm.PauseUnRegister();
//        fpm.CloseDevice();
//    }
//}
