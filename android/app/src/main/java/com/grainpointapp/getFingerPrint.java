//package com.grainpointapp;
//
//import static android.content.Context.BATTERY_SERVICE;
//import android.annotation.SuppressLint;
//import android.app.Dialog;
//import android.content.ContextWrapper;
//import android.content.DialogInterface;
//import android.content.DialogInterface.OnCancelListener;
//import android.content.DialogInterface.OnClickListener;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//import android.os.BatteryManager;
//import android.os.Build;
//import android.os.Build.VERSION;
//import android.os.Build.VERSION_CODES;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Base64;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.grainpointapp.fgtit.device.Constants;
//import com.grainpointapp.fgtit.device.FPModule;
//
//import java.io.ByteArrayOutputStream;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.AlertDialog;
//import androidx.appcompat.app.AlertDialog.Builder;
//import io.flutter.embedding.android.FlutterActivity;
//import io.flutter.embedding.engine.FlutterEngine;
//import io.flutter.plugin.common.MethodChannel;
//import io.flutter.plugins.GeneratedPluginRegistrant;
//
//import static android.content.Context.BATTERY_SERVICE;
//
//
//public class getFingerPrint extends FlutterActivity {
//    private static final String CHANNEL = "samples.flutter.dev/battery";
//
//    String dm="empty";
//    String em = "empty";
//
//    private FPModule fpm ;
//    private EditText editText1, editText2, editText6, editText7, editText8, editText9, editText10,editText11;
//    private ImageView imgPhoto, imgFinger1, imgFinger2;
//
//    private byte[] jpgbytes = null;
//
//    private byte refdata[] = new byte[Constants.TEMPLATESIZE * 2];
//    private int refsize = 0;
//
//    private byte bmpdata[] = new byte[Constants.RESBMP_SIZE];
//    private int bmpsize = 0;
//
//    private byte[] model1 = new byte[512];
//    private byte[] model2 = new byte[512];
//
//    private ImageView fpImage;
//    private TextView tvFpStatus;
//    private Dialog fpDialog = null;
//    private int iFinger = 0;
//    //Barcode
//    private byte[] databuf = new byte[1024];
//
//    private boolean bIsCancel = false;
//    private boolean bCapture = false;
//    private boolean isenrol1;
//    private boolean isenrol2;
//    private TextView text1;
//    private TextView text2;
//    private TextView text3;
//    String farmerId;
//
//    @Override
//    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
//
//        GeneratedPluginRegistrant.registerWith(flutterEngine);
//
//
//
//        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
//                .setMethodCallHandler(
//                        (call, result) -> {
//
//
//                            fpm = new FPModule();
//                            fpm.InitMatch();
//                            fpm.SetTimeOut(Constants.TIMEOUT_LONG);
//                            fpm.SetLastCheckLift(false);
//
//                            if (call.method.equals("getBatteryLevel")) {
//                                  FPDialog(1);
//                                int batteryLevel = getBatteryLevel();
//
//                                if (batteryLevel != -1) {
//                                    result.success(batteryLevel);
//
//                                } else {
//                                    result.error("UNAVAILABLE", "Battery level not available.", null);
//                                }
//                            } else {
//                                result.notImplemented();
//                            }
//                        }
//                );
//    }
//
//    private int getBatteryLevel() {
//        int batteryLevel = -1;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            BatteryManager batteryManager = (BatteryManager) getSystemService(BATTERY_SERVICE);
//            batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
//        } else {
//            Intent intent = new ContextWrapper(getApplicationContext()).
//                    registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
//            batteryLevel = (intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) * 100) /
//                    intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
//        }
//
//        return batteryLevel;
//    }
//
//    @SuppressLint("HandlerLeak")
//    private Handler mHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case Constants.FPM_DEVICE:
//                    switch (msg.arg1) {
//                        case Constants.DEV_OK:
//                            tvFpStatus.setText("Open Device OK");
//                            fpm.GenerateTemplate(2);
//                            break;
//                        case Constants.DEV_FAIL:
//                            tvFpStatus.setText("Open Device Fail");
//                            break;
//                        case Constants.DEV_ATTACHED:
//                            tvFpStatus.setText("USB Device Attached");
//                            break;
//                        case Constants.DEV_DETACHED:
//                            tvFpStatus.setText("USB Device Detached");
//                            break;
//                        case Constants.DEV_CLOSE:
//                            tvFpStatus.setText("Device Close");
//                            break;
//                    }
//                    break;
//                case Constants.FPM_PLACE:
//                    tvFpStatus.setText("Place Finger");
//                    break;
//                case Constants.FPM_LIFT:
//                    tvFpStatus.setText("Lift Finger");
//                    break;
//                case Constants.FPM_GENCHAR: {
//                    if (msg.arg1 == 1) {
//                        tvFpStatus.setText("Enrol Template OK");
//                        refsize = fpm.GetTemplateByGen(refdata);
//                        //TODO Insert data into the database
//
//                        /*
//                        for (int i = 0; i < GlobalData.getInstance().userList.size(); i++) {
//                            if (GlobalData.getInstance().userList.get(i).bytes1 != null) {
//                                if (FPMatch.getInstance().MatchTemplate(refdata, GlobalData.getInstance().userList.get(i).bytes1) > 60) { tvFpStatus.setText(getString(R.string.txt_fpduplicate));
//                                    return;
//                                }
//                            }
//                            if (GlobalData.getInstance().userList.get(i).bytes2 != null) {
//                                if (FPMatch.getInstance().MatchTemplate(refdata, GlobalData.getInstance().userList.get(i).bytes2) > 60) {
//                                    tvFpStatus.setText(getString(R.string.txt_fpduplicate));
//                                    return;
//                                }
//                            }
//                        }
//                        */
//
//                        if (iFinger == 1) {
//                            dm = Base64.encodeToString(refdata, 1);
//                            System.arraycopy(refdata, 0, getFingerPrint.this.model1, 0, 512);
//
////                            storeProfileOffline.setF_print(dm);
////                            storeProfileOffline.update();
//
//                            Toast.makeText(getFingerPrint.this, "Fingerprint taken successfully", Toast.LENGTH_SHORT).show();
//                            Toast.makeText(getFingerPrint.this, dm, Toast.LENGTH_SHORT).show();
//                            // startActivity(new Intent(MainActivity.this, MainActivity.class));
//                            //finishAffinity();
//                        } else {
//                            em = Base64.encodeToString(refdata, 1);
//                            Log.d("emis: ", em);
//                            System.arraycopy(refdata, 0, getFingerPrint.this.model2, 0, 512);
//                            isenrol2 = true;
//                        }
//                        // tvFpStatus.setText(getString(R.string.txt_fpenrolok));
//                        fpDialog.cancel();
//
//                    } else {
//                        tvFpStatus.setText("Generate Template Fail");
//                    }
//                }
//                break;
//                case Constants.FPM_NEWIMAGE: {
//                    bmpsize = fpm.GetBmpImage(bmpdata);
//                    Bitmap bm1 = BitmapFactory.decodeByteArray(bmpdata, 0, bmpsize);
//                    fpImage.setImageBitmap(bm1);
//                }
//                break;
//                case Constants.FPM_TIMEOUT:
//                    tvFpStatus.setText("Time Out");
//                    break;
//            }
//        }
//    };
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (resultCode) {
//            case 1: {
//                bCapture = false;
//                Bundle bl = data.getExtras();
//                String barcode = bl.getString("barcode");
//                editText9.setText(barcode);
//            }
//            break;
//            case 2:
//                break;
//            case 3: {
//                bCapture = false;
//                Bundle bl = data.getExtras();
//                String id = bl.getString("id");
//                Toast.makeText(getFingerPrint.this, "Pictures Finish", Toast.LENGTH_SHORT).show();
//                byte[] photo = bl.getByteArray("photo");
//                if (photo != null) {
//                    try {
//                        Matrix matrix = new Matrix();
//                        Bitmap bm = BitmapFactory.decodeByteArray(photo, 0, photo.length);
//                        matrix.preRotate(-90);
//                        Bitmap nbm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(),
//                                matrix, true);
//
//
//                        ByteArrayOutputStream out = new ByteArrayOutputStream();
//                        nbm.compress(Bitmap.CompressFormat.JPEG, 80, out);
//                        jpgbytes = out.toByteArray();
//
//                        Bitmap bitmap = BitmapFactory.decodeByteArray(jpgbytes, 0, jpgbytes.length);
//                        imgPhoto.setImageBitmap(bitmap);
//                    } catch (Exception e) {
//                    }
//                }
//            }
//            break;
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }
//
//
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
//            AlertDialog.Builder builder = new Builder(this);
//            builder.setTitle("Back");
//            builder.setMessage("Data not save, back?");
//            //builder.setCancelable(false);
//            builder.setPositiveButton("Cancel", new OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//            builder.setNegativeButton("Back", new OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                    finish();
//                }
//            });
//            builder.create().show();
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//
//
//    private void FPDialog(int i) {
//        iFinger = i;
//        AlertDialog.Builder builder = new Builder(getFingerPrint.this);
//        builder.setTitle("Registration fingerprint");
//        final LayoutInflater inflater = LayoutInflater.from(getFingerPrint.this);
//        View vl = inflater.inflate(R.layout.dialog_enrolfinger, null);
//        fpImage = (ImageView) vl.findViewById(R.id.imageView1);
//        tvFpStatus = (TextView) vl.findViewById(R.id.textview1);
//        builder.setView(vl);
//        builder.setCancelable(false);
//        builder.setNegativeButton("Cancel", new OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                //SerialPortManager.getInstance().closeSerialPort();
//                dialog.dismiss();
//            }
//        });
//        builder.setOnCancelListener(new OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                //SerialPortManager.getInstance().closeSerialPort();
//                dialog.dismiss();
//            }
//        });
//
//        fpDialog = builder.create();
//        fpDialog.setCanceledOnTouchOutside(false);
//        fpDialog.show();
//
//        fpm.SetContextHandler(this, mHandler);
//        fpm.ResumeRegister();
//        fpm.OpenDevice();
//    }
//
//
//
//
//
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        fpm.PauseUnRegister();
//        fpm.CloseDevice();
//    }
//}
