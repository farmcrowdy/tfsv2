import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/const.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

class ApproveHarvest extends StatefulWidget {
  final String produceId;

  const ApproveHarvest({Key key, this.produceId}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState(produceId);
}

class _MyAppState extends State<ApproveHarvest> {
  TextEditingController reasonController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String firstName;
  String lg;
  String farmerId;
  String produceId;
  String unitPrice;
  var locations = List();
  Map<String, dynamic> responseData;
  String message;
  var data;
  var token;
  bool passwordVisible;
  bool passwordVisible2;
  String name;
  String status;
  String approved;
  String accessLevel;
  var body;
  int total;
  String cropId;
  bool _isLoading = false;
  bool _isLoading2 = false;
  String produceName;
  String produceQuantity;
  String totalCost;
  String moistureContent;
  String farmerConsented;
  String date;
  String farmerDetails;
  String farmerPhone;
  String txCode;
  String submitReason;
  String payDecline;
  String inspectorId;
  String reason = 'Did not meet the moisture content requirement';
  final oCcy = new NumberFormat("#,##0.00", "en_US");

  _MyAppState(this.produceId);
  void _handlePickupChange(String value) {
    setState(() {
      reason = value;
      Navigator.pop(context);
      _declineDialog();

    });
  }
  _confirmClientProfile() async {
    print(produceId);
    body = {
      "tx_code": '$produceId',
    };

    final response = await http.post(
      baseUrl + 'harvest_details_by_tx_code',
      headers: {'api-key': 'aa47f8215c6f30a0dcdb2a36a9f4168e' , },body:body,
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        print(responseData);
        {
           produceId = (responseData['message']['produce_id']);
           produceQuantity = (responseData['message']['quantity']);
           totalCost = (responseData['message']['amount']);
           moistureContent = (responseData['message']['moisture_level']);
           farmerConsented= (responseData['message']['confirmed']);
           date = (responseData['message']['date_created']);
           farmerDetails= (responseData['message']['farmer_name']);
           farmerPhone= (responseData['message']['farmer_phone']);
           txCode = (responseData['message']['tx_code']);
           payDecline = (responseData['message']['pay_declined']);
           inspectorId  = (responseData['message']['inspector_id']);
           _produceName();

           if(payDecline == '1'){
             Fluttertoast.showToast(msg: 'Produce already declined');
             Navigator.pop(context);
           }
        }
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      Fluttertoast.showToast(msg: er);
      Navigator.pop(context);
      throw Exception('Failed to load internet');
    }
  }
  _produceName() async {
    print(produceId);

    final response = await http.get(
      baseUrl + 'produce_details?produce_id=$produceId',
      headers: {'api-key': 'aa47f8215c6f30a0dcdb2a36a9f4168e'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        print(responseData);
        {
          produceName = (responseData['data']['name']);

        }

      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      Fluttertoast.showToast(msg: er);
      Navigator.pop(context);
      throw Exception('Failed to load internet');
    }
  }

  _declineDialog(
      {String message =
      "Both username and password fields are required.", String where}) async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              backgroundColor: appTheme,
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Why do you want to decline produce?',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),
                    FittedBox(
                      child:
                      Row(

                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                           Radio(
                                value: 'Did not meet the moisture content requirement',
                                activeColor: Colors.white,
                                groupValue: reason,
                                onChanged: _handlePickupChange,
                              ),

                            Text(
                              "Did not meet the moisture content requirement",
                              //textAlign: TextAlign.center,
                            ),
                          ]
                        ),
                    ),
                        FittedBox(
                          child:
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                          Radio(
                                value: 'Did not meet the pre-registered information',
                                activeColor:  Colors.white,
                                groupValue: reason,
                                onChanged: _handlePickupChange,
                              ),

                            Text(
                              "Did not meet the pre-registered information",
                            ),
                          ],
                        ),
                        ),
                    FittedBox(
                      child:
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Radio(
                              value: 'Others',
                              activeColor:  Colors.white,
                              groupValue: reason,
                              onChanged: _handlePickupChange,
                            ),

                            Text(
                              "Others (type your reason)",
                            ),
                          ],
                        ),
                    ),
                        reason == 'Others'?
                        Container(
                          padding: EdgeInsets.all(5),
                          child: TextFormField(
                            controller: reasonController,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              hintText: '',
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              labelStyle: TextStyle(color: Colors.grey),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            style: TextStyle(color: Colors.black),
                            cursorColor: Colors.grey,
                          ),
                        ):Container(),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: 300,
                          height: 60,
                          child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              elevation: 5.0,
                              child: MaterialButton(
                                padding: EdgeInsets.all(5),
                                onPressed: () {
                                  Navigator.pop(context);
                                 reason != null
                                      ? decline()
                                      : Fluttertoast.showToast(
                                      msg: 'Select a reason');
                                },
                                child: Text(
                                  'Decline',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 12.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }
  // Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error!"),
            content: Text(message),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  //Http call
  approve() async {

      body = {
        "amount":totalCost ,
        "destinationPhone": farmerPhone,
        "tx_code": txCode,
        "inspector_id": inspectorId,
      };
//      body = {
//        "sourcePhone": '234${phoneNumber.substring(1, 11)}',
//        "account_num": accountController.text,
//        "amount": amountController.text,
//        "destinationPhone": '234${phoneNumber.substring(1, 11)}',
//      };
      print(data);
      final response = await http.post(baseUrlFCMB + 'fcmb/Account2Wallet',
          headers: {'api-key': apiKey}, body: body);
      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
//      status = responseData["status"];
//      message = responseData["message"];
//      approved = responseData['approved'];
//      accessLevel = responseData['access_level'];
//      id = responseData['client_id'];
       // tx_code = responseData['data']['tx_code'];
//      await prefs.setString('name', name);
//      await prefs.setString('phoneNo', phoneNo);
//      await prefs.setString('userId', '$id');
//      await prefs.setString('email', '$email');
        // print('token $token and $name');
        print(responseData);
        print(body);
        print(status);
        setState(() {
          _isLoading = false;
        });
      } else {
        setState(() {
          _isLoading = false;
        });
        responseData = json.decode(response.body);
        var er = responseData['message']['error'];
        print('error$responseData');
        _showErrorDialog(message: er);
      }
    }

  //Http call
  decline() async {
if (reason == 'Others'){
  submitReason = reasonController.text;
}else{
  submitReason = reason;
}
print(submitReason);
    if (reason == null) {
      _showErrorDialog(message: 'Select a reason');
    } else {
      body = {
        "tx_code": txCode,
        "inspector_id": inspectorId,
        "reason": submitReason
      };
      print(data);
      final response = await http.post(baseUrl + 'decline_harvest_collected',
          headers: {'api-key': apiKey}, body: body);
      if (response.statusCode == 200) {
        responseData = json.decode(response.body);

        print(responseData);
        print(body);
        print(status);
        setState(() {
          _isLoading2 = false;
        });
      } else {
        setState(() {
          _isLoading2 = false;
        });
        responseData = json.decode(response.body);
        var er = responseData['message'];
        print('error$responseData');
        _showErrorDialog(message: er);
      }
    }
  }

  // ignore: must_call_super
  void initState() {
    // TODO: implement initState
    passwordVisible = true;
    _confirmClientProfile();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: false);

    if (produceName == null) {
      return SpinKitChasingDots(
        size: 50,
        color: appTheme,
      );
    }
    return Scaffold(
      body: Form(
          key: _formKey,
          autovalidate: true,
          child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              Container(
                padding: EdgeInsets.all(ScreenUtil().setWidth(30)),
                  child:Column(
                    children: [

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Produce name',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20 , color: greyish),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          produceName,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),

                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Produce Quantity',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20 , color: greyish),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          produceQuantity,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),

                      ],
                    ),


                  ],
                ),

                SizedBox(
                  height: 20,
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Total cost',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20 , color: greyish),
                        ),
                        SizedBox(height: 10,),
                        Text(
                        '\u{20A6}'+ oCcy.format(int.parse(totalCost)),
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                      ],
                    ),

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Moisture content',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20 , color: greyish),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          moistureContent,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),

                      ],
                    ),


                  ],
                ),

                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Farmer Consented',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20 , color: greyish),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          farmerConsented == "1" ? "Yes" : "No, not confirmed",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                      ],
                    ),

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Date',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20 , color: greyish),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          date,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),

                      ],
                    ),


                  ],
                ),


                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Farmer Name',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20 , color: greyish),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          farmerDetails,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                      ],
                    ),

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Farmer Phone',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20 , color: greyish),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          farmerPhone,
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                        ),

                      ],
                    ),


                  ],
                ),

                    ],
                  ),
              ),
                SizedBox(
                  height: 20,
                ),


                SizedBox(
                  height: 100,
                ),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 80,
                    width: deviceWidth,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        //side: BorderSide(color: Colors.red)
                      ),
                      color: appTheme,
                      onPressed: () async {

                          setState(() {
                            _isLoading2 = true;
                          });
                          await _declineDialog();

                      },
                      child: _isLoading2
                          ? Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                          : Text(
                              'Decline',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                    )),

                Container(
                    padding: EdgeInsets.all(10),
                    height: 80,
                    width: deviceWidth,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        //side: BorderSide(color: Colors.red)
                      ),
                      color: appTheme,
                      onPressed: () async {

                          setState(() {
                            _isLoading = true;
                          });
                          await approve();

                      },
                      child: _isLoading
                          ? Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                      )
                          : Text(
                        'Approve',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    )),
              ]))),
    );
  }
}
