import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/const.dart';
import 'package:http/http.dart' as http;


class MarketPrice extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MarketPrice> {
  TextEditingController priceController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String gender = 'Male';
  String unitPrice;
  var crops = List();
  var state = List();
  var lg = List();
  var cropId;
  var stateId;
  var ldId;
  Map<String, dynamic> responseData;
  String message;
  var data;
  var body;
  bool _isLoading = false;

  // Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Submitting"),
            content: Text(message),
            shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }
  //Http call
  submit() async {
    if (stateId == null && ldId == null && cropId != null) {
      _showErrorDialog(message: 'All fields are compulsory');
    } else {
      body = {
        "crop_id": cropId,
        "state_id": stateId,
        "local_id": ldId,
        "price_per_unit": priceController.text,
        "user_id": id,

      };
      print(data);
      final response = await http.post(baseUrl + 'upload_field_price',
          headers: {'api-key': apiKey},
          body: body);
      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        print(responseData);
        print(body);
        setState(() {
          _isLoading = false;
        });
        Fluttertoast.showToast(
          msg: 'Field Price Added',
          toastLength: Toast.LENGTH_LONG,
        );
        Navigator.pop(context);
      } else {
        setState(() {
          _isLoading = false;
        });
        responseData = json.decode(response.body);
        var er = responseData['message']['error'];
        print('error$responseData');
        _showErrorDialog(message: er);
      }
    }
  }
  // ignore: missing_return
  Future<String> _crops() async {
    final response = await http.get(
        baseUrl + 'crops',
        headers: {
          'api-key': apiKey});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        crops = responseData["message"];
        print(crops);
      });

    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }
  // ignore: missing_return
  Future<String> _state() async {
    final response = await http.get(
        baseUrl + 'states',
        headers: {
          'api-key': apiKey});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        state = responseData["message"];
        print(state);
      });

    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }
  // ignore: missing_return
  Future<String> _localG() async {
    final response = await http.get(
        baseUrl + 'localgovt',
        headers: {
          'api-key': apiKey});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
       //exLg = responseData["message"];
       lg =responseData["message"];
       // print(lg);
       print('from the top ');
       print(lg);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _crops();
    _state();
    _localG();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        body:
        Form(
        key: _formKey,
        autovalidate: false,
        child:
        Container(
        padding: EdgeInsets.all(20),
    child:
    ListView(
            children: <Widget>[
              SizedBox(height: 10,),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [

                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back, size: 40,),
                    )
                  ],
                ),
              ),

              SizedBox(height: 30,),

              Text('Market Price', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
              SizedBox(height: 20,),
              Text('Enter detail below to share the market price.', style: TextStyle(color: greyish, fontSize: 20,fontWeight: FontWeight.w400),),
              SizedBox(height: 30,),

              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'CROP TO BE REPORTED',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15, ),
                      ),
                      SizedBox(height: 10,),

                      Container(
                        child:
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RichText(
                              text: TextSpan(
                                text: 'Item Name',
                                style: TextStyle(
                                    color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                              ),

                            ),
                            SizedBox(height: 20,),
                            Container(
                                padding: EdgeInsets.only(left: 10),
                                height: 60.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7.0),
                                    border: Border.all(color: Colors.grey)),
                                child:
                                Center(
                                  child:
                                  new DropdownButtonFormField(
                                    decoration: InputDecoration.collapsed(
                                        hintText: ''  ),
                                    isExpanded: true,
                                    items: crops.map((item) {
                                      return new DropdownMenuItem(
                                        child: new Text(item['crop_name']),
                                        value: item,
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      setState(() {
                                        cropId  = newVal['crop_id'];
                                        print(cropId);
                                      });
                                    },
                                    //value: unitPrice,
                                  ),
                                )
                            )
                          ],
                        ),
                      ),
                    ]
                ),
              ),
              SizedBox(height: 30,),

              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'MARKET PRICE FOR WHICH STATE',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15, ),
                      ),
                      SizedBox(height: 10,),

                      Container(
                        child:
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[


                            Container(
                                padding: EdgeInsets.only(left: 10),
                                height: 60.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7.0),
                                    border: Border.all(color: Colors.grey)),
                                child:
                                Center(
                                  child:
                                  new DropdownButtonFormField(
                                    decoration: InputDecoration.collapsed(
                                        hintText: ''  ),
                                    isExpanded: true,
                                    items: state.map((item) {
                                      return new DropdownMenuItem(
                                        child: new Text(item['name']),
                                        value: item,
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      setState(() {
                                        stateId = newVal['state_id'];
                                        print(stateId);
                                       // s = null;
                                        //lg.clear();
                                       // _localG();
                                        Timer(Duration(milliseconds: 500), () {
                                          setState(() {

                                          });
                                        });
                                      });
                                    },
                                    //value: unitPrice,
                                  ),
                                )
                            )
                          ],
                        ),
                      ),
                    ]
                ),
              ),
              SizedBox(height: 30,),

              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'LGA FOR THE CHOSEN STATE',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15, ),
                      ),
                      SizedBox(height: 10,),
                      Container(
                        child:
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[

                            Container(
                                padding: EdgeInsets.only(left: 10),
                                height: 60.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7.0),
                                    border: Border.all(color: Colors.grey)),
                                child:
                                Center(
                                  child:
                                  new DropdownButtonFormField(
                                    decoration: InputDecoration.collapsed(
                                        hintText: ''  ),
                                    isExpanded: true,
                                    items: lg.where((i) => i['state_id'] == stateId).map((item) {
                                      print('from the below');
                                      print(item);
                                      return new DropdownMenuItem(
                                        child: new Text(item['local_name']),
                                        value:item,
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      setState(() {
                                       ldId = newVal['local_id'];
print(ldId);
                                      });
                                    },

                                  ),
                                )
                            )
                          ],
                        ),
                      )
                    ]
                ),
              ),
              SizedBox(height: 30,),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'NEW PRICE PER KG',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15, ),
                    ),
                    TextFormField(
                      controller: priceController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Invalid Price";
                        }
                        return null;
                      },
                      // maxLength: 11,
                      keyboardType: TextInputType.phone,
                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
                      cursorColor: Colors.black,
                      maxLength: 11,
                      decoration: InputDecoration(
                        hintText: '1000',
                        hintStyle: TextStyle(color: greyish, fontWeight: FontWeight.bold)
                      ),
                    ),
                  ],
                ),
              ),
SizedBox(height: 100,),
              Container(
                  padding: EdgeInsets.all(10),
                  height: 80,
                  width: deviceWidth,
                  child:
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius
                          .circular(25.0),
                      //side: BorderSide(color: Colors.red)
                    ),
                    color: appTheme,
                    onPressed: ()async {
                      if (_formKey.currentState.validate()) {
                        setState(() {
                          _isLoading = true;
                        });
                        await submit();
                      }
                    },
                    child: _isLoading
                        ? Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      ),
                    )
                        :  Text('Send Market Price', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                  )
              ),
            ]
        )
        )
        ),
    );
  }
}
