import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/Input/verification.dart';
import 'package:grainpointapp/const.dart';
import 'package:http/http.dart' as http;

var batchNumber;
class DisburseInput extends StatefulWidget {

  final String farmerPhoneNo;

  const DisburseInput({Key key, this.farmerPhoneNo}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState(farmerPhoneNo);
}

class _MyAppState extends State<DisburseInput> {
  TextEditingController quantityController = TextEditingController();
  TextEditingController quantityController2 = TextEditingController();
  TextEditingController quantityController3 = TextEditingController();
  TextEditingController quantityController4 = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  String firstName;
  String lg;
  String farmerId;
  String farmerPhoneNo;
  String unitPrice;
  var inputs = List();
  var inputs2 = List();
  var inputs3 = List();
  var inputs4 = List();
  Map<String, dynamic> responseData;
  String message;
  var data;
  var token;
  bool passwordVisible;
  bool passwordVisible2;
  String name;
  String status;
  String approved;
  String accessLevel;
  var body;
  String quantity = '1';
  int total;
  String inputsDisbursed;
  String inputsDisbursed2;
  String inputsDisbursed3;
  String inputsDisbursed4;
  int tx_code;
  bool _isLoading = false;
  void _onSubmit(String value) {
    setState(() {
      print(value);
      quantity = value;
      if (_formKey.currentState.validate()) {
        cal();
      }
    });
  }

  _MyAppState(this.farmerPhoneNo);

  _confirmClientProfile() async {
    body = {
      "phone": '$farmerPhoneNo',
    };

    final response = await http.get(
      baseUrl + 'nfarmer_by_phone/$farmerPhoneNo',
      headers: {'api-key': 'aa47f8215c6f30a0dcdb2a36a9f4168e'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        print(responseData);
        firstName = (responseData['data']['fname']);
        farmerId = (responseData['data']['farmerId']);
        lg = (responseData['data']['govtString']);
        //phoneNo = (responseData['message']['phone']);
        print(firstName);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      Fluttertoast.showToast(msg: er);
      Navigator.pop(context);
    }
  }

  // ignore: missing_return
  Future<String> _inputs() async {
    final response =
    await http.get(baseUrl + 'input_types', headers: {'api-key': apiKey});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        inputs = responseData["data"];
        inputs2 = responseData["data"];
        inputs3 = responseData["data"];
        inputs4 = responseData["data"];
        print(inputs);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }

  // Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Submitting"),
            content: Text(message),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  //Http call
  submit() async {
    if (quantityController.text.isEmpty) {
      _showErrorDialog(message: 'All fields are compulsory');
    } else {
      body = {
        "farmer_phone": farmerPhoneNo,
        "quantity_array": inputsDisbursed+"-"+inputsDisbursed2+"-"+inputsDisbursed3+"-"+inputsDisbursed4,
        "input_type_id_array": quantityController.text+"-"+quantityController2.text+"-"+quantityController3.text+"-"+quantityController4.text,
        "agent_id": id,
      };
      print(body);
      final response = await http.post(baseUrl + 'nfarmer_batch_input',
          headers: {'api-key': apiKey}, body: body);
      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        batchNumber = responseData["data"]['batch_number'];
        toDisburse(context, phoneNo: farmerPhoneNo);
//     message = responseData["message"];
//      approved = responseData['approved'];
//      accessLevel = responseData['access_level'];
//      id = responseData['client_id'];
//      await prefs.setString('name', name);
//      await prefs.setString('phoneNo', phoneNo);
//      await prefs.setString('userId', '$id');
//      await prefs.setString('email', '$email');
        // print('token $token and $name');
        print(responseData);
        print(body);
        print(status);
        setState(() {
          _isLoading = false;
        });
      } else {
        setState(() {
          _isLoading = false;
        });
        responseData = json.decode(response.body);
        var er = responseData['message']['error'];
        print('error$responseData');
        _showErrorDialog(message: er);
      }
    }
  }

  // ignore: must_call_super
  void initState() {
    // TODO: implement initState
    passwordVisible = true;
    _confirmClientProfile();
    _inputs();
  }

  cal() {
    total = int.parse(unitPrice) * int.parse(quantity);
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    if (firstName == null) {
      return SpinKitChasingDots(
        size: 50,
        color: appTheme,
      );
    }
    return Scaffold(
      body: Form(
          key: _formKey,
          autovalidate: true,
          child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Disburse Input',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Enter detail below to collect harvest.',
                  style: TextStyle(
                      color: greyish,
                      fontSize: 20,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Text(
                      firstName,
                      style: TextStyle(fontSize: 15),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      farmerId,
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      lg,
                      style: TextStyle(fontSize: 15),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      farmerPhoneNo,
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          text: 'Input 1',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 10),
                          height: 60.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          child: Center(
                            child: new DropdownButtonFormField(
                              decoration:
                              InputDecoration.collapsed(hintText: ''),
                              isExpanded: true,
                              items: inputs.map((item) {
                                return new DropdownMenuItem(
                                  child: new Text(item['name']),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                setState(() {
                                  inputsDisbursed = newVal['id'];
                                  print(inputsDisbursed);
                                });
                              },
                              //value: unitPrice,
                            ),
                          ))
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Input Quantity (kg)',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: quantityController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Invalid Quantity";
                          }
                          return null;
                        },
                        onChanged: _onSubmit,
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: '23.5',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          text: 'Input 2',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 10),
                          height: 60.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          child: Center(
                            child: new DropdownButtonFormField(
                              decoration:
                              InputDecoration.collapsed(hintText: ''),
                              isExpanded: true,
                              items: inputs2.map((item) {
                                return new DropdownMenuItem(
                                  child: new Text(item['name']),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                setState(() {
                                  inputsDisbursed2 = newVal['id'];
                                  print(inputsDisbursed);
                                });
                              },
                              //value: unitPrice,
                            ),
                          ))
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Input Quantity (kg)',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: quantityController2,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Invalid Quantity";
                          }
                          return null;
                        },
                        onChanged: _onSubmit,
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: '23.5',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          text: 'Input 3',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 10),
                          height: 60.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          child: Center(
                            child: new DropdownButtonFormField(
                              decoration:
                              InputDecoration.collapsed(hintText: ''),
                              isExpanded: true,
                              items: inputs3.map((item) {
                                return new DropdownMenuItem(
                                  child: new Text(item['name']),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                setState(() {
                                  inputsDisbursed3 = newVal['id'];
                                  print(inputsDisbursed);
                                });
                              },
                              //value: unitPrice,
                            ),
                          ))
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Input Quantity (kg)',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: quantityController3,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Invalid Quantity";
                          }
                          return null;
                        },
                        onChanged: _onSubmit,
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: '23.5',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          text: 'Input 4',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 10),
                          height: 60.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          child: Center(
                            child: new DropdownButtonFormField(
                              decoration:
                              InputDecoration.collapsed(hintText: ''),
                              isExpanded: true,
                              items: inputs4.map((item) {
                                return new DropdownMenuItem(
                                  child: new Text(item['name']),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                setState(() {
                                  inputsDisbursed4 = newVal['id'];
                                  print(inputsDisbursed);
                                });
                              },
                              //value: unitPrice,
                            ),
                          ))
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Input Quantity (kg)',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: quantityController4,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Invalid Quantity";
                          }
                          return null;
                        },
                        onChanged: _onSubmit,
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: '23.5',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),


                SizedBox(
                  height: 10,
                ),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 80,
                    width: deviceWidth,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        //side: BorderSide(color: Colors.red)
                      ),
                      color: appTheme,
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            _isLoading = true;
                          });
                          await submit();
                        }
                      },
                      child: _isLoading
                          ? Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                      )
                          : Text(
                        'Record with OTP',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    )),
              ]))),
    );
  }
  toDisburse(BuildContext context, { String phoneNo,}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) =>
                DisburseOTP( phoneNo: phoneNo,)));
  }
}
