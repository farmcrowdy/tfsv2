import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/Input/collectHarvest.dart';
import 'package:grainpointapp/const.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

//Making User phone number general
String uPhoneNo;
String apiPhoneNo;
// ignore: must_be_immutable
class CollectOTP extends StatefulWidget {
 final String phoneNo;

 CollectOTP({Key key, this.phoneNo,}) : super(key: key);
  @override
  _VerifyPin createState() => _VerifyPin(phoneNo);
}

class _VerifyPin extends State<CollectOTP> {
  String phoneNo;


  _VerifyPin(this.phoneNo,);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    print(phoneNo);
    uPhoneNo = phoneNo;
    return Scaffold(
      body: PinCodeVerificationScreen(
          "$phoneNo"), // a random number, please don't call xD
    );
  }
}

class PinCodeVerificationScreen extends StatefulWidget {
  final String uPhoneNo;


  PinCodeVerificationScreen(this.uPhoneNo,);
  @override
  _PinCodeVerificationScreenState createState() =>
      _PinCodeVerificationScreenState();
}

class _PinCodeVerificationScreenState extends State<PinCodeVerificationScreen> {
  var onTapRecognizer;

  TextEditingController textEditingController = TextEditingController();

  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  Map<String, dynamic> responseData;
  String message;
  var data;
  var token;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  SharedPreferences prefs;
//



//Http for verification
  verify()  async {
    prefs = await SharedPreferences.getInstance();
    var body = {
      "otp": currentText.toUpperCase(),
      "tx_code": txCode.toString(),
      "confirmation_type": '3',
    };
    print(body);
    final response = await http.post(
        baseUrl+ 'confirm_harvest_collected',
        headers: {'api-key': 'aa47f8215c6f30a0dcdb2a36a9f4168e'},  body: body);
    if (response.statusCode == 200) {

     // Navigator.pushReplacementNamed(context, "/ResetPassField");
//      responseData = json.decode(response.body);
//      message = responseData["message"];
      print(response.body);
      print(data);
      print(message);
      Fluttertoast.showToast(msg: 'Successfully verified');
      Navigator.pushReplacementNamed(context, '/Home');
    }
    else {
      _isLoading = false;
      // var er = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      Fluttertoast.showToast(msg: er);
    }
  }

  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
      };

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;
    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;
    print(uPhoneNo);
    return Scaffold(
      key: scaffoldKey,
      body:
        ListView(
        children: <Widget>[
          Stack(
            alignment: Alignment.bottomLeft,
            children: <Widget>[
              Container(
                child:
                Image.asset(
                  'images/greenHeader.png',
                  width: deviceWidth,
                  height: deviceHeight/4,
                  fit: BoxFit.fill,
                ),
              ),
            ],
          ),
        
      GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },

          child: Column(
            children: <Widget>[
//              Container(
//                height: MediaQuery.of(context).size.height /15,
//                child: FlareActor(
//                  "images/otp.flr",
//                  animation: "otp",
//                  fit: BoxFit.fitHeight,
//                  alignment: Alignment.center,
//                ),
//              ),
//               Image.asset(
//                 'images/verify.png',
//                 height: MediaQuery.of(context).size.height / 15,
//                 fit: BoxFit.fitHeight,
//               ),
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'Verification Code',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  textAlign: TextAlign.center,
                ),
              ),

        Container(
        padding: EdgeInsets.all(30),
          child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Please enter the One-Time Pin (OTP)", style:
                    TextStyle(
                      fontSize: 16
                    ),
                  ),
                  Text(
                    "code sent to your phone ", style:
                  TextStyle(
                      fontSize: 16
                  ),
                  ),
                  Text(
                    "Not case sensitive", style:
                  TextStyle(
                      fontSize: 16
                  ),
                  ),
                  SizedBox(
                    height: 20,
                  ),


                ],
              ),
        ),
              InkWell(
                  onTap: (){
                  },
                  child:
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        widget.uPhoneNo, style:
                      TextStyle(
                          fontSize: 25, color: appTheme
                      ),
                      ),

                    ],
                  )
              ),

//              Padding(
//                padding:
//                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
//                child: RichText(
//                  text: TextSpan(
//                      text: "Please enter the One-Time Pin (OTP) code sent to your phone ",
//                      children: [
//                        TextSpan(
//                            text: widget.uPhoneNo,
//                            style: TextStyle(
//                                color: Colors.black,
//                                fontWeight: FontWeight.bold,
//                                fontSize: 16)),
//                      ],
//                      style: TextStyle(color: Colors.black54, fontSize: 15)),
//                  textAlign: TextAlign.center,
//                ),
//              ),

              Container(
                  width: deviceWidth,
                  padding: EdgeInsets.all(30),
                  child: PinCodeTextField(
                    length: 6,
                    obsecureText: false,
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      borderRadius: BorderRadius.circular(5),
                      fieldHeight: 50,
                      activeColor: appTheme,
                      fieldWidth: 40,
                      activeFillColor:
                      hasError ? Colors.orange : Colors.white,
                    ),
                    //inactiveColor: Color(0XFFC4C4C4) ,
                    //selectedColor: Colors.red,
                    animationType: AnimationType.fade,
                    //shape: PinCodeFieldShape.box,
                    animationDuration: Duration(milliseconds: 300),
                    //backgroundColor: backgroundColor,
                    controller: textEditingController,
                    textCapitalization: TextCapitalization.sentences,
                    onChanged: (value) {
                      print(value);
                      setState(() {
                        currentText = value;
                      });
                    },
                    onCompleted: (v) {
                      print('Finish$v');
                     // verify();
                    },
                  )),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
          SizedBox(
                height: 14,
              ),  Container(
                      padding: EdgeInsets.all(10),
                      height: 80,
                      width: deviceWidth,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          //side: BorderSide(color: Colors.red)
                        ),
                        color: appTheme,
                        onPressed: ()  {
                            setState(() {
                              _isLoading = true;
                            });
                             verify();

                        },
                        child: _isLoading
                            ? Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          ),
                        )
                            : Text(
                          'Confirm',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      )),
                ],
              )
            ],
          ),

      ),
    ]
      ),

    );
  }

}