import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grainpointapp/Farmers/addFarmer/bankIdentification.dart';
import 'package:grainpointapp/Farmers/addFarmer/farmerProfile.dart';
import 'package:grainpointapp/Farmers/addFarmer/farmingHistory.dart';
import 'package:grainpointapp/Farmers/addFarmer/selectClient.dart';
import 'package:grainpointapp/Farmers/checkFarmers.dart';
import 'package:grainpointapp/Farmers/createFarmGroup.dart';
import 'package:grainpointapp/Farmers/farmGroups.dart';
import 'package:grainpointapp/Farmers/offlineFarmers.dart';
import 'package:grainpointapp/Farmers/syncedFarmers.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/manageOrders/ordersPlaced.dart';
import 'package:grainpointapp/profile/userProfile.dart';
import 'package:grainpointapp/Input/collectHarvest.dart';
import 'package:grainpointapp/Input/disburseInput.dart';
import 'package:grainpointapp/Input/marketPrice.dart';
import 'package:grainpointapp/profile/changePassword.dart';
import 'package:grainpointapp/profile/profile.dart';
import 'package:grainpointapp/registration/register.dart';
import 'package:grainpointapp/registration/registerTwo.dart';
import 'package:grainpointapp/splashscreen.dart';

import 'login/login.dart';

// Started on 27th July 2020
// By Makinde Daniel

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
        debugShowCheckedModeBanner: false,
      title: 'TFS',
        theme: ThemeData(
          textTheme: GoogleFonts.montserratTextTheme(
            Theme.of(context).textTheme,
          ),
        ),
    home:AnimatedSplashScreen(),
    routes: <String,WidgetBuilder>{

    "/Login":(BuildContext context)=>new Login(),
    "/Register":(BuildContext context)=>new Register(),
      "/RegisterTwo":(BuildContext context)=>new RegisterTwo(),
      "/Home":(BuildContext context)=>new Home(),
      "/SelectClient":(BuildContext context)=>new SelectClient(),
      "/FarmerProfile":(BuildContext context)=>new FarmerProfile(),
      "/CreateGroup":(BuildContext context)=>new CreateGroup(),
      "/CheckFarmers":(BuildContext context)=>new CheckFarmers(),
      "/DisburseInput":(BuildContext context)=>new DisburseInput(),
      "/CollectHarvest":(BuildContext context)=>new CollectHarvest(),
      "/MarketPrice":(BuildContext context)=>new MarketPrice(),
      "/Profile":(BuildContext context)=>new Profile(),
      "/ChangePassword":(BuildContext context)=>new ChangePassword(),
      "/FarmerGroup":(BuildContext context)=>new FarmerGroup(),
      "/BankIdentification":(BuildContext context)=>new BankIdentification(),
      "/FarmingHistory":(BuildContext context)=>new FarmingHistory(),
      "/OfflineFarmers":(BuildContext context)=>new OfflineFarmers(),
      "/SyncedFarmers":(BuildContext context)=>new SyncedFarmers(),
      "/UserProfile":(BuildContext context)=>new UserProfile(),
      "/OrderPlaced":(BuildContext context)=>new OrderPlaced(),


















    });
  }
}


