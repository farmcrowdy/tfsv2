
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/const.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Login extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Login> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool passwordVisible;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordVisible = true;
    _state();
    _localG();
    _banks();
  }

  Map<String, dynamic> responseData;
  var data;
  var token;
  String name;
  String phoneNo;
  String id;
  String email;
  String status;
  String approved;
  String accessLevel;
  String message;
  var body;
  SharedPreferences prefs;

  var lg = List();
  var state = List();
  var banks = List();
  // Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Logging in"),
            content: Text(message),
            shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }
  // ignore: missing_return
  Future<String> _state() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = await http.get(
        baseUrl + 'states',
        headers: {
          'api-key': apiKey});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        state = responseData["message"];
      });

      await prefs.setString("state", json.encode(state));
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }
  // ignore: missing_return
  Future<String> _localG() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = await http.get(
        baseUrl + 'localgovt',
        headers: {
          'api-key': apiKey});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        lg =responseData["message"];
      });
      await   prefs.setString("lg", json.encode(lg));
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }
  // ignore: missing_return
  Future<String> _banks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final response = await http.get(
        baseUrl + 'banks',
        headers: {
          'api-key': apiKey});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        banks =responseData["message"];
      });

      await   prefs.setString("banks", json.encode(banks));
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }


  //Http call
  login() async {
    prefs = await SharedPreferences.getInstance();
    body = {
      "email": emailController.text.trim(),
      "password": passwordController.text.trim(),
    };
    print(data);
    final response = await http.post(baseUrl + 'userlogin/',
        headers: {'api-key': apiKey},
        body: body);
    if (response.statusCode == 200) {

      Navigator.popAndPushNamed(context, "/Home");
      responseData = json.decode(response.body);
      status = responseData["status"];
      message = responseData["message"];
      approved = responseData['approved'];
      accessLevel = responseData['access_level'];
      print(message);
      await prefs.setString('id', '$message');
      id = responseData['client_id'];
//      email = responseData['data']['user']['email'];
//      await prefs.setString('name', name);
//      await prefs.setString('phoneNo', phoneNo);
//      await prefs.setString('userId', '$id');
//      await prefs.setString('email', '$email');
     // print('token $token and $name');
      print(responseData);
      print(body);
      print(status);
      Fluttertoast.showToast(
        msg: status,
        toastLength: Toast.LENGTH_LONG,
      );
      setState(() {
        _isLoading = false;
      });
    } else {
      setState(() {
        _isLoading = false;
      });
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$responseData');
      _showErrorDialog(message: er);
    }
  }


  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;

    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;
    return Scaffold(
        body:
        Form(
        key: _formKey,
        autovalidate: false,
        child:

        Column(

            children: <Widget>[
              Expanded(
                  child: Stack(
                      children: <Widget>[
                        Container(
                          height: deviceHeight /2.0,
                          width: deviceWidth,
                          child:
                          Image.asset('images/welcomeHeader.png', fit: BoxFit.cover,),
                        ),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                                padding: EdgeInsets.all(3),
                                height: deviceHeight / 1.7,
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                child:

                                Container(
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: Color(0xffFFFFFF),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(15),
                                        topRight: Radius.circular(15)),
                                    // boxShadow: <BoxShadow>[
                                  ),
                                 child:
                                      ListView(

                                          children: <Widget>[
Container(
  padding: EdgeInsets.only(left: 24, top: 40,),
  child:
Text('WELCOME!', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),),
                                            Container(
                                              padding: EdgeInsets.all(20),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    'Email Address',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 15, ),
                                                  ),
                                                  TextFormField(
                                                    controller: emailController,
                                                    validator: (value) {
                                                      if (value.isEmpty ||
                                                          !value.contains('@') ||
                                                          !value.contains('.')) {
                                                        return "Invalid Email";
                                                      }
                                                      return null;
                                                    },
                                                    keyboardType: TextInputType.emailAddress,
                                                    style: TextStyle(color: Colors.black),
                                                    cursorColor: Colors.black,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.all(20),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    'Password',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 15),
                                                  ),
                                                  TextFormField(
                                                    controller: passwordController,
                                                    validator: (value) {
                                                      if (value.isEmpty) {
                                                        return "Invalid Password";
                                                      }
                                                      return null;
                                                    },
                                                    decoration: InputDecoration(
                                                      suffixIcon: IconButton(
                                                        icon: Icon(
                                                          // Based on passwordVisible state choose the icon
                                                          passwordVisible
                                                              ? Icons.visibility
                                                              : Icons.visibility_off,
                                                          color: Colors.black,
                                                        ),
                                                        onPressed: () {
                                                          // Update the state i.e. toogle the state of passwordVisible variable
                                                          setState(() {
                                                            passwordVisible = !passwordVisible;
                                                          });
                                                        },
                                                      ),

                                                      hintText: '******',
                                                      hintStyle: TextStyle(
                                                        color: Colors.black45,
                                                      ),
                                                      labelStyle: TextStyle(color: Colors.blue),
                                                    ),
                                                    obscureText: passwordVisible,
                                                    keyboardType: TextInputType.visiblePassword,
                                                    style: TextStyle(color: Colors.black),
                                                    cursorColor: Colors.black,
                                                  ),
                                                ],
                                              ),
                                            ),

                                            InkWell(
                                              onTap: (){

                                              },
                                              child:
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Text(''),

                                                Text('Forgot Password?', style: TextStyle(color: appTheme),)
                                              ],
                                            ),
                                            ),

                                            SizedBox(height: 10,),

                                            Container(
                                                padding: EdgeInsets.all(10),
                                                height: 80,
                                                width: deviceWidth,
                                                child:
                                                RaisedButton(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius: new BorderRadius
                                                        .circular(25.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  color: appTheme,
                                                  onPressed: () async{
    if (_formKey.currentState.validate()) {
    setState(() {
    _isLoading = true;
    });
    await login();
    }
    },
                                                  child: _isLoading
                                                      ? Padding(
                                                    padding: const EdgeInsets.all(4.0),
                                                    child: CircularProgressIndicator(
                                                      backgroundColor: Colors.white,
                                                    ),
                                                  )
                                                  : Text('Login', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                                                )
                                            ),
                                            Center(
                                              child:
                                              Text('or',style: TextStyle(fontSize: 15),),
                                            ),
Center(
  child:
      InkWell(
        onTap: (){
Navigator.pushNamed(context, '/Register');
        },
        child:
                                            Container(
                                              margin: EdgeInsets.all(10),
                                                height: 60,
                                                width: deviceWidth  ,
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(20.0),
                                                    border:
                                                    Border.all(color: Colors.grey[400])),
                                                  child:

Column(
  crossAxisAlignment: CrossAxisAlignment.center,
  mainAxisAlignment: MainAxisAlignment.center,
  children: [
    Text('Create an account', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),),
  ],
)

                                                ),
      ),
),
                                            Center(
                                              child:
                                            RichText(
                                              text: TextSpan(children: [
                                                TextSpan(
                                                  text: 'By signing in you agree to our,',
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.blueGrey,
                                                  ),
                                                ),
                                                TextSpan(
                                                  text: 'Terms of Service ',
                                                  style: TextStyle(
                                                      fontSize: 15,
                                                      color: appTheme,
                                                      fontWeight: FontWeight.bold),
                                                ),
                                              ]),
                                            ),),
                                          ]
                                      ),
                            )
                        )
                        )
                      ]
                  )
              )
            ]
        )
        )
    );
  }
}
