import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/Farmers/checkFarmers.dart';
import 'package:grainpointapp/Input/approveHarvest.dart';
import 'package:grainpointapp/Input/collectHarvest.dart';
import 'package:grainpointapp/Input/disburseInput.dart';
import 'package:grainpointapp/SQL/addFarmerModel.dart';
import 'package:grainpointapp/SQL/database_helper.dart';
import 'package:grainpointapp/const.dart';
import 'package:grainpointapp/manageOrders/agentLocation.dart';
import 'package:grainpointapp/manageOrders/ordersInCenter.dart';
import 'package:grainpointapp/manageOrders/ordersPlaced.dart';
import 'package:grainpointapp/manageOrders/pricesOfProduce.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:sqflite/sqflite.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';

String id;
String phoneNo;
var lat;
var lng;
String clientName;
var clientId;

class Home extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Home> {
  TextEditingController farmerNumberController = TextEditingController();
  TextEditingController produceController = TextEditingController();
  TextEditingController agentIdController = TextEditingController();
  TextEditingController clientIdController = TextEditingController();
  TextEditingController collectCenterIdController = TextEditingController();

  var location = Location();
  bool passwordVisible;
  var content = "Farmers";
  String firstName;
  String phoneNumber;
  var userId;
  String title;
  var data;
  Map<String, dynamic> responseData;
  var result;
  String token;
  var body;
  var bodyFcmb;
  var message;
  var centerId;
  var centerName;

  String lastName;
  String lg;
  String state;
  String email;
  SharedPreferences prefs;
  var collectionCenter = List();
  var  projects  = List();
  var projectId;
  bool isUploading = false;
  bool _isLoading = false;
  var databaseId;
  List results;

  //Counters
  int duplicate = 0;
  int success = 0;
  int failed = 0;
  int counter = 0;
  int databaseLength = 0;

  Farmers farmers;
  List<Farmers> farmerList;
  DatabaseAddFarmerHelper helper = DatabaseAddFarmerHelper();
  Database db;



  @override
  void initState() {
    super.initState();
    getToken();

    getUserLocation();
  }

  void getToken() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      id = prefs.getString('id');
      _fetchUserProfile();
      _collectionCenter();


    });
  }

  // To get the collection centers to the dropdown
  // ignore: missing_return
  Future<String> _collectionCenter() async {
    final response =
        await http.get(baseUrl + 'agent_assigned_locations/$id', headers: {'api-key': apiKey});
    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
       // centerId = (responseData['message']['collection_center_id']);
        //centerName = (responseData['message']['collection_center_name']);
        collectionCenter = responseData["message"];
        print(collectionCenter);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }
  // ignore: missing_return
  Future<String> _projects() async {
    final response =
    await http.get(baseUrl + 'projects_by_client/$clientId', headers: {'api-key': apiKey});

    print(response.statusCode);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        print(responseData);
        projects = responseData["message"];
        print(projects);
      });
      await prefs.setString("projects", json.encode(projects));
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      Fluttertoast.showToast(msg: er);
      print('error$sa');
      print(er);
      throw Exception('Failed to load internet');
    }
  }

  // This fetches the user profile
  _fetchUserProfile() async {
    prefs = await SharedPreferences.getInstance();
    final response = await http.get(
      baseUrl + 'user/$id',
      headers: {'api-key': apiKey},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        firstName = (responseData['message']['firstname']);
        lastName = (responseData['message']['surname']);
        email = (responseData['message']['email']);
        clientId = responseData['message']['client_id'];
        clientName = responseData['message']['client_name'];
        phoneNo = (responseData['message']['uphone']);
        print(phoneNo);
        userId = (responseData['message']['user_id']);
        print(firstName);
        print(userId);
        print(responseData);
        _projects();
      });
      await prefs.setString('phoneNo', '$phoneNo');
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  //Dialog for Collect Harvest
  _showFarmerHarvest() async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              backgroundColor: appTheme,
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Input the user phone number',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: TextFormField(
                            controller: farmerNumberController,
                            maxLength: 11,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              hintText: '08101010101',
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              labelStyle: TextStyle(color: Colors.grey),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black),
                            cursorColor: Colors.grey,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: 300,
                          height: 60,
                          child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              elevation: 5.0,
                              child: MaterialButton(
                                padding: EdgeInsets.all(5),
                                onPressed: () {
                                  Navigator.pop(context);
                                  farmerNumberController.text.isNotEmpty
                                      ? toFull(context,
                                          farmerPhoneNo:
                                              farmerNumberController.text)
                                      : Fluttertoast.showToast(
                                          msg: 'Phone number can\'t be empty!');
                                },
                                child: Text(
                                  'Verify',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 12.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }

  //Dialog for Check for Farmers
  _checkFarmers() async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              backgroundColor: appTheme,
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Input the user phone number',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: TextFormField(
                            controller: farmerNumberController,
                            maxLength: 11,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              hintText: '08101010101',
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              labelStyle: TextStyle(color: Colors.grey),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black),
                            cursorColor: Colors.grey,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: 300,
                          height: 60,
                          child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              elevation: 5.0,
                              child: MaterialButton(
                                padding: EdgeInsets.all(5),
                                onPressed: () {
                                  Navigator.pop(context);
                                  farmerNumberController.text.isNotEmpty
                                      ? checkFarmers(context,
                                          farmerPhoneNo:
                                              farmerNumberController.text)
                                      : Fluttertoast.showToast(
                                          msg: 'Phone number can\'t be empty!');
                                },
                                child: Text(
                                  'Verify',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 12.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }

//Dialog for Approve Harvest and Payment
  _approve() async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              backgroundColor: appTheme,
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Input the Produce ID',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: TextFormField(
                            controller: produceController,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              hintText: '',
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              labelStyle: TextStyle(color: Colors.grey),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black),
                            cursorColor: Colors.grey,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: 300,
                          height: 60,
                          child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              elevation: 5.0,
                              child: MaterialButton(
                                padding: EdgeInsets.all(5),
                                onPressed: () {
                                  Navigator.pop(context);
                                  produceController.text.isNotEmpty
                                      ? toApprove(context,
                                          produceId: produceController.text)
                                      : Fluttertoast.showToast(
                                          msg: 'Phone number can\'t be empty!');
                                },
                                child: Text(
                                  'Verify',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 12.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }

  //Dialog for Disburse Input
  _showFarmerDisburse() async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              backgroundColor: appTheme,
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Input the user phone number',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: TextFormField(
                            controller: farmerNumberController,
                            maxLength: 11,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              hintText: '08101010101',
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              labelStyle: TextStyle(color: Colors.grey),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black),
                            cursorColor: Colors.grey,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: 300,
                          height: 60,
                          child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              elevation: 5.0,
                              child: MaterialButton(
                                padding: EdgeInsets.all(5),
                                onPressed: () {
                                  Navigator.pop(context);
                                  farmerNumberController.text.isNotEmpty
                                      ? toDisburse(context,
                                          farmerPhoneNo:
                                              farmerNumberController.text)
                                      : Fluttertoast.showToast(
                                          msg: 'Phone number can\'t be empty!');
                                },
                                child: Text(
                                  'Verify',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 12.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }

  //Dialog for Agent Location
  _agentLocation() async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              backgroundColor: appTheme,
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Input the agent Id',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),
                        Container(
                          padding: EdgeInsets.all(5),
                          child: TextFormField(
                            controller: agentIdController,
                            maxLength: 11,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(
                                  color: Colors.white,
                                  width: 1.0,
                                ),
                              ),
                              hintText: '123',
                              hintStyle: TextStyle(
                                color: Colors.grey,
                              ),
                              labelStyle: TextStyle(color: Colors.grey),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black),
                            cursorColor: Colors.grey,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: 300,
                          height: 60,
                          child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              elevation: 5.0,
                              child: MaterialButton(
                                padding: EdgeInsets.all(5),
                                onPressed: () {
                                  Navigator.pop(context);
                                  agentIdController.text.isNotEmpty
                                      ? toAgentLocation(context,
                                          agentId: agentIdController.text)
                                      : Fluttertoast.showToast(
                                          msg: 'Phone number can\'t be empty!');
                                },
                                child: Text(
                                  'Verify',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 12.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }


// Dialog for Prices in collection center
  _priceOfProduce() async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              backgroundColor: appTheme,
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Select the collection center Id',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),

                        Center(
                            child: Container(

                                padding: EdgeInsets.only(left: 10),
                                height: 60.0,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(7.0),
                                    border: Border.all(color: Colors.grey)),
                                child: Center(
                                  child: new DropdownButtonFormField(
                                    decoration:
                                    InputDecoration.collapsed(hintText: ''),
                                    isExpanded: true,
                                    items: collectionCenter.map((item) {
                                      return new DropdownMenuItem(
                                        child: new Text(item['collection_center_name']),
                                        value: item['collection_center_id'],
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      setState(() {
                                        centerId = newVal;
                                        print(centerId);
                                      });
                                    },
                                    //value: unitPrice,
                                  ),
                                ))
                        ),
                        SizedBox(height: 10,),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: 300,
                          height: 60,
                          child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              elevation: 5.0,
                              child: MaterialButton(
                                padding: EdgeInsets.all(5),
                                onPressed: () {
                                  Navigator.pop(context);
                                  centerId != null
                                      ? priceCollectionCenter(
                                          context,
                                          collectionCenterId:
                                          centerId,
                                        )
                                      : Fluttertoast.showToast(
                                          msg: 'Center can\'t be empty!');
                                },
                                child: Text(
                                  'Verify',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 12.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }

  //Dialog for Orders in Center
  _orderInCenter() async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              backgroundColor: appTheme,
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Select the collection center Id',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),

                        Center(
                            child: Container(
                                padding: EdgeInsets.only(left: 10),
                                height: 60.0,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(7.0),
                                    border: Border.all(color: Colors.grey)),
                                child: Center(
                                  child: new DropdownButtonFormField(
                                    decoration:
                                    InputDecoration.collapsed(hintText: ''),
                                    isExpanded: true,
                                    items: collectionCenter.map((item) {
                                      return new DropdownMenuItem(
                                        child: new Text(item['collection_center_name']),
                                        value: item['collection_center_id'],
                                      );
                                    }).toList(),
                                    onChanged: (newVal) {
                                      setState(() {
                                        centerId = newVal;
                                        print(centerId);
                                      });
                                    },
                                    //value: unitPrice,
                                  ),
                                ))
                        ),
                        SizedBox(height: 10,),
                        Container(
                          padding: EdgeInsets.all(5),
                          width: 300,
                          height: 60,
                          child: Material(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                              elevation: 5.0,
                              child: MaterialButton(
                                padding: EdgeInsets.all(5),
                                onPressed: () {
                                  Navigator.pop(context);
                                  centerId != null
                                      ? orderInCenter(context,
                                          collectionCenterId:
                                             centerId)
                                      : Fluttertoast.showToast(
                                          msg: 'Center can\'t be empty!');
                                },
                                child: Text(
                                  'Verify',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 12.0,
                                    color: Colors.blueGrey,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }

  @override
  Widget build(BuildContext context) {

    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: false);
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        body: ListView(children: <Widget>[
      Stack(children: <Widget>[
        Container(
          height: 300,
          width: deviceWidth,
          child: Image.asset(
            'images/greenHeader.png',
            fit: BoxFit.cover,
          ),
        ),
        Container(
            height: 300,
            padding: EdgeInsets.only(top: 100, left: 30),
            child: InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/Profile');
              },
              child: Column(
                children: [
                  Container(
                      //padding: EdgeInsets.only(top: 100, left: 50),
                      child: Text(
                    firstName != null ? firstName : '',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 30),
                  )),
                  Container(
                      // padding: EdgeInsets.only(top: 140, left: 50),
                      child: Text(
                    lastName != null ? lastName : '',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 30),
                  )),
                ],
              ),
            ))
      ]),
      SizedBox(
        height: 20,
      ),

      //This for the tabs
      Container(
        height: 50,
        child: Flex(
          direction: Axis.vertical,
          children: [
            Expanded(
              child: DefaultTabController(
                length: 5,
                child: Column(
                  children: <Widget>[
                    Container(
                      constraints: BoxConstraints.expand(height: 50),
                      child: TabBar(
                          onTap: (index) {
                            switch (index) {
                              case 0:
                                setState(() {
                                  content = "Farmers";
                                });

                                break;
                              case 1:
                                setState(() {
                                  content = "Insurance";
                                });

                                break;
                              case 2:
                                setState(() {
                                  content = "Input & Harvest";
                                });
                                break;
                              case 3:
                                setState(() {
                                  content = "Manage Orders";
                                });
                                break;
                              case 4:
                                setState(() {
                                  content = "Data Collation";
                                });

                                break;
                              default:
                                content = "Default";
                                break;
                            }
                            print("You are clicking the $content");
                          },
                          indicatorColor: Colors.black,
                          unselectedLabelColor: greyish,
                          //isScrollable: true,
                          labelColor: Colors.black,
                          tabs: [
                            Tab(
                                child: Column(
                              children: [
                                Text(
                                  'Farmers',
                                  style: TextStyle(fontSize: ScreenUtil().setSp(20),),
                                ),
                              ],
                            )),
                            Tab(
                                child: Column(
                              children: [
                                Text(
                                  'Insurance',
                                  style: TextStyle(fontSize: ScreenUtil().setSp(20),),
                                ),
                              ],
                            )),
                            Tab(
                                child: Column(
                              children: [
                                Text(
                                  'Input & Harvest',
                                  style: TextStyle(fontSize: ScreenUtil().setSp(20),),
                                ),
                              ],
                            )),
                            Tab(
                                child: Column(
                              children: [
                                Text(
                                  'Manage Orders',
                                  style: TextStyle(fontSize: ScreenUtil().setSp(20),),
                                ),
                              ],
                            )),
                            Tab(
                                child: Column(
                              children: [
                                Text(
                                  'Data Collation',
                                  style: TextStyle(fontSize: ScreenUtil().setSp(20),),
                                ),
                              ],
                            )),
                          ]),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      content == 'Farmers'
          ? Column(
              children: [
                Container(
                    padding: EdgeInsets.all(10),
                    height: 90,
                    width: deviceWidth,
                    child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          //side: BorderSide(color: Colors.red)
                        ),
                        color: Colors.grey[200],
                        onPressed: () {
                          Navigator.pushNamed(context, '/SelectClient');
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Icon(Icons.perm_identity),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                'Register',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(25),
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                              ),
                            ]),
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: appTheme,
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ))),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 90,
                    width: deviceWidth,
                    child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          //side: BorderSide(color: Colors.red)
                        ),
                        color: Colors.grey[200],
                        onPressed: () {
                          Navigator.pushNamed(context, '/CreateGroup');
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Icon(Icons.perm_identity),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                'Create a New Farm Group',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(25),
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                              ),
                            ]),
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: appTheme,
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ))),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 90,
                    width: deviceWidth,
                    child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          //side: BorderSide(color: Colors.red)
                        ),
                        color: Colors.grey[200],
                        onPressed: () {
                          Navigator.pushNamed(context, '/FarmerGroup');
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Icon(Icons.perm_identity),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                'All Farmer Groups',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(25),
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                              ),
                            ]),
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: appTheme,
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ))),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 90,
                    width: deviceWidth,
                    child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          //side: BorderSide(color: Colors.red)
                        ),
                        color: Colors.grey[200],
                        onPressed: () {
                          _checkFarmers();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Icon(Icons.perm_identity),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                'Check for Farmers',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(25),
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                              ),
                            ]),
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: appTheme,
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ))),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 90,
                    width: deviceWidth,
                    child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          //side: BorderSide(color: Colors.red)
                        ),
                        color: Colors.grey[200],
                        onPressed: () {
                          Navigator.pushNamed(context, '/OfflineFarmers');
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Icon(Icons.perm_identity),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                'Offline Farmers',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(25),
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                              ),
                            ]),
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: appTheme,
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ))),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 90,
                    width: deviceWidth,
                    child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          //side: BorderSide(color: Colors.red)
                        ),
                        color: Colors.grey[200],
                        onPressed: () {
                          Navigator.pushNamed(context, '/SyncedFarmers');
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [
                              Icon(Icons.perm_identity),
                              SizedBox(
                                width: 30,
                              ),
                              Text(
                                'Farmers Already Synced',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(25),
                                    fontWeight: FontWeight.w500,
                                    color: Colors.black),
                              ),
                            ]),
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                color: appTheme,
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ))),
              ],
            )
          : content == 'Insurance'
              ? Column(
                  children: [
                    Container(
                        padding: EdgeInsets.all(10),
                        height: 90,
                        width: deviceWidth,
                        child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(25.0),
                              //side: BorderSide(color: Colors.red)
                            ),
                            color: Colors.grey[200],
                            onPressed: () {
                              // Navigator.pushNamed(context, '/Home');
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(children: [
                                  Icon(Icons.perm_identity),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    'Add Farmer to an Insurance Claim',
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(25),
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black),
                                  ),
                                ]),
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    color: appTheme,
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ))),
                    Container(
                        padding: EdgeInsets.all(10),
                        height: 90,
                        width: deviceWidth,
                        child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(25.0),
                              //side: BorderSide(color: Colors.red)
                            ),
                            color: Colors.grey[200],
                            onPressed: () {
                              // Navigator.pushNamed(context, '/Home');
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(children: [
                                  Icon(Icons.perm_identity),
                                  SizedBox(
                                    width: 30,
                                  ),
                                  Text(
                                    'Check Farmer\'s Insurance Claim',
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(25),
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black),
                                  ),
                                ]),
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    color: appTheme,
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  child: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ))),
                  ],
                )
              : content == 'Input & Harvest'
                  ? Column(
                      children: [
                        Container(
                            padding: EdgeInsets.all(10),
                            height: 90,
                            width: deviceWidth,
                            child: FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(25.0),
                                  //side: BorderSide(color: Colors.red)
                                ),
                                color: Colors.grey[200],
                                onPressed: () {
                                  _showFarmerDisburse();
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(children: [
                                      Icon(Icons.perm_identity),
                                      SizedBox(
                                        width: 30,
                                      ),
                                      Text(
                                        'Disburse Input',
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(25),
                                            fontWeight: FontWeight.w500,
                                            color: Colors.black),
                                      ),
                                    ]),
                                    Container(
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                        color: appTheme,
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      ),
                                    )
                                  ],
                                ))),
                        Container(
                            padding: EdgeInsets.all(10),
                            height: 90,
                            width: deviceWidth,
                            child: FlatButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(25.0),
                                  //side: BorderSide(color: Colors.red)
                                ),
                                color: Colors.grey[200],
                                onPressed: () {
                                  _showFarmerHarvest();
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(children: [
                                      Icon(Icons.perm_identity),
                                      SizedBox(
                                        width: 30,
                                      ),
                                      Text(
                                        'Collect Harvest',
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(25),
                                            fontWeight: FontWeight.w500,
                                            color: Colors.black),
                                      ),
                                    ]),
                                    Container(
                                      height: 40,
                                      width: 40,
                                      decoration: BoxDecoration(
                                        color: appTheme,
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.white,
                                      ),
                                    )
                                  ],
                                ))),
                        Container(
                            padding: EdgeInsets.all(10),
                            height: 90,
                            width: deviceWidth,
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                //side: BorderSide(color: Colors.red)
                              ),
                              color: Colors.grey[200],
                              onPressed: () {
                                Navigator.pushNamed(context, '/MarketPrice');
                              },
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(children: [
                                    Icon(Icons.perm_identity),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Text(
                                      'Market Prices',
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(25),
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                    ),
                                  ]),
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      color: appTheme,
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.white,
                                    ),
                                  )
                                ],
                              ),
                            )),
                        Container(
                            padding: EdgeInsets.all(10),
                            height: 90,
                            width: deviceWidth,
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(25.0),
                                //side: BorderSide(color: Colors.red)
                              ),
                              color: Colors.grey[200],
                              onPressed: () {
                                _approve();
                              },
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(children: [
                                    Icon(Icons.perm_identity),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Text(
                                      'Approve Harvest and Payment',
                                      style: TextStyle(
                                          fontSize: ScreenUtil().setSp(25),
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                    ),
                                  ]),
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                      color: appTheme,
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.white,
                                    ),
                                  )
                                ],
                              ),
                            )),
                      ],
                    )
                  : content == 'Manage Orders'
                      ? Column(
                          children: [
                            Container(
                                padding: EdgeInsets.all(10),
                                height: 90,
                                width: deviceWidth,
                                child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(25.0),
                                      //side: BorderSide(color: Colors.red)
                                    ),
                                    color: Colors.grey[200],
                                    onPressed: () {
                                      _priceOfProduce();
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(children: [
                                          Icon(Icons.perm_identity),
                                          SizedBox(
                                            width: 30,
                                          ),
                                          Text(
                                            'Prices of produce',
                                            style: TextStyle(
                                                fontSize: ScreenUtil().setSp(25),
                                                fontWeight: FontWeight.w500,
                                                color: Colors.black),
                                          ),
                                        ]),
                                        Container(
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                            color: appTheme,
                                            borderRadius:
                                                BorderRadius.circular(30.0),
                                          ),
                                          child: Icon(
                                            Icons.arrow_forward_ios,
                                            color: Colors.white,
                                          ),
                                        )
                                      ],
                                    ))),
                            Container(
                                padding: EdgeInsets.all(10),
                                height: 90,
                                width: deviceWidth,
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(25.0),
                                    //side: BorderSide(color: Colors.red)
                                  ),
                                  color: Colors.grey[200],
                                  onPressed: () {
                                    _orderInCenter();
                                  },
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(children: [
                                        Icon(Icons.perm_identity),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Orders in Center',
                                          style: TextStyle(
                                              fontSize: ScreenUtil().setSp(25),
                                              fontWeight: FontWeight.w500,
                                              color: Colors.black),
                                        ),
                                      ]),
                                      Container(
                                        height: 40,
                                        width: 40,
                                        decoration: BoxDecoration(
                                          color: appTheme,
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        ),
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          color: Colors.white,
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                            Container(
                                padding: EdgeInsets.all(10),
                                height: 90,
                                width: deviceWidth,
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(25.0),
                                    //side: BorderSide(color: Colors.red)
                                  ),
                                  color: Colors.grey[200],
                                  onPressed: () {
                                    Navigator.pushNamed(context, '/OrderPlaced');
                                  },
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(children: [
                                        Icon(Icons.perm_identity),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Text(
                                          'Order placed',
                                          style: TextStyle(
                                              fontSize: ScreenUtil().setSp(25),
                                              fontWeight: FontWeight.w500,
                                              color: Colors.black),
                                        ),
                                      ]),
                                      Container(
                                        height: 40,
                                        width: 40,
                                        decoration: BoxDecoration(
                                          color: appTheme,
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        ),
                                        child: Icon(
                                          Icons.arrow_forward_ios,
                                          color: Colors.white,
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                          ],
                        )
                      : Column(
                          children: [
                            Container(
                                padding: EdgeInsets.all(10),
                                height: 90,
                                width: deviceWidth,
                                child: FlatButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(25.0),
                                      //side: BorderSide(color: Colors.red)
                                    ),
                                    color: Colors.grey[200],
                                    onPressed: () {
                                      query();
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                          Icon(Icons.perm_identity),
                                          SizedBox(
                                            width: 30,
                                          ),
                                          _isLoading
                                              ? Center(child:Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: CircularProgressIndicator(
                                              backgroundColor: Colors.white,
                                            ),
                                          ))
                                              :
                                          Text(
                                            'Sync Online',
                                            style: TextStyle(
                                                fontSize: ScreenUtil().setSp(25),
                                                fontWeight: FontWeight.w500,
                                                color: Colors.black),
                                          ),
                                        ]),
                                        Container(
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                            color: appTheme,
                                            borderRadius:
                                                BorderRadius.circular(30.0),
                                          ),
                                          child: Icon(
                                            Icons.arrow_forward_ios,
                                            color: Colors.white,
                                          ),
                                        )
                                      ],
                                    ))),
//                            Container(
//                                padding: EdgeInsets.all(10),
//                                height: 90,
//                                width: deviceWidth,
//                                child: FlatButton(
//                                    shape: RoundedRectangleBorder(
//                                      borderRadius:
//                                          new BorderRadius.circular(25.0),
//                                      //side: BorderSide(color: Colors.red)
//                                    ),
//                                    color: Colors.grey[200],
//                                    onPressed: () {
//                                      query();
//                                    },
//                                    child: Row(
//                                      mainAxisAlignment:
//                                          MainAxisAlignment.spaceBetween,
//                                      children: [
//                                        Row(children: [
//                                          Icon(Icons.perm_identity),
//                                          SizedBox(
//                                            width: 30,
//                                          ),
//                                          _isLoading
//                                              ? Center(child:Padding(
//                                            padding: const EdgeInsets.all(4.0),
//                                            child: CircularProgressIndicator(
//                                              backgroundColor: Colors.white,
//                                            ),
//                                          ))
//                                              :
//                                          Text(
//                                            'Re-Sync All Data',
//                                            style: TextStyle(
//                                                fontSize: 20,
//                                                fontWeight: FontWeight.w500,
//                                                color: Colors.black),
//                                          ),
//                                        ]),
//                                        Container(
//                                          height: 40,
//                                          width: 40,
//                                          decoration: BoxDecoration(
//                                            color: appTheme,
//                                            borderRadius:
//                                                BorderRadius.circular(30.0),
//                                          ),
//                                          child: Icon(
//                                            Icons.arrow_forward_ios,
//                                            color: Colors.white,
//                                          ),
//                                        )
//                                      ],
//                                    ))),
                          ],
                        )
    ]));
  }

  // This Navigates to Collect harvest page
  toFull(BuildContext context, {String farmerPhoneNo}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) =>
                CollectHarvest(farmerPhoneNo: farmerPhoneNo)));
  }

  // This Navigates to get agent location page
  toAgentLocation(BuildContext context, {String agentId}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) => AgentLocation(agentId: agentId)));
  }

// This Navigates to check farmers page
  checkFarmers(BuildContext context, {String farmerPhoneNo}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) =>
                CheckFarmers(farmerPhoneNo: farmerPhoneNo)));
  }

  // This Navigates to disburse input page
  toDisburse(BuildContext context, {String farmerPhoneNo}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) =>
                DisburseInput(farmerPhoneNo: farmerPhoneNo)));
  }

  // This Navigates to approve harvest page
  toApprove(BuildContext context, {String produceId}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) => ApproveHarvest(produceId: produceId)));
  }



  // This Navigates to price of collection center page
  priceCollectionCenter(BuildContext context,
      {String clientId, String collectionCenterId}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) => PriceProduce(
                 collectionCenterId: collectionCenterId)));
  }

// This Navigates to orders in centers page
  orderInCenter(BuildContext context, {String collectionCenterId}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) =>
                OrderCenter(collectionCenterId: collectionCenterId)));
  }

// Function to get users coordinate
  Future getUserLocation() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      lat = currentLocation.latitude;
      lng = currentLocation.longitude;
      print('$lat');
      print('$lng');
      // final center = LatLng(lat, lng);
      //return center;
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }
  void method2() async {
    List<String> myArray = <String>['a','b','c'];
    print('before loop');
    for(int i=0; i<myArray.length; i++) {
      await delayed();
    }
    print('end of loop');
  }

  Future<void> delayed() async {
    await Future.delayed(Duration(seconds: 5));
   // print('delayedPrint: $value');
  }

  // This is the syncing function
  //It queries the database and sync the local database online
  query() async {
    //gets the database path
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + "grainPoint.db";
    var db = await openDatabase(path);

    //go through all the tables in batches
    Batch batch = db.batch();
    batch.query('farmersData', columns: [
      'id',
      'fcmb_pin',
      'years_of_experience',
      'timestamp',
      'lat1',
      'long1',
      'appVersion',
      'fname',
      'farmerId',
      'govtString',
      'farm_group_id',
      'bankName',
      'bvnNumber',
      'idType',
      'idNumber',
      'dob',
      'gender',
      'phone',
      'marital_status',
      'crop_proficiency',
      'income_range',
      'pro_image',
      'pro_image_thumbnail',
      'acct_number',
      'land_area_farmed',
      'nextOfKin',
      'user_add_id',
      'state_id',
      'local_id',
      'farm_location_id',
      'bank_id',
      'number_of_dependants',
      'uploadedToParent',
      'readWriteEnglish',
      'sourceOfData',
      'haveSmartPhone',
      'f_print',
      'pimage',
      'project_id',
      'role',
      'sname',
      'synced',
      'nextOfKinPhone',
      'dobForPaga',
      'lat2',
      'lat3',
      'lat4',
      'lat5',
      'lat6',
      'long2',
      'long3',
      'long4',
      'long5',
      'long6',
      'coordinate'
    ]);

    // get all the columns to the result
    try {

      results = await batch.commit();
        afterQuery();

    } catch (e) {
      Fluttertoast.showToast(msg: 'Database is empty');
    }

  }


  afterQuery() async{
    //final numsTo2 = results.takeWhile((int f) => n=f <= 2);
    results.forEach((f) async {
      print('Number of data ${results.length}');
      setState(() {
        databaseLength = f.length;
      });

      looping(f);
    });
  }

    Future<void> looping(var f ) async {
    print(DateTime.now().second);
    // Looping through all the results to sync through
    for (int i = 0; i < f.length; i++) {
      var dbData = f.elementAt(i);

      setState(() {
        _isLoading = true;
      });

      attachData(dbData);

      //print(dbData['fname']);

// set the variables from the sqflite database

      // Uploading picture and sending fields


    }
  }


  dialog(){

    if ((success+failed+duplicate) == databaseLength) {
      print((success+failed+duplicate).toString() +'Equal'+ databaseLength.toString() );
      print(counter.toString());
      print(success+failed+duplicate);
      print(databaseLength);
      Timer(Duration(seconds: 5), () {
        setState(() {
          _isLoading = false;
        });
        _syncDialog();
        // Fluttertoast.showToast(msg: 'Synced successfully');
        // Navigator.pop(context);
      });
    }
  }

  Future<void> attachData(var dbData ) async {
    _uploadImage(File.fromUri(Uri.parse(dbData['pro_image_thumbnail'])), dbData);

  }

  //Api to send the the synced farmers details
  Future<Map<String, dynamic>> _uploadImage(File image,var dbData) async {
    setState(() {
      counter = counter + 1;
    });


    var header =
    {
      'Accept': 'application/json',
      'api-key': apiKey,
    };
    setState(() {
      isUploading = true;
    });
    print(image.path);
    // Find the mime type of the selected file by looking at the header bytes of the file
    final mimeTypeData =
    lookupMimeType(image.path, headerBytes: [0xFF, 0xD8]).split('/');
    // Intilize the multipart request
    final requests = http.MultipartRequest('POST', Uri.parse(baseUrl + 'nfarmers'));
    requests.headers.addAll(header);
    // Attach the file in the request
    final file = await http.MultipartFile.fromPath('pimage', image.path,
        contentType: MediaType(mimeTypeData[0], mimeTypeData[1]));
    // Explicitly pass the extension of the image with request body
    // Since image_picker has some bugs due which it mixes up
    // image extension with file name like this filenamejpge
    // Which creates some problem at the server side to manage
    // or verify the file extension
    //requests.fields['ext'] = mimeTypeData[1];
    requests.files.add(file);
    requests.fields['fname'] = dbData['fname'] != null ? dbData['fname'] : '';
    requests.fields['bvnNumber'] =  dbData['bvnNumber'] != null ? dbData['bvnNumber'] : '0';
    requests.fields['farm_location_id'] = dbData['farm_location_id'] != null? dbData['farm_location_id']: '';
    requests.fields['state_id'] =  dbData['state_id'] != null ? dbData['state_id'] : '';
    requests.fields['local_id'] = dbData['local_id'] != null? dbData['local_id']: '';
    requests.fields['gender'] = dbData['gender'] != null ? dbData['gender'] : '';
    requests.fields['phone'] =  dbData['phone'] != null ? dbData['phone'] : '';
    requests.fields['marital_status'] = dbData['marital_status'] != null? dbData['marital_status']: '';
    requests.fields['crop_proficiency'] =  dbData['crop_proficiency'] != null ? dbData['crop_proficiency'] : '';
    requests.fields['income_range'] = dbData['income_range'] != null? dbData['income_range']: '';
    requests.fields['dob'] = dbData['dob'] != null ? dbData['dob'] : '';
    requests.fields['bankName'] =  dbData['bank_id'] != null ? dbData['bank_id'] : '';
    requests.fields['bank_id'] = dbData['bank_id'] != null? dbData['bank_id']: '';
    requests.fields['farm_group_id'] =  dbData['farm_group_id'] != null ? dbData['farm_group_id'] : '';
    requests.fields['farmerId'] = dbData['farmerId'] != null? dbData['farmerId']: '';
    requests.fields['user_add_id'] = dbData['user_add_id'] != null ? dbData['user_add_id'] : '';
    requests.fields['fcmb_pin'] =  dbData['fcmb_pin'] != null ? dbData['fcmb_pin'] : '';
    requests.fields['timestamp'] = dbData['timestamp'] != null? dbData['timestamp']: '';
    requests.fields['lat1'] =  dbData['lat1'] != null ? dbData['lat1'] : '';
    requests.fields['long1'] = dbData['long1'] != null? dbData['long1']: '';
    requests.fields['appVersion'] = dbData['appVersion'] != null ? dbData['appVersion'] : '';
    requests.fields['govtString'] =  dbData['govtString'] != null ? dbData['govtString'] : '';
    requests.fields['idType'] = dbData['idType'] != null? dbData['idType']: '';
    requests.fields['idNumber'] =  dbData['idNumber'] != null ? dbData['idNumber'] : '';
    requests.fields['pro_image'] = dbData['pro_image'] != null? dbData['pro_image']: '';
    requests.fields['pro_image_thumbnail'] = dbData['pro_image_thumbnail'] != null ? dbData['pro_image_thumbnail'] : '';
    requests.fields['acct_number'] =  dbData['acct_number'] != null ? dbData['acct_number'] : '';
    requests.fields['land_area_farmed'] = dbData['land_area_farmed'] != null? dbData['land_area_farmed']: '';
    requests.fields['nextOfKin'] =  dbData['nextOfKin'] != null ? dbData['nextOfKin'] : '';
    requests.fields['number_of_dependants'] = dbData['number_of_dependants'] != null? dbData['number_of_dependants']: '';
    requests.fields['years_of_experience'] = dbData['years_of_experience'] != null ? dbData['years_of_experience'] : '';
    requests.fields['uploadedToParent'] =  dbData['uploadedToParent'] != null ? dbData['uploadedToParent'] : '';
    requests.fields['readWriteEnglish'] = dbData['readWriteEnglish'] != null? dbData['readWriteEnglish']: '';
    requests.fields['haveSmartPhone'] =  dbData['haveSmartPhone'] != null ? dbData['haveSmartPhone'] : '';
    requests.fields['sourceOfData'] = dbData['sourceOfData'] != null? dbData['sourceOfData']: '';
    requests.fields['f_print'] = dbData['f_print'] != null? dbData['f_print']: '';
    requests.fields['project_id'] = dbData['project_id'] != null ? dbData['project_id'] : '';
    requests.fields['role'] =  dbData['role'] != null ? dbData['role'] : '';
    requests.fields['sname'] = dbData['sname'] != null? dbData['sname']: '';
    requests.fields['synced'] =  dbData['synced'] != null ? dbData['synced'] : '';
    requests.fields['nextOfKinPhone'] = dbData['nextOfKinPhone'] != null? dbData['nextOfKinPhone']: '';
    requests.fields['dobForPaga'] = dbData['dobForPaga'] != null? dbData['dobForPaga']: '';
    requests.fields['lat2'] = dbData['lat2'] != null ? dbData['lat2'] : '';
    requests.fields['lat3'] =  dbData['lat3'] != null ? dbData['lat3'] : '';
    requests.fields['lat4'] = dbData['lat4'] != null? dbData['lat4']: '';
    requests.fields['lat5'] = dbData['lat5'] != null? dbData['lat5']: '';
    requests.fields['lat6'] = dbData['lat6'] != null? dbData['lat6']: '';
    requests.fields['long2'] =  dbData['long2'] != null ? dbData['long2'] : '';
    requests.fields['long3'] = dbData['long3'] != null? dbData['long3']: '';
    requests.fields['long4'] = dbData['long4'] != null ? dbData['long4'] : '';
    requests.fields['long5'] = dbData['long5'] != null ? dbData['long5'] : '';
    requests.fields['long6'] = dbData['long6'] != null ? dbData['long6'] : '';
   requests.fields['coordinates'] =  dbData['coordinate'] != null ? dbData['coordinate'] : '';

    try {
      final streamedResponse = await requests.send();
      final response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = json.decode(response.body);
        _resetState();
        fcmbWallet(dbData);



        //Update local database when successful
        // Get a location using getDatabasesPath

        updateSynced(dbData['id']);

        //result = await helper.updateNote(databaseId);
        setState(() {
          //Successful counter
          success = success + 1;
          print('success $success');
          dialog();
        });
        print(responseData);
        return responseData;
      }
        //print(responseData);
      else {
        final Map<String, dynamic> responseData = json.decode(response.body);
        print(response.body);
        var er = responseData['message'];
        print(er);
        //Fluttertoast.showToast(msg: er);

        if (er == 'Farmer Already Exists!' ||
            er == 'Farmer Already Exists! Duplicate Phone number error.') {
          setState(() {
            //Duplicate counter
            duplicate = duplicate + 1;
            print('duplicate $duplicate');
            dialog();
          });
        } else {
          setState(() {
            //Failed counter
            failed = failed + 1;
            print('failed $failed');
            dialog();
          });
        }
      }
    } catch (e) {
      setState(() {
        _isLoading = false;
      });
      Fluttertoast.showToast(msg: 'Network not available, Kindly Connect');
      print(e);
      return null;
    }
    return null;

  }

  updateSynced(var id) async {

      int updateCount = await helper.updateSynced(id);
      print(updateCount);

  }

  void _resetState() {
    setState(() {
      isUploading = false;
      //file = null;
    });
  }

  //Add data the FCMB
  fcmbWallet(var dbData) async {
    //Api to send FCMB wallet
    bodyFcmb = {
      "first_name": dbData['fname'],
      "last_name": dbData['sname'] ,
      "sourcePhone": '234${dbData['phone'].substring(1, 11)}',
      "gender": dbData['gender'],
      "dob": dbData['dob'],
      "pin":dbData['fcmb_pin'],
    };
    print(data);
    final response = await http.post(baseUrlFCMB + 'fcmb/EnrolCustomerWithPIN',
        headers: {'api-key': apiKey}, body: bodyFcmb);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      print(responseData);
      print(body);
    } else {
      responseData = json.decode(response.body);
     // var er = responseData['message'];
      print('fcmb-error$responseData');
    }
  }


  //Result dialog after it is synced
  _syncDialog() async {
    //Delayed counter reset to make the dialog shows before reset
    Timer(Duration(seconds: 1),(){
      setState(() {
        counter = 0;
        duplicate = 0;
        success = 0;
        failed = 0;
      });
    });

    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Sync results',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),
                        Text(
                          "$counter Synced ",
                          //textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "$success Successful ",
                          //textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "$failed Failed ",
                          //textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "$duplicate Duplicate ",
                          //textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }

}
