import 'package:flutter/material.dart';
import 'package:grainpointapp/const.dart';
import 'package:grainpointapp/registration/registerTwo.dart';

class Register extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Register> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  bool passwordVisible;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Form(
            key: _formKey,
            autovalidate: false,
            child: Column(children: <Widget>[
              Expanded(
                  child: Stack(children: <Widget>[
                Container(
                  height: deviceHeight / 2.0,
                  width: deviceWidth,
                  child: Image.asset(
                    'images/accountHeader.png',
                    fit: BoxFit.cover,
                  ),
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                        padding: EdgeInsets.all(3),
                        height: deviceHeight / 1.9,
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Color(0xffFFFFFF),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15)),
                              // boxShadow: <BoxShadow>[
                            ),
                            child: ListView(children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  left: 24,
                                  top: 40,
                                ),
                                child: Text(
                                  'Create new account',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'First Name',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                      ),
                                    ),
                                    TextFormField(
                                      controller: firstNameController,
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Invalid Name";
                                        }
                                        return null;
                                      },
                                      keyboardType: TextInputType.text,
                                      style: TextStyle(color: Colors.black),
                                      cursorColor: Colors.black,
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'Last name',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                      ),
                                    ),
                                    TextFormField(
                                      controller: lastNameController,
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return "Invalid Name";
                                        }
                                        return null;
                                      },
                                      keyboardType: TextInputType.text,
                                      style: TextStyle(color: Colors.black),
                                      cursorColor: Colors.black,
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'Email Address',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                      ),
                                    ),
                                    TextFormField(
                                      controller: emailController,
                                      validator: (value) {
                                        if (value.isEmpty ||
                                            !value.contains('@') ||
                                            !value.contains('.')) {
                                          return "Invalid Email";
                                        }
                                        return null;
                                      },
                                      keyboardType: TextInputType.emailAddress,
                                      style: TextStyle(color: Colors.black),
                                      cursorColor: Colors.black,
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                  padding: EdgeInsets.all(10),
                                  height: 80,
                                  width: deviceWidth,
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(25.0),
                                      //side: BorderSide(color: Colors.red)
                                    ),
                                    color: appTheme,
                                    onPressed: ()  async{
                                      if (_formKey.currentState.validate()) {
                                        setState(() {
                                          _isLoading = true;
                                        });
                                        await    nextPage(context, email: emailController.text, firstName: firstNameController.text, lastName: lastNameController.text);
                                      }
                                    },
                                    child: _isLoading
                                        ? Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: CircularProgressIndicator(
                                        backgroundColor: Colors.white,
                                      ),
                                    )
                                        : Text(
                                      'Next step',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                  )),
                            ]))))
              ]))
            ])));
  }


  nextPage(BuildContext context,
      {String email, String firstName, String lastName }) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return RegisterTwo(
          email: email, firstName: firstName, lastName: lastName);
    }));
  }
}
