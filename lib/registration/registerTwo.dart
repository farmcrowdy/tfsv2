
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:grainpointapp/const.dart';
import 'package:intl/intl.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:http/http.dart' as http;

class RegisterTwo extends StatefulWidget {
  final String email;
  final String firstName;
  final String lastName;
  RegisterTwo({Key key, this.email, this.firstName, this.lastName}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState(email, firstName, lastName);
}

class _MyAppState extends State<RegisterTwo> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String email;
  final String firstName;
  final String lastName;
  _MyAppState(this.email, this.firstName, this.lastName);
  bool passwordVisible;
  String gender = 'Male';
  var dateExpired;
  var dateOfBirth;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  Map<String, dynamic> responseData;
  var data;
  var token;
  String name;
  String phoneNo;
  String id;
  String status;
  String approved;
  String accessLevel;
  String message;
  var body;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    passwordVisible = true;
  }

  // Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Registering"),
            content: Text(message),
            shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  // Alert dialog
  _success() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Successful! "),
            content: Text("Successfully registered."+ "An FCMB wallet has been created for you and your PIN is " + passwordController.text.trim()),
            shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.popAndPushNamed(context, "/Login");
                },
              )
            ],
          );
        });
  }

  //Http call
  register() async {
    body = {
      "email": email,
      "password": passwordController.text.trim(),
    "firstname": firstName,
    "surname": lastName,
    "dob": dateOfBirth,
    "uphone": phoneController.text,
    "pin": passwordController.text.trim(),
    "gender": gender,
    "client_id": '3',

    };
    print(data);
    final response = await http.post(baseUrl + 'user',
        headers: {'api-key': apiKey},
        body: body);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      fcmbWallet();
//      status = responseData["status"];
//      message = responseData["message"];
//      approved = responseData['approved'];
//      accessLevel = responseData['access_level'];
//      id = responseData['client_id'];
//      email = responseData['data']['user']['email'];
//      await prefs.setString('name', name);
//      await prefs.setString('phoneNo', phoneNo);
//      await prefs.setString('userId', '$id');
//      await prefs.setString('email', '$email');
      // print('token $token and $name');
      print(responseData);
      print(body);
      print(status);
      setState(() {
        _isLoading = false;
      });
    } else {
      setState(() {
        _isLoading = false;
      });
      responseData = json.decode(response.body);
      var er = responseData['message'][0];
      print('error$responseData');
      _showErrorDialog(message: er);
    }
  }
  //Http call
  fcmbWallet() async {
    body = {
      "first_name": firstName,
      "last_name": lastName,
      "sourcePhone": '234${phoneController.text.substring(1,11)}',
      "gender": gender,
      "dob": dateOfBirth,
      "pin": passwordController.text.trim(),
    };
    print(data);
    final response = await http.post(baseUrlFCMB + 'fcmb/EnrolAgentWithPIN',
        headers: {'api-key': apiKey},
        body: body);
    if (response.statusCode == 200) {

      responseData = json.decode(response.body);
      _success();

//      status = responseData["status"];
//      message = responseData["message"];
//      approved = responseData['approved'];
//      accessLevel = responseData['access_level'];
//      id = responseData['client_id'];
//      email = responseData['data']['user']['email'];
//      await prefs.setString('name', name);
//      await prefs.setString('phoneNo', phoneNo);
//      await prefs.setString('userId', '$id');
//      await prefs.setString('email', '$email');
      // print('token $token and $name');
      print(responseData);
      print(body);
      print(status);
    } else {
      setState(() {
        _isLoading = false;
      });
      responseData = json.decode(response.body);
      var er = responseData['message'];
      print('error$responseData');
      _showErrorDialog(message: er);
    }
  }



  @override
  Widget build(BuildContext context) {

    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;

    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;
    return Scaffold(
        body:
        Form(
        key: _formKey,
        autovalidate: false,
        child:
        Column(
            children: <Widget>[
              Expanded(
                  child: Stack(
                      children: <Widget>[
                        Container(
                          height: deviceHeight /2.0,
                          width: deviceWidth,
                          child:
                          Image.asset('images/accountHeader.png', fit: BoxFit.cover,),
                        ),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                                padding: EdgeInsets.all(3),
                                height: deviceHeight / 1.9,
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                child:

                                Container(
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: Color(0xffFFFFFF),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(15),
                                        topRight: Radius.circular(15)),
                                    // boxShadow: <BoxShadow>[
                                  ),
                                 child:
                                      ListView(

                                          children: <Widget>[
Container(
  padding: EdgeInsets.only(left: 24, top: 40,),
  child:
Text('Create new account', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),),),
                                            Container(
                                              padding: EdgeInsets.all(20),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    'Phone Number',
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 15, ),
                                                  ),
                                                  TextFormField(
                                                    controller: phoneController,
                                                     maxLength: 11,
                                                    validator: (value) {
                                                      if (value.isEmpty || value.length < 11) {
                                                        return "Invalid Phone Number";
                                                      }
                                                      return null;
                                                    },
                                                    keyboardType: TextInputType.phone,
                                                    style: TextStyle(color: Colors.black),
                                                    cursorColor: Colors.black,
                                                    decoration: InputDecoration(
                                                      hintText: '08101010101',
                                                      hintStyle: TextStyle(
                                                        color: greyish,
                                                      ),

                                                    ),

                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.all(20),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    'Password',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: 15),
                                                  ),
                                                  TextFormField(
                                                    validator: (value) {
                                                      if (value.isEmpty) {
                                                        return "Invalid Password";
                                                      }
                                                      return null;
                                                    },
                                                    controller: passwordController,
                                                    decoration: InputDecoration(
                                                      suffixIcon: IconButton(
                                                        icon: Icon(
                                                          // Based on passwordVisible state choose the icon
                                                          passwordVisible
                                                              ? Icons.visibility
                                                              : Icons.visibility_off,
                                                          color: Colors.black,
                                                        ),
                                                        onPressed: () {
                                                          // Update the state i.e. toogle the state of passwordVisible variable
                                                          setState(() {
                                                            passwordVisible = !passwordVisible;
                                                          });
                                                        },
                                                      ),

                                                      hintText: '******',
                                                      hintStyle: TextStyle(
                                                        color: Colors.black45,
                                                      ),
                                                      labelStyle: TextStyle(color: Colors.blue),
                                                    ),
                                                    obscureText: passwordVisible,
                                                    keyboardType: TextInputType.visiblePassword,
                                                    style: TextStyle(color: Colors.black),
                                                    cursorColor: Colors.black,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.all(20),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(
                                                    'Gender',
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 15, ),
                                                  ),
                                                      SizedBox(height: 10,),

                                                      Center(
                                                        child: DropdownButtonFormField<String>(

                                                          isExpanded: true,
                                                          value: gender,

                                                          onChanged: (String newValue) {
                                                            setState(() {
                                                              gender = newValue;
                                                            });
                                                          },
                                                          decoration: InputDecoration.collapsed(
                                                              hintText: ''  ),
                                                          items: <String>[
                                                            'Male',
                                                            'Female',
                                                          ].map<DropdownMenuItem<String>>(
                                                                  (String value) {
                                                                return DropdownMenuItem<String>(
                                                                  value: value,
                                                                  child: Text(value),
                                                                );
                                                              }).toList(),
                                                        ),
                                                      ),
                                                 ]
                                              ),
                                            ),

                                                        InkWell(
                                                          onTap: (){
                                                            DatePicker.showDatePicker(
                                                                context,
                                                                showTitleActions:
                                                                true,
                                                                onChanged: (date) {
                                                                  print('change $date in time zone ' +
                                                                      date.toUtc().toString());
                                                                }, onConfirm: (date) {
                                                              setState(() {
                                                                dateExpired = date;
                                                                dateOfBirth =
                                                                    DateFormat(
                                                                        'dd/MM/y')
                                                                        .format(date);
                                                                // age = DateFormat.yMd().format(date);
                                                                // time = date.toLocal();
                                                                print(
                                                                    'confirm $dateExpired');
                                                              });
                                                            },
                                                                currentTime:
                                                                DateTime.now());
                                                          },
                                                          child:

                                                        Container(
                                                            width:200,
                                                            padding:
                                                            EdgeInsets.all(10),
                                                            height: 50.0,
                                                            decoration: BoxDecoration(
                                                                borderRadius:
                                                                BorderRadius.circular(
                                                                    7.0),
                                                                border: Border.all(
                                                                    color: Colors.grey)),
                                                            child: Center(
                                                              child: FlatButton(
                                                                  onPressed: () {

                                                                  },
                                                                  child: Text(
                                                                    dateOfBirth != null
                                                                        ?
                                                                    //time.toString() + ' / ' + dater.toString():
                                                                    dateOfBirth
                                                                        .toString()
                                                                        : 'Date of birth',
                                                                    style: TextStyle(
                                                                        color: Colors.black,
                                                                        fontSize: 15,
                                                                        fontWeight:
                                                                        FontWeight.w500),
                                                                  )),
                                                            )),),

                                            Container(
                                                padding: EdgeInsets.all(10),
                                                height: 80,
                                                width: deviceWidth,
                                                child:
                                                RaisedButton(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius: new BorderRadius
                                                        .circular(25.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  color: appTheme,
                                                  onPressed: ()  async{
    if (_formKey.currentState.validate()) {
    setState(() {
    _isLoading = true;
    });
    await    register();
    }
    },
    child: _isLoading
    ? Padding(
    padding: const EdgeInsets.all(4.0),
    child: CircularProgressIndicator(
    backgroundColor: Colors.white,
    ),
    )
        : Text('Register', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                                                )
                                            ),


                      ]
                  )
              )
                            )
                        )
            ]
        )
              )
    ]
    ))

    );
  }
}
