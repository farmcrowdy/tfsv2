import 'package:grainpointapp/SQL/groupModel.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
class DatabaseGroupHelper {

  static DatabaseGroupHelper _databaseHelper; //Singleton DatabaseHelper
  static Database _database; // Singleton Database
  String colId = "id";
 String groupTable              = "farmersGroup";



  DatabaseGroupHelper._createInstance(); //Named constructor to create instance of DatabaseHelper

  factory DatabaseGroupHelper(){
    if (_databaseHelper == null) {
      _databaseHelper = DatabaseGroupHelper._createInstance();
    }
    return _databaseHelper;
  }

  Future<Database> get database async {

    if (_database == null) {
      _database = await initializeDatabase();
    }
return _database;

  }

  Future<Database> initializeDatabase() async {
    //Get the directory path for both Android and ios to store database.
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + "group.db";

// Open/Create the database at a given path
    var notesDatabase = await openDatabase(
        path, version: 1, onCreate: _createDb);
    return notesDatabase;
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
        'CREATE TABLE $groupTable(id  INTEGER PRIMARY KEY AUTOINCREMENT, groupName TEXT)');

  }
//Fetch Operations: Get all note objects from database
  Future<List<Map<String, dynamic>>> getNoteMapList() async{
    Database db = await this.database;

    //var result = await db.rawQuery('SELECT * FROM $groupTable order by $colPriority ASC' );
    var result = await db.query(groupTable, orderBy: '$colId ASC');
    return result;

  }

  listTables() async {
    Database db = await this.database;
    List<Map<String, dynamic>> tables = await db
        .query(groupTable);
    print('tables$tables');
  }
  getTable(var tables) async {
    Database db = await this.database;
    List<Map<String, dynamic>> tables = await db
        .query(groupTable);
    //print('tables$tables');
  }
  // Insert Operation: Insert a note object to database
  Future<int> insertNote(Group note,) async {
    Database db = await this.database;
    var result = await db.insert(groupTable, note.toMap());
    return result;
  }
  //Update Operation: Update a note object and save it to database
Future<int> updateNote(Group note) async {
    var db = await this.database;
    var result = await db.update(groupTable, note.toMap(), where: '$colId = ?', whereArgs: [note.id]);
    return result;
}

//Delete Operation: Delete a Note object from database
Future<int> deleteNote(int id) async {
    var db = await this.database;
    int result = await db.rawDelete('DELETE FROM $groupTable WHERE $colId  = $colId');
    return result;
}

// Get number of Note object in database

Future<int> getCount() async{
    Database db = await this.database;
    List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $groupTable');
    int result = Sqflite.firstIntValue(x);
    return result;
}
// Get the 'Map List' [List<Map> ] and convert it to 'Note List' [List<Note>]
Future<List<Group>> getGroupList() async{

    var noteMapList = await getNoteMapList(); //Get 'Map List' from Database
  int count = noteMapList.length; //Count the number of map entries in db table

  List<Group> groupList = List<Group>();
  //For loop to create a 'Note List' from a 'Map List'
  for (int i = 0; i < count; i++) {
    groupList.add(Group.fromMapObject(noteMapList[i]));
  }
return groupList;

}

//  Future<List<Group>> getUserModelData() async {
//    var dbClient = await database;
//    String sql;
//    sql = "SELECT * FROM user";
//
//    var result = await dbClient.rawQuery(sql);
//    if (result.length == 0) return null;
//
//    List<Group> list = result.map((item) {
//      return Group.fromMapObject(item);
//    }).toList();
//
//    print(result);
//    return list;
//  }
//


}
//Future<Database> get database async {
//
//  if (_database == null) {
//    _database = await initializeDatabase();
//  }
//  return _database;
//
//}
//Future<int> getCount() async{
//  Database db = await this.database;
//  List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from group1');
//  int result = Sqflite.firstIntValue(x);
//  print('Count$result');
//  return result;
//}
//
//listTables() async {
//  Database db = await this.database;
//  List<Map<String, dynamic>> tables = await db
//      .query('group1');
//  print('tables$tables');
//}
//printTables() async {
//  Database db = await this.database;
//  print(db);
//  var tableNames = (await db.rawQuery('SELECT * FROM app_fluttergrainPoint where type=\'table\' '));
//  print(tableNames);
//}