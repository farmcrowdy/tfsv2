import 'package:grainpointapp/SQL/addFarmerModel.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
class DatabaseAddFarmerHelper {

  static DatabaseAddFarmerHelper _databaseHelper; //Singleton DatabaseHelper
  static Database _database; // Singleton Database
  String colId = "id";
  String colSynced = "synced";
 String farmerTable  = "farmersData";



  DatabaseAddFarmerHelper._createInstance(); //Named constructor to create instance of DatabaseHelper

  factory DatabaseAddFarmerHelper(){
    if (_databaseHelper == null) {
      _databaseHelper = DatabaseAddFarmerHelper._createInstance();
    }
    return _databaseHelper;
  }

  Future<Database> get database async {

    if (_database == null) {
      _database = await initializeDatabase();
    }
return _database;

  }

  Future<Database> initializeDatabase() async {
    //Get the directory path for both Android and ios to store database.
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + "grainPoint.db";

// Open/Create the database at a given path
    var notesDatabase = await openDatabase(
        path, version: 1, onCreate: _createDb);
    return notesDatabase;
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
        'CREATE TABLE $farmerTable(id  INTEGER PRIMARY KEY AUTOINCREMENT,'
        'fcmb_pin TEXT,years_of_experience TEXT,'
        'timestamp TEXT,'
        'lat1  TEXT,'
        'long1  TEXT,'
        'appVersion  TEXT,'
        'fname TEXT,'
        'farmerId  TEXT,'
        'govtString  TEXT,'
        'farm_group_id  TEXT,'
        'bankName TEXT,'
        'bvnNumber TEXT,'
        'idType TEXT,'
        'idNumber  TEXT,'
        'dob TEXT,'
        'gender  TEXT,'
        'phone TEXT,'
        'marital_status TEXT,'
        'crop_proficiency TEXT,'
        'income_range TEXT,'
        'pro_image TEXT,'
        'pro_image_thumbnail TEXT,'
        'acct_number TEXT,'
        'land_area_farmed TEXT,'
        'nextOfKin TEXT,'
        'user_add_id TEXT,'
        'state_id TEXT,'
        'local_id TEXT,'
        'farm_location_id TEXT,'
        'bank_id TEXT,'
        'number_of_dependants TEXT,'
        'uploadedToParent TEXT,'
        'readWriteEnglish TEXT,'
        'sourceOfData TEXT,'
            'haveSmartPhone TEXT,'
        'f_print TEXT,'
        'pimage TEXT,'
        'project_id TEXT,'
        'role TEXT,'
        'sname TEXT,'
        '$colSynced TEXT,'
        'nextOfKinPhone TEXT,'
        'dobForPaga TEXT,'
            'lat2 TEXT,'
        'lat3 TEXT,'
        'lat4 TEXT,'
            'lat5 TEXT,'
            'lat6 TEXT,'
        'long2 TEXT,'
        'long3 TEXT,'
        'long4 TEXT,'
            'long5 TEXT,'
            'long6 TEXT,'
            'coordinate TEXT)');

  }
//Fetch Operations: Get all note objects from database
  Future<List<Map<String, dynamic>>> getNoteMapList() async{
    Database db = await this.database;

    //var result = await db.rawQuery('SELECT * FROM $farmerTable order by $colPriority ASC' );
    var result = await db.query(farmerTable, orderBy: 'timestamp ASC');
    return result;

  }


  // Insert Operation: Insert a note object to database
  Future<int> insertNote(Farmers note,) async {
    Database db = await this.database;
    var result = await db.insert( farmerTable,note.toMap());
    return result;
  }
  //Update Operation: Update a note object and save it to database
Future<int> updateNote(Farmers note) async {
    var db = await this.database;
    var result = await db.update(farmerTable, note.toMap(), where: '$colId = ?', whereArgs: [note.id]);
    return result;
}

  //Update Operation: Update a note object and save it to database
  Future<int> updateSynced(var id) async {
    Map<String, dynamic> row = {
      colSynced : 'True',
    };
    var db = await this.database;
    var result = await db.update(farmerTable, row, where: '$colId = ?', whereArgs: [id]);
    return result;
  }

//Delete Operation: Delete a Note object from database
Future<int> deleteNote(int id) async {
    var db = await this.database;
    int result = await db.rawDelete('DELETE FROM $farmerTable WHERE $colId  = $colId');
    return result;
}

// Get number of Note object in database

Future<int> getCount() async{
    Database db = await this.database;
    List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $farmerTable');
    int result = Sqflite.firstIntValue(x);
    return result;
}
// Get the 'Map List' [List<Map> ] and convert it to 'Note List' [List<Note>]
Future<List<Farmers>> getFarmerList() async{

    var noteMapList = await getNoteMapList(); //Get 'Map List' from Database
  int count = noteMapList.length; //Count the number of map entries in db table

  List<Farmers> farmerList = List<Farmers>();
  //For loop to create a 'Note List' from a 'Map List'
  for (int i = 0; i < count; i++) {
    farmerList.add(Farmers.fromMapObject(noteMapList[i]));
  }
return farmerList;

}

}
//Future<Database> get database async {
//
//  if (_database == null) {
//    _database = await initializeDatabase();
//  }
//  return _database;
//
//}
//Future<int> getCount() async{
//  Database db = await this.database;
//  List<Map<String, dynamic>> x = await db.rawQuery('SELECT COUNT (*) from group1');
//  int result = Sqflite.firstIntValue(x);
//  print('Count$result');
//  return result;
//}
//

//printTables() async {
//  Database db = await this.database;
//  print(db);
//  var tableNames = (await db.rawQuery('SELECT * FROM app_fluttergrainPoint where type=\'table\' '));
//  print(tableNames);
//}