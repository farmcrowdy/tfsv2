class Farmers {
  int _id;
  String _fcmb_pin;
  String _timestamp;
  String _lat1;
  String _long1;
  String _appVersion;
  String _fname;
  String _farmerId;
  String _govtString;
  String _farm_group_id;
  String _bankName;
  String _bvnNumber;
  String _idType;
  String _idNumber;
  String _dob;
  String _gender;
  String _phone;
  String _marital_status;
  String _crop_proficiency;
  String _income_range;
  String _pro_image;
  String _pro_image_thumbnail;
  String _acct_number;
  String _land_area_farmed;
  String _nextOfKin;
  String _user_add_id;
  String _state_id;
  String _local_id;
  String _farm_location_id;
  String _bank_id;
  String _number_of_dependants;
  String _years_of_experience;
  String _uploadedToParent;
  String _readWriteEnglish;
  String _haveSmartPhone;
  String _sourceOfData;
  String _f_print;
  String _pimage;
  String _project_id;
  String _role;
  String _sname;
  String _synced;
  String _nextOfKinPhone;
  String _dobForPaga;
  String _lat2;
  String _lat3;
  String _lat4;
  String _lat5;
  String _lat6;
  String _long2;
  String _long3;
  String _long4;
  String _long5;
  String _long6;
  String _coordinate;


   Farmers(
    //this._id,
    this._fcmb_pin,
    this._timestamp,
    this._lat1,
    this._long1,
    this._appVersion,
    this._fname,
    this._farmerId,
    this._govtString,
    this._farm_group_id,
    this._bankName,
    this._bvnNumber,
    this._idType,
    this._idNumber,
    this._dob,
    this._gender,
    this._phone,
    this._marital_status,
    this._crop_proficiency,
    this._income_range,
    this._pro_image,
    this._pro_image_thumbnail,
    this._acct_number,
    this._land_area_farmed,
    this._nextOfKin,
    this._user_add_id,
    this._state_id,
    this._local_id,
    this._farm_location_id,
    this._bank_id,
    this._number_of_dependants,
    this._years_of_experience,
    this._uploadedToParent,
    this._readWriteEnglish,
    this._haveSmartPhone,
    this._sourceOfData,
    this._f_print,
    this._pimage,
    this._project_id,
    this._role,
    this._sname,
    this._synced,
    this._nextOfKinPhone,
    this._dobForPaga,
    this._lat2,
    this._lat3,
    this._lat4,
       this._lat5,
       this._lat6,
    this._long2,
    this._long3,
    this._long4,
       this._long5,
       this._long6,
      this._coordinate,
  );

  Farmers.withId(
    this._id,
    this._fcmb_pin,
    this._timestamp,
    this._lat1,
    this._long1,
    this._appVersion,
    this._fname,
    this._farmerId,
    this._govtString,
    this._farm_group_id,
    this._bankName,
    this._bvnNumber,
    this._idType,
    this._idNumber,
    this._dob,
    this._gender,
    this._phone,
    this._marital_status,
    this._crop_proficiency,
    this._income_range,
    this._pro_image,
    this._pro_image_thumbnail,
    this._acct_number,
    this._land_area_farmed,
    this._nextOfKin,
    this._user_add_id,
    this._state_id,
    this._local_id,
    this._farm_location_id,
    this._bank_id,
    this._number_of_dependants,
    this._years_of_experience,
    this._uploadedToParent,
    this._readWriteEnglish,
    this._haveSmartPhone,
    this._sourceOfData,
    this._f_print,
    this._pimage,
    this._project_id,
    this._role,
    this._sname,
    this._synced,
    this._nextOfKinPhone,
    this._dobForPaga,
    this._lat2,
    this._lat3,
    this._lat4,
      this._lat5,
      this._lat6,
    this._long2,
    this._long3,
    this._long4,
      this._long5,
      this._long6,
      this._coordinate
  );

  int get id => _id;
  String get fcmb_pin => _fcmb_pin;
  String get timestamp => _timestamp;
  String get lat1 => _lat1;
  String get long1 => _long1;
  String get appVersion => _appVersion;
  String get fname => _fname;
  String get farmerId => _farmerId;
  String get govtString => _govtString;
  String get farm_group_id => _farm_group_id;
  String get bankName => _bankName;
  String get bvnNumber => _bvnNumber;
  String get idType => _idType;
  String get idNumber => _idNumber;
  String get dob => _dob;
  String get gender => _gender;
  String get phone => _phone;
  String get marital_status => _marital_status;
  String get crop_proficiency => _crop_proficiency;
  String get income_range => _income_range;
  String get pro_image => _pro_image;
  String get pro_image_thumbnail => _pro_image_thumbnail;
  String get acct_number => _acct_number;
  String get land_area_farmed => _land_area_farmed;
  String get nextOfKin => _nextOfKin;
  String get user_add_id => _user_add_id;
  String get state_id => _state_id;
  String get local_id => _local_id;
  String get farm_location_id => _farm_location_id;
  String get bank_id => _bank_id;
  String get number_of_dependants => _number_of_dependants;
  String get years_of_experience => _years_of_experience;
  String get uploadedToParent => _uploadedToParent;
  String get readWriteEnglish => _readWriteEnglish;
  String get haveSmartPhone => _haveSmartPhone;
  String get sourceOfData => _sourceOfData;
  String get f_print => _f_print;
  String get pimage => _pimage;
  String get project_id => _project_id;
  String get role => _role;
  String get sname => _sname;
  String get synced => _synced;
  String get nextOfKinPhone => _nextOfKinPhone;
  String get dobForPaga => _dobForPaga;
  String get lat2 => _lat2;
  String get lat3 => _lat3;
  String get lat4 => _lat4;
  String get lat5 => _lat5;
  String get lat6 => _lat6;
  String get long2 => _long2;
  String get long3 => _long3;
  String get long4 => _long4;
  String get long5 => _long5;
  String get long6 => _long6;
  String get coordinate => _coordinate;

  set fcmb_pin(String newfcmb_pin) {
    this._fcmb_pin = newfcmb_pin;
  }

  set timestamp(String newtimestamp) {
    this._timestamp = newtimestamp;
  }

  set lat1(String newlat1) {
    this._lat1 = newlat1;
  }

  set long1(String newlong1) {
    this._long1 = newlong1;
  }

  set appVersion(String newappVersion) {
    this._appVersion = newappVersion;
  }

  set fname(String newfname) {
    this._fname = newfname;
  }

  set farmerId(String newfarmerId) {
    this._farmerId = newfarmerId;
  }

  set govtString(String newgovtString) {
    this._govtString = newgovtString;
  }

  set farm_group_id(String newfarm_group_id) {
    this._farm_group_id = newfarm_group_id;
  }

  set bankName(String newbankName) {
    this._bankName = newbankName;
  }

  set bvnNumber(String newbvnNumber) {
    this._bvnNumber = newbvnNumber;
  }

  set idType(String newidType) {
    this._idType = newidType;
  }

  set idNumber(String newidNumber) {
    this._idNumber = newidNumber;
  }

  set dob(String newdob) {
    this._dob = newdob;
  }

  set gender(String newgender) {
    this._gender = newgender;
  }

  set phone(String newphone) {
    this._phone = newphone;
  }

  set marital_status(String newmarital_status) {
    this._marital_status = newmarital_status;
  }

  set crop_proficiency(String newcrop_proficiency) {
    this._crop_proficiency = newcrop_proficiency;
  }

  set income_range(String newincome_range) {
    this._income_range = newincome_range;
  }

  set pro_image(String newpro_image) {
    this._pro_image = newpro_image;
  }

  set pro_image_thumbnail(String newpro_image_thumbnail) {
    this._pro_image_thumbnail = newpro_image_thumbnail;
  }

  set acct_number(String newacct_number) {
    this._acct_number = newacct_number;
  }

  set land_area_farmed(String newland_area_farmed) {
    this._land_area_farmed = newland_area_farmed;
  }

  set years_of_experience(String newyears_of_experience) {
    this._years_of_experience = newyears_of_experience;
  }

  set user_add_id(String newuser_add_id) {
    this._user_add_id = newuser_add_id;
  }

  set state_id(String newstate_id) {
    this._state_id = newstate_id;
  }

  set local_id(String newlocal_id) {
    this._local_id = newlocal_id;
  }

  set farm_location_id(String newfarm_location_id) {
    this._farm_location_id = newfarm_location_id;
  }

  set bank_id(String newbank_id) {
    this._bank_id = newbank_id;
  }

  set number_of_dependants(String newnumber_of_dependants) {
    this._number_of_dependants = newnumber_of_dependants;
  }

  set uploadedToParent(String newuploadedToParent) {
    this._uploadedToParent = newuploadedToParent;
  }

  set readWriteEnglish(String newreadWriteEnglish) {
    this._readWriteEnglish = newreadWriteEnglish;
  }
  set haveSmartPhone(String newhaveSmartPhone) {
    this._haveSmartPhone = newhaveSmartPhone;
  }

  set sourceOfData(String newsourceOfData) {
    this._sourceOfData = newsourceOfData;
  }

  set f_print(String newf_print) {
    this._f_print = newf_print;
  }

  set pimage(String newpimage) {
    this._pimage = newpimage;
  }

  set project_id(String newproject_id) {
    this._project_id = newproject_id;
  }

  set role(String newrole) {
    this._role = newrole;
  }

  set sname(String newsname) {
    this._sname = newsname;
  }

  set synced(String newsynced) {
    this._synced = newsynced;
  }

  set nextOfKin(String newnextOfKin) {
    this._nextOfKin = newnextOfKin;
  }
  set nextOfKinPhone(String newnextOfKinPhone) {
    this._nextOfKinPhone = newnextOfKinPhone;
  }

  set dobForPaga(String newdobForPaga) {
    this._dobForPaga = newdobForPaga;
  }

  set lat2(String newlat2) {
    this._lat2 = newlat2;
  }

  set lat3(String newlat3) {
    this._lat3 = newlat3;
  }

  set lat4(String newlat4) {
    this._lat4 = newlat4;
  }
  set lat5(String newlat5) {
    this._lat5 = newlat5;
  }
  set lat6(String newlat6) {
    this._lat6 = newlat6;
  }
  set long2(String newlong2) {
    this._long2 = newlong2;
  }

  set long3(String newlong3) {
    this._long3 = newlong3;
  }

  set long4(String newlong4) {
    this._long4 = newlong4;
  }
  set long5(String newlong5) {
    this._long5 = newlong5;
  }
  set long6(String newlong6) {
    this._long6 = newlong6;
  }
  set coordinate(String newcoordinate) {
    this._coordinate = newcoordinate;
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = _id;
    }

    map['fcmb_pin'] = _fcmb_pin;
    map['timestamp'] = _timestamp;
    map['lat1'] = _lat1;
    map['long1'] = _long1;
    map['appVersion'] = _appVersion;
    map['fname'] = _fname;
    map['farmerId'] = _farmerId;
    map['govtString'] = _govtString;
    map['farm_group_id'] = _farm_group_id;
    map['bankName'] = _bankName;
    map['bvnNumber'] = _bvnNumber;
    map['idType'] = _idType;
    map['idNumber'] = _idNumber;
    map['dob'] = _dob;
    map['gender'] = _gender;
    map['phone'] = _phone;
    map['marital_status'] = _marital_status;
    map['crop_proficiency'] = _crop_proficiency;
    map['income_range'] = _income_range;
    map['pro_image'] = _pro_image;
    map['pro_image_thumbnail'] = _pro_image_thumbnail;
    map['acct_number'] = _acct_number;
    map['land_area_farmed'] = _land_area_farmed;
    map['nextOfKin'] = _nextOfKin;
    map['user_add_id'] = _user_add_id;
    map['state_id'] = _state_id;
    map['local_id'] = _local_id;
    map['farm_location_id'] = _farm_location_id;
    map['bank_id'] = _bank_id;
    map['number_of_dependants'] = _number_of_dependants;
    map['years_of_experience'] = _years_of_experience;
    map['uploadedToParent'] = _uploadedToParent;
    map['readWriteEnglish'] = _readWriteEnglish;
    map['haveSmartPhone'] = _haveSmartPhone;
    map['sourceOfData'] = _sourceOfData;
    map['f_print'] = _f_print;
    map['pimage'] = _pimage;
    map['project_id'] = _project_id;
    map['role'] = _role;
    map['sname'] = _sname;
    map['synced'] = _synced;
    map['nextOfKinPhone'] = _nextOfKinPhone;
    map['dobForPaga'] = _dobForPaga;
    map['lat2'] = _lat2;
    map['lat3'] = _lat3;
    map['lat4'] = _lat4;
    map['lat5'] = _lat5;
    map['lat6'] = _lat6;
    map['long2'] = _long2;
    map['long3'] = _long3;
    map['long4'] = _long4;
    map['long5'] = _long5;
    map['long6'] = _long6;
    map['coordinate'] = _coordinate;

    return map;
  }

  Farmers.fromMapObject(Map<String, dynamic> map) {
    this._id = map['id'];
    this._fcmb_pin = map['fcmb_pin'];
    this._timestamp = map['timestamp'];
    this._lat1 = map['lat1'];
    this._long1 = map['long1'];
    this._appVersion = map['appVersion'];
    this._fname = map['fname'];
    this._farmerId = map['farmerId'];
    this._govtString = map['govtString'];
    this._farm_group_id = map['farm_group_id'];
    this._bankName = map['bankName'];
    this._bvnNumber = map['bvnNumber'];
    this._idType = map['idType'];
    this._idNumber = map['idNumber'];
    this._dob = map['dob'];
    this._gender = map['gender'];
    this._phone = map['phone'];
    this._marital_status = map['marital_status'];
    this._crop_proficiency = map['crop_proficiency'];
    this._income_range = map['income_range'];
    this._pro_image = map['pro_image'];
    this._pro_image_thumbnail = map['pro_image_thumbnail'];
    this._acct_number = map['acct_number'];
    this._land_area_farmed = map['land_area_farmed'];
    this._nextOfKin = map['nextOfKin'];
    this._user_add_id = map['user_add_id'];
    this._state_id = map['state_id'];
    this._local_id = map['local_id'];
    this._farm_location_id = map['farm_location_id'];
    this._bank_id = map['bank_id'];
    this._number_of_dependants = map['number_of_dependants'];
    this._years_of_experience = map['years_of_experience'];
    this._uploadedToParent = map['uploadedToParent'];
    this._readWriteEnglish = map['readWriteEnglish'];
    this._haveSmartPhone = map['haveSmartPhone'];
    this._sourceOfData = map['sourceOfData'];
    this._f_print = map['f_print'];
    this._pimage = map['pimage'];
    this._project_id = map['project_id'];
    this._role = map['role'];
    this._sname = map['sname'];
    this._synced = map['synced'];
    this._nextOfKinPhone = map['nextOfKinPhone'];
    this._dobForPaga = map['dobForPaga'];
    this._lat2 = map['lat2'];
    this._lat3 = map['lat3'];
    this._lat4 = map['lat4'];
    this._lat5 = map['lat5'];
    this._lat6 = map['lat6'];
    this._long2 = map['long2'];
    this._long3 = map['long3'];
    this._long4 = map['long4'];
    this._long5 = map['long5'];
    this._long6 = map['long6'];
    this._coordinate = map['coordinate'];

  }
}
////convert to the string "TRUE"
//string StringValue = boolValue.ToString;
//And back to boolean:
//
////convert the string back to boolean
//bool Boolvalue = Convert.ToBoolean(StringValue);