import 'package:flutter/material.dart';
import 'dart:ui';

import 'package:flutter/cupertino.dart';



const appTheme = const Color(0xFFABBA16);
const appTheme2 = const Color(0xFF469b83);
const greyish = const Color(0xFF8F92A1);
const appBackground = const Color(0xFFF9F9F9);

 const Color loginGradientStart = const Color(0xFF315da5);
 const Color loginGradientEnd = const Color(0xFF469b83);

 const appThemes = const LinearGradient(
  colors: const [loginGradientStart, loginGradientEnd],
  stops: const [0.0, 1.0],
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
);