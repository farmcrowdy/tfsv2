import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/SQL/addFarmerModel.dart';
import 'package:grainpointapp/SQL/database_helper.dart';
import 'package:grainpointapp/const.dart';
import 'package:location/location.dart';

class FarmingHistory extends StatefulWidget {
  final String projectId,
      firstName,
      lastName,
      phoneNo,
      dob,
      martial,
      gender,
      nextKin,
      nextKinNo,
      cropFarmerProduces,
  accountNumber,bankName;
  final String farmerGroup,
      localG,
      pro_image,
      pro_image_thumbnail,
      bvn,
      methodIdentification,
      idCardNumber,
      farmerRole,
      writeEnglish,
      haveSmartPhone;
  final int numberDependent, stateId, localGId, bankId;
  final File farmerImage;


  const FarmingHistory(
      {Key key,
      this.projectId,
      this.firstName,
      this.lastName,
      this.phoneNo,
      this.dob,
      this.martial,
      this.gender,
      this.nextKin,
      this.nextKinNo,
      this.cropFarmerProduces,
      this.farmerGroup,
      this.localG,
      this.pro_image,
      this.pro_image_thumbnail,
      this.bvn,
      this.methodIdentification,
      this.idCardNumber,
      this.farmerRole,
      this.writeEnglish,
      this.haveSmartPhone,
      this.numberDependent,
      this.stateId,
      this.localGId,
      this.bankId,
      this.farmerImage, this.accountNumber, this.bankName})
      : super(key: key);

  @override
  _MyAppState createState() => _MyAppState(
      this.projectId,
      this.firstName,
      this.lastName,
      this.phoneNo,
      this.dob,
      this.martial,
      this.gender,
      this.nextKin,
      this.nextKinNo,
      this.cropFarmerProduces,
      this.farmerGroup,
      this.localG,
      this.pro_image,
      this.pro_image_thumbnail,
      this.bvn,
      this.methodIdentification,
      this.idCardNumber,
      this.farmerRole,
      this.writeEnglish,
      this.haveSmartPhone,
      this.numberDependent,
      this.stateId,
      this.localGId,
      this.bankId,
      this.farmerImage,this.accountNumber, this.bankName);
}

class _MyAppState extends State<FarmingHistory> {
  final String projectId,
      firstName,
      lastName,
      phoneNo,
      dob,
      martial,
      gender,
      nextKin,
      nextKinNo,
      cropFarmerProduces,
  accountNumber,bankName;
  final String farmerGroup,
      localG,
      pro_image,
      pro_image_thumbnail,
      bvn,
      methodIdentification,
      idCardNumber,
      farmerRole,
      writeEnglish,
      haveSmartPhone;
  final int numberDependent, stateId, localGId, bankId;
  final File farmerImage;

  _MyAppState(
      this.projectId,
      this.firstName,
      this.lastName,
      this.phoneNo,
      this.dob,
      this.martial,
      this.gender,
      this.nextKin,
      this.nextKinNo,
      this.cropFarmerProduces,
      this.farmerGroup,
      this.localG,
      this.pro_image,
      this.pro_image_thumbnail,
      this.bvn,
      this.methodIdentification,
      this.idCardNumber,
      this.farmerRole,
      this.writeEnglish,
      this.haveSmartPhone,
      this.numberDependent,
      this.stateId,
      this.localGId,
      this.bankId,
      this.farmerImage, this.accountNumber, this.bankName);

  TextEditingController landController = TextEditingController();
  TextEditingController pinController = TextEditingController();
  TextEditingController yearsController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String incomeRange = '0-99,000';
  bool _isLoading = false;
  String farmerId;
  String lastTime =  DateTime.now().millisecondsSinceEpoch.toString().substring(6);
  var location = Location();
  String lat1;
  String lat2;
  String lat3;
  String lat4;
  String lat5;
  String lat6;
  String long1;
  String long2;
  String long3;
  String long4;
  String long5;
  String long6;
  String fingerPrint;

  // Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Submitting"),
            content: Text(message),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  _showDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Your registration is successful, \n Farmers ID: $farmerId \n FCMB Pin: ${pinController.text}"),
            content: Text(message),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Done"),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                 //Navigator.pushReplacementNamed(context, '/Home');
                },
              ),
              FlatButton(
                child: Text("Redo"),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                //  Navigator.pushReplacementNamed(context, '/SelectClient');
                },
              )
            ],
          );
        });
  }

  Farmers farmer;
  DatabaseAddFarmerHelper helper = DatabaseAddFarmerHelper();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    farmerId = 'FCDY/'+id+'/'+lastTime;

  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Form(
          key: _formKey,
          autovalidate: false,
          child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Farm History',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Enter detail of land area farmed previously.',
                  style: TextStyle(
                      color: greyish,
                      fontSize: 20,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: 'Land area in Hectares',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        TextFormField(
                          controller: landController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Invalid";
                              }
                              return null;
                            },
//
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w500),
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              hintText: '',
                              hintStyle: TextStyle(
                                  color: greyish, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: 'Years of experience',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        TextFormField(
                          controller: yearsController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Invalid Years";
                              }
                              return null;
                            },
//
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w500),
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              hintText: '',
                              hintStyle: TextStyle(
                                  color: greyish, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Income range before this moment',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                height: 60.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7.0),
                                    border: Border.all(color: Colors.grey)),
                                child: Center(
                                  child: DropdownButtonFormField<String>(
                                    isExpanded: false,
                                    value: incomeRange,
                                    onChanged: (String newValue) {
                                      setState(() {
                                        incomeRange = newValue;
                                      });
                                    },
                                    decoration:
                                        InputDecoration.collapsed(hintText: ''),
                                    items: <String>[
                                      '0-99,000',
                                      '100,000-500,000',
                                      '> 1,000,000',
                                    ].map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ]),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: 'Enter your Pin',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        TextFormField(
                          controller: pinController,
                            validator: (value) {
                              if (value.isEmpty || value.length != 4) {
                                return "Invalid pin";
                              }
                              return null;
                            },
                          maxLength: 4,

                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w500),
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              hintText: '',
                              hintStyle: TextStyle(
                                  color: greyish, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                ),

                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: 'Get farm coordinates',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            text: 'It requires you to go round the farm land and tap on the icons accordingly.',
                            style: TextStyle(
                                color: greyish,
                                fontWeight: FontWeight.w500,
                                fontSize: 15),
                          ),
                        ),
                    SizedBox(height: 20,),
                        InkWell(
                          onTap: () {
                            getLatitude1();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),

                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point A',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      lat1 != null
                                          ? Text(
                                        'Lat: $lat1, Long: $long1',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                          : Text(
                                        'Tap to get point A coordinate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude2();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point B',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      lat2 != null
                                          ? Text(
                                        'Lat: $lat2, Long: $long2',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                          : Text(
                                        'Tap to get point B coordinate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude3();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point C',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      lat3 != null
                                          ? Text(
                                        'Lat: $lat3, Long: $long3',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                          : Text(
                                        'Tap to get point C coordinate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude4();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point D',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      lat4 != null
                                          ? Text(
                                        'Lat: $lat4, Long: $long4',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                          : Text(
                                        'Tap to get point D coordinate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude5();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point E',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      lat5 != null
                                          ? Text(
                                        'Lat: $lat5, Long: $long5',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                          : Text(
                                        'Tap to get point E coordinate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude6();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point F',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      lat6 != null
                                          ? Text(
                                        'Lat: $lat6, Long: $long6',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                          : Text(
                                        'Tap to get point F coordinate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),


SizedBox(height: 20,),
InkWell(
  onTap: _readFingerPrint,
  child:  Container(
    padding: EdgeInsets.all(10),
    color: Colors.black,
    child:
    Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text('Tap to get finger print', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),

              fingerPrint != null?
              Text('Finger print taken', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),):Text('Fingerprint empty', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
                ],
          ),
          Icon(
            Icons.fingerprint, color: Colors.white,
          )
        ]
    ),
  ) ,
),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                ),

                SizedBox(
                  height: 100,
                ),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 80,
                    width: deviceWidth,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        //side: BorderSide(color: Colors.red)
                      ),
                      color: appTheme,
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            _isLoading = true;
                          });
                             await  _getFingerString();
                        }
                      },
                      child: _isLoading
                          ? Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                          : Text(
                              'Create farmer\'s profile Project',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                    )),
              ]))),
    );
  }

  save() async {

    farmer = Farmers(
      '','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');

    farmer.fcmb_pin = pinController.text;
    farmer.timestamp = lastTime;
    farmer.lat1 = lat1;
    farmer.long1 = long1;
    farmer.appVersion = '28';
    farmer.fname = firstName;
    farmer.farmerId = farmerId;
    farmer.govtString = localG;
    farmer.farm_group_id = farmerGroup;
    farmer.bankName = bankName;
    farmer.bvnNumber = bvn;
    farmer.idType = methodIdentification;
    farmer.idNumber = idCardNumber;
    farmer.dob = dob;
    farmer.gender = gender;
    farmer.phone = phoneNo;
    farmer.marital_status = martial;
    farmer.crop_proficiency = cropFarmerProduces;
    farmer.income_range = incomeRange;
    farmer.pro_image = pro_image;
    farmer.pro_image_thumbnail = pro_image_thumbnail;
    farmer.acct_number = accountNumber ;
    farmer.land_area_farmed = landController.text;
    farmer.nextOfKin = nextKin;
    farmer.user_add_id = id;
    farmer.state_id = stateId.toString();
    farmer.local_id = localGId.toString();
    farmer.farm_location_id = '0';
    farmer.bank_id = bankId.toString();
    farmer.number_of_dependants = numberDependent.toString();
    farmer.years_of_experience = yearsController.text;
    farmer.uploadedToParent = 'False';
    farmer.readWriteEnglish = writeEnglish;
    farmer.haveSmartPhone = haveSmartPhone;
    farmer.sourceOfData = '0';
    farmer.f_print = fingerPrint;
    farmer.pimage = farmerImage.toString();
    farmer.project_id = projectId;
    farmer.role = farmerRole;
    farmer.sname = lastName;
    farmer.synced = 'False';
    farmer.nextOfKinPhone = nextKinNo;
    farmer.dobForPaga = dob;
    farmer.lat2 = lat2;
    farmer.lat3 = lat3;
    farmer.lat4 = lat4;
    farmer.lat5 = lat5;
    farmer.lat6 = lat6;
    farmer.long2 = long2;
    farmer.long3 = long3;
    farmer.long4 = long4;
    farmer.long5 = long5;
    farmer.long6 = long6;
    farmer.coordinate = 'Lat - $lat, Long - $lng';
    int result;
    print(result);
    if (farmer.id != null) {
      print(farmer.id);
      _isLoading = false;
      Fluttertoast.showToast(msg: 'Farmer already exist');

    } else {
      // Case 2: Insert// Operation
      result = await helper.insertNote(farmer);
    }

    if (result != 0) {
      //Success
      _isLoading = false;
      _showDialog();
      Fluttertoast.showToast(msg: 'Farmer saved successfully to the local database');
    } else {
      // Failure
      _isLoading = false;
      Fluttertoast.showToast(msg: 'Problem Saving Farmer');
    }
  }


  // Getting farmers FingerPrint
  static const platform = const MethodChannel('samples.flutter.dev/readFingerPrint');
  static const platform2 = const MethodChannel('samples.flutter.dev/finger');


  Future<void> _readFingerPrint() async {
    try {
      final String result = await platform.invokeMethod('readFingerPrint');
      setState(() {
        fingerPrint = result;
        print(fingerPrint);
      });

    } on PlatformException catch (e) {

      print( e.message);
    }
  }

  Future<void> _getFingerString() async {
    try {

      final String result = await platform2.invokeMethod('getFingerPrint');
      setState(() {
        fingerPrint = result;
        print(fingerPrint);
        save();
      });
    } on PlatformException catch (e) {
      print(e.message);
    }
  }


  // Getting farmers Coordinates functions

  Future getLatitude1() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat1 = currentLocation.latitude.toString();
        long1 = currentLocation.longitude.toString();

        print('lat1$lat1');

      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }

  Future getLatitude2() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat2 = currentLocation.latitude.toString();
        long2 = currentLocation.longitude.toString();
        print('lat1$lat2');
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }

  Future getLatitude3() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat3 = currentLocation.latitude.toString();
        long3 = currentLocation.longitude.toString();
        print('lat1$lat3');
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }

  Future getLatitude4() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat4 = currentLocation.latitude.toString();
        long4 = currentLocation.longitude.toString();
        print('lat1$lat4');
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }
  Future getLatitude5() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat5 = currentLocation.latitude.toString();
        long5 = currentLocation.longitude.toString();
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }
  Future getLatitude6() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat6 = currentLocation.latitude.toString();
        long6 = currentLocation.longitude.toString();
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }
}
