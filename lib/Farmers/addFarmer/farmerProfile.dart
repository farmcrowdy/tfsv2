import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Farmers/addFarmer/bankIdentification.dart';
import 'package:grainpointapp/SQL/groupModel.dart';
import 'package:grainpointapp/SQL/group_database_helper.dart';
import 'package:grainpointapp/const.dart';
import 'package:image/image.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as Im;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid_enhanced/uuid.dart';

class FarmerProfile extends StatefulWidget {
  final String projectId;

  const FarmerProfile({Key key, this.projectId}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState(projectId);
}

class _MyAppState extends State<FarmerProfile> {
  String gender = 'Male';
  String martial = 'Single';
  String crop = 'Choose crop';
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController dependantController = TextEditingController();
  TextEditingController nextOfKinController = TextEditingController();
  TextEditingController nextOfKinNumberController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  var dateBirth;
  var dateString;
  File file;
  String fileName;
  List state = List();
  List lg = List();

  var farmGroup;
  var group;
  String stateId;
  String lgIg;
  String govtString;
  String pro_image_thumbnail;
  String pro_image;
  String postId = new Uuid.randomUuid().toString();
  bool _isLoading = false;
  final String projectId;
  _MyAppState(this.projectId);

  DatabaseGroupHelper db = DatabaseGroupHelper();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getState();
    getLg();
    setState(() {
      group =  db.getGroupList();
    });

  }

  void getState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      state = json.decode(prefs.getString("state"));
    });
  }

  void getLg() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      lg = json.decode(prefs.getString("lg"));
    });
  }

  handleTakePhoto() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 960, maxHeight: 675);
    setState(() {
      this.file = file;
    });
    print(file.path);
    setState(() {
      pro_image_thumbnail = file.path;
      pro_image = '"farmer-${firstNameController.text}-${lastNameController.text}.jpg';
    });
  }

  handleChooseFromGallery() async {
    Navigator.pop(context);
    File file = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 960, maxHeight: 675);
    setState(() {
      fileName = file.path.split('/').last;
      this.file = file;
      final _imageFile = decodeImage(
        file.readAsBytesSync(),
      );
      print(file.path);
      setState(() {
        pro_image_thumbnail = file.path;
        pro_image = '"farmer-${firstNameController.text}-${lastNameController.text}.jpg';
        String base64Image = base64Encode(encodePng(_imageFile));
      });
    });
  }

  selectImage(parentContext) {
    return showDialog(
        context: parentContext,
        builder: (context) {
          return SimpleDialog(
            title: Text('Upload Display Picture'),
            children: <Widget>[
              SimpleDialogOption(
                child: Text('Photo with Camera'),
                onPressed: handleTakePhoto,
              ),
              SimpleDialogOption(
                child: Text('Image from Gallery'),
                onPressed: handleChooseFromGallery,
              ),
              SimpleDialogOption(
                child: Text('Cancel'),
                onPressed: () => Navigator.pop(context),
              )
            ],
          );
        });
  }

  compressImage() async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    Im.Image imageFile = Im.decodeImage(file.readAsBytesSync());
    final compressedImageFile = File(
        '$path/"farmer-${firstNameController.text}-${lastNameController.text}.jpg')
      ..writeAsBytesSync(Im.encodeJpg(imageFile, quality: 85));
    setState(() {
      file = compressedImageFile;
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Form(
          key: _formKey,
          autovalidate: false,
          child: Container(
              padding: EdgeInsets.all(20),
              child:
              SingleChildScrollView(
              child:
              Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Farmer\'s Profile',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Enter the farmer\'s personal details',
                  style: TextStyle(
                      color: greyish,
                      fontSize: 20,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
//                        height: 20,
                      ),

                      Text(
                        'FIRST NAME',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: firstNameController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Invalid Name";
                          }
                          return null;
                        },
                        // maxLength: 11,
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: 'Jonathan',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'LAST NAME',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: lastNameController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Invalid  Name";
                          }
                          return null;
                        },
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: 'Doe',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'PHONE NUMBER',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: phoneNoController,
                        validator: (value) {
                          if (value.isEmpty || value.length < 11) {
                            return "Invalid Phone number";
                          }
                          return null;
                        },
                        // maxLength: 11,
                        keyboardType: TextInputType.phone,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        maxLength: 11,
                        decoration: InputDecoration(
                            hintText: '08101010101',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),

                    SizedBox(
                      height: 20,
                    ),
                    Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'DATE OF BIRTH',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        onTap: () {
                          DatePicker.showDatePicker(context,
                              showTitleActions: true, onChanged: (date) {
                            print('change $date in time zone ' +
                                date.toUtc().toString());
                          }, onConfirm: (date) {
                            setState(() {
                              dateBirth = date;
                              //06-Jun-96
                              //dateString = DateFormat('dd-MMM-yy').format(date);
                              dateString = DateFormat('dd/MM/y').format(date);
                              // age = DateFormat.yMd().format(date);
                              // time = date.toLocal();
                              print('confirm $dateBirth');
                            });
                          }, currentTime: DateTime.now());
                        },
                        child: Container(
                            width: deviceWidth,
                            padding: EdgeInsets.all(10),
                            height: 50.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7.0),
                                border: Border.all(color: Colors.grey)),
                            child: Center(
                              child: FlatButton(
                                  onPressed: () {

                                    DatePicker.showDatePicker(context,
                                        showTitleActions: true, onChanged: (date) {
                                          print('change $date in time zone ' +
                                              date.toUtc().toString());
                                        }, onConfirm: (date) {
                                          setState(() {
                                            dateBirth = date;
                                            //06-Jun-96
                                            //dateString = DateFormat('dd-MMM-yy').format(date);
                                            dateString = DateFormat('dd/MM/y').format(date);
                                            // age = DateFormat.yMd().format(date);
                                            // time = date.toLocal();
                                            print('confirm $dateBirth');
                                          });
                                        }, currentTime: DateTime.now());
                                  },
                                  child: Text(
                                    dateString != null
                                        ?
                                        //time.toString() + ' / ' + dater.toString():
                                        dateString.toString()
                                        : 'Date of birth',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                  )),
                            )),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                FittedBox(
                  child:
                Row(
                  children: [
                    Container(
                      width: deviceWidth / 2.2,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'MARTIAL STATUS',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: DropdownButtonFormField<String>(
                                isExpanded: false,
                                value: martial,
                                onChanged: (String newValue) {
                                  setState(() {
                                    martial = newValue;
                                  });
                                },
                                decoration:
                                    InputDecoration.collapsed(hintText: ''),
                                items: <String>[
                                  'Single',
                                  'Married',
                                  'Divorced',
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ),
                          ]),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: deviceWidth / 2.2,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'GENDER',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: DropdownButtonFormField<String>(
                                isExpanded: false,
                                value: gender,
                                onChanged: (String newValue) {
                                  setState(() {
                                    gender = newValue;
                                  });
                                },
                                decoration:
                                    InputDecoration.collapsed(hintText: ''),
                                items: <String>[
                                  'Male',
                                  'Female',
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ),
                          ]),
                    ),
                  ],
                ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'NUMBER OF DEPENDANTS',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: dependantController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Invalid";
                          }
                          return null;
                        },
                        // maxLength: 11,
                        keyboardType: TextInputType.number,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: '3',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'NEXT OF KIN NAME',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: nextOfKinController,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Invalid Name";
                          }
                          return null;
                        },
                        // maxLength: 11,
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: 'George Doe',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.w500)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'NEXT OF KIN\'S PHONE NUMBER',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: nextOfKinNumberController,
                        validator: (value) {
                          if (value.isEmpty || value.length < 11) {
                            return "Invalid Phone number";
                          }
                          return null;
                        },
                        // maxLength: 11,
                        keyboardType: TextInputType.phone,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                        maxLength: 11,
                        decoration: InputDecoration(
                            hintText: '08101010101',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'CROP FARMER PRODUCES',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Center(
                          child: DropdownButtonFormField<String>(
                            isExpanded: true,
                            value: crop,
                            onChanged: (String newValue) {
                              setState(() {
                                crop = newValue;
                              });
                            },
                            decoration: InputDecoration.collapsed(hintText: ''),
                            items: <String>[
                              'Choose crop',
                              'Rice',
                              'Maize',
                              'Sorghum',
                              'Cassava',
                              'Millet',
                              'Tomatoes',
                              'Palm fruit',
                              'Ginger',
                              'Soybean',
                              'Pineapple',
                              'Watermelon',
                              'Cocoyam',
                              'Melon',
                              'Plantain',
                              'Cocoa',
                              'Yam',
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      ]),
                ),
                SizedBox(
                  height: 30,
                ),

                FittedBox(
                  child:
                Row(
                  children: [
                    Container(

                      width: deviceWidth / 2.2,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'STATE',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                                child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    height: 60.0,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                        border: Border.all(color: Colors.grey)),
                                    child: Center(
                                      child: new DropdownButtonFormField(
                                        decoration: InputDecoration.collapsed(
                                            hintText: ''),
                                        isExpanded: true,
                                        
                                        validator: (value) => value == null ? 'Select State' : null,
                                        items: state.map((item) {
                                          return new DropdownMenuItem(
                                            child: new Text(item['name']),
                                            value: item,
                                          );
                                        }).toList(),
                                        onChanged: (newVal) {
                                          setState(() {
                                            stateId = newVal['state_id'];
                                            print(stateId);
                                          });
                                        },
                                        //value: unitPrice,
                                      ),
                                    ))),
                          ]),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: deviceWidth / 2.2,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'LGA',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                                child: Container(
                                    padding: EdgeInsets.only(left: 10),
                                    height: 60.0,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                        border: Border.all(color: Colors.grey)),
                                    child: Center(
                                      child: new DropdownButtonFormField(
                                        decoration: InputDecoration.collapsed(
                                            hintText: ''),
                                        isExpanded: true,
                                        validator: (value) => value == null ? 'Choose LGA' : null,
                                        items: lg
                                            .where(
                                                (i) => i['state_id'] == stateId)
                                            .map((item) {
                                          return new DropdownMenuItem(
                                            child: new Text(item['local_name']),
                                            value: item,
                                          );
                                        }).toList(),
                                        onChanged: (newVal) {
                                          setState(() {
                                            lgIg = newVal['local_id'];
                                            lg = newVal['local_name'];
                                            print(lgIg);
                                            lg.clear();
                                            lg.add('value');
                                          });
                                        },
                                        //value: lgIg,
                                      ),
                                    ))),
                          ]),
                    ),
                  ],
                ),
                ),

                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'DESIGNATED FARM GROUP',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
//
//                      Center(
//                        child: Container(
//                            padding: EdgeInsets.only(left: 10),
//                            height: 60.0,
//                            decoration: BoxDecoration(
//                                borderRadius: BorderRadius.circular(7.0),
//                                border: Border.all(color: Colors.grey)),
//                            child: Center(
//                              child: new DropdownButtonFormField(
//                                decoration:
//                                InputDecoration.collapsed(hintText: ''),
//                                isExpanded: true,
//                                items: farmGroup.map((item) {
//                                  return new DropdownMenuItem(
//                                    child: new Text(item['groupName']),
//                                    value: item['local_id'],
//                                  );
//                                }).toList(),
//                                onChanged: (newVal) {
//                                  setState(() {
//                                    print(lgIg);
//                                  });
//                                },
//                                value: lgIg,
//                              ),
//                            ))
//                      ),

                            FutureBuilder<List<Group>>(
                                future:group,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Group>> snapshot) {
                                  if (!snapshot.hasData){
                                    return Text(
                                      'Farmer group empty, kindly add',
                                      style: TextStyle(color: Colors.red),
                                    );
                                  }
                                  if (snapshot.data.isEmpty){
                                    Timer(Duration(seconds: 1), (){
                                      Navigator.pushReplacementNamed(context, '/Home');
                                    });
                                    Fluttertoast.showToast(msg: 'Kindly create farmers group first');
                                    return Text(
                                      'Farmer group empty, kindly add',
                                      style: TextStyle(color: Colors.red),
                                    );

                                  }

                                  return Center(
                                      child: Container(
                                          padding: EdgeInsets.only(left: 10),
                                          height: 60.0,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(7.0),
                                              border:
                                              Border.all(color: Colors.grey)),
                                          child: Center(
                                              child: new DropdownButtonFormField<
                                                  Group>(
                                                decoration: InputDecoration.collapsed(
                                                    hintText: ''),
                                                isExpanded: true,
                                                items: snapshot.data
                                                    .map((item) => DropdownMenuItem(
                                                  child: Text(item.groupName),
                                                  value: item,
                                                ))
                                                    .toList(),
                                                onChanged: (Group value) {
                                                  print(value.groupName);
                                                  farmGroup = value.groupName;
                                                },
                                                //value: Group,
                                              ))));
                                }),
                          ]),
                    ),
                SizedBox(
                  height: 30,
                ),
                Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      file == null
                          ? GestureDetector(
                              onTap: () {
                                //handleChooseFromGallery();
                                selectImage(context);
                              },
                              child: CircleAvatar(
                                radius: 50,
                                backgroundImage: AssetImage('images/dpPlaceholder.png'),
                              ))
                          : GestureDetector(
                              onTap: () {
                                //handleChooseFromGallery();
                                selectImage(context);
                              },
                              child: Container(
                                  width: 100.0,
                                  height: 100.0,
                                  child: new CircleAvatar(
                                    radius: 50,
                                    backgroundImage: new FileImage(file),
                                  )),
                            ),
                    ]),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 80,
                    width: deviceWidth,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        //side: BorderSide(color: Colors.red)
                      ),
                      color: appTheme,
                      onPressed: () async {
                        if (
                        dateString != null &&
                            crop != 'Choose crop' &&
                            farmGroup != null &&
                            stateId != null &&
                            lgIg != null
                        && file != null && firstNameController.text.isNotEmpty && lastNameController.text.isNotEmpty) {
                          if (_formKey.currentState.validate()) {
                            setState(() {
                              _isLoading = true;
                            });
                            await navigateToBankIdentification(
                                projectId,
                                firstNameController.text,
                                lastNameController.text,
                                phoneNoController.text,
                                dateString,
                                martial,
                                gender,
                                int.parse(dependantController.text),
                                nextOfKinController.text,
                                nextOfKinNumberController.text,
                                crop,
                                farmGroup,
                                int.parse(stateId),
                                int.parse(lgIg),
                                govtString,
                                pro_image,
                                pro_image_thumbnail,
                                file);
                          }
                        }
                      else {
                          Fluttertoast.showToast(
                              msg: 'All fields are required');
                        }
                      },

                      child: Text(
                        'Next',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    )),
              ]))),
      ),
    );
  }

  navigateToBankIdentification(
    String projectId,
    String firstName,
    String lastName,
    String phoneNo,
    String dob,
    String martial,
    String gender,
    int numberDependent,
    String nextKin,
    String nextKinNo,
    String cropFarmerProduces,
    String farmerGroup,
    int stateId,
    int localGId,
    String localG,
    String pro_image,
    String pro_image_thumbnail,
    File farmerImage,
  ) async {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return BankIdentification(
        projectId: projectId,
        firstName: firstName,
        lastName: lastName,
        phoneNo: phoneNo,
        dob: dob,
        martial: martial,
        gender: gender,
        numberDependent: numberDependent,
        nextKin: nextKin,
        nextKinNo: nextKinNo,
        cropFarmerProduces: cropFarmerProduces,
        farmerGroup: farmerGroup,
        stateId: stateId,
        localGId: localGId,
        localG: localG,
        pro_image: pro_image,
        pro_image_thumbnail: pro_image_thumbnail,
        farmerImage: farmerImage,
      );
    }));
  }

}
