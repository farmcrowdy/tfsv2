import 'dart:convert';
import 'dart:io';

import 'package:custom_switch/custom_switch.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:grainpointapp/Farmers/addFarmer/farmingHistory.dart';
import 'package:grainpointapp/const.dart';
import 'package:shared_preferences/shared_preferences.dart';



class BankIdentification extends StatefulWidget {
 final String projectId,  firstName,  lastName, phoneNo, dob, martial,  gender, nextKin, nextKinNo, cropFarmerProduces;
 final String farmerGroup, localG, pro_image, pro_image_thumbnail;
 final int numberDependent,stateId,localGId;
 final File farmerImage;

 const BankIdentification({Key key, this.projectId, this.firstName, this.lastName, this.phoneNo, this.dob, this.martial, this.gender, this.nextKin, this.nextKinNo, this.cropFarmerProduces, this.farmerGroup, this.localG, this.pro_image, this.pro_image_thumbnail, this.numberDependent, this.stateId, this.localGId, this.farmerImage}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState(this.projectId, this.firstName, this.lastName, this.phoneNo, this.dob, this.martial, this.gender, this.nextKin, this.nextKinNo, this.cropFarmerProduces, this.farmerGroup, this.localG, this.pro_image, this.pro_image_thumbnail, this.numberDependent, this.stateId, this.localGId, this.farmerImage);
}

class _MyAppState extends State<BankIdentification> {
  final String projectId,  firstName,  lastName, phoneNo, dob, martial,  gender, nextKin, nextKinNo, cropFarmerProduces;
  final String farmerGroup, localG, pro_image, pro_image_thumbnail;
  final int numberDependent,stateId,localGId;
  final File farmerImage;

  _MyAppState(this.projectId, this.firstName, this.lastName, this.phoneNo, this.dob, this.martial, this.gender, this.nextKin, this.nextKinNo, this.cropFarmerProduces, this.farmerGroup, this.localG, this.pro_image, this.pro_image_thumbnail, this.numberDependent, this.stateId, this.localGId, this.farmerImage);


  TextEditingController bvnController = TextEditingController();
  TextEditingController idCardController = TextEditingController();
  TextEditingController accountController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String identificationType = 'National ID card';
  String farmerRole = 'Chairman';
  var banks = List();
  var bankId;
  var bankName;
  bool canRead = false;
  bool smartPhone = false;
  bool _isLoading = false;
  void getBanks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      print(prefs.getString("banks"));
    });
    banks =  json.decode(prefs.getString("banks"));

  }





  //Http call

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(firstName);
    print(farmerImage);
    print(farmerGroup);
    getBanks();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: false);

    return Scaffold(
      body:
      Form(
          key: _formKey,
          autovalidate: false,
          child:
          Container(
              padding: EdgeInsets.all(20),
              child:
              ListView(
                  children: <Widget>[
                    SizedBox(height: 10,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [

                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(Icons.arrow_back, size: 40,),
                          )
                        ],
                      ),
                    ),

                    SizedBox(height: 30,),

                    Text('Bank Identification', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                    SizedBox(height: 20,),
                    Text('Enter detail below to share the market price.', style: TextStyle(color: greyish, fontSize: 20,fontWeight: FontWeight.w400),),
                    SizedBox(height: 30,),

                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Bank Details',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15, ),
                            ),
                            SizedBox(height: 10,),

                            Container(
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  RichText(
                                    text: TextSpan(
                                      text: 'Bank Name',
                                      style: TextStyle(
                                          color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                                    ),

                                  ),
                                  SizedBox(height: 20,),
                                  Container(
                                      padding: EdgeInsets.only(left: 10),
                                      height: 60.0,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(7.0),
                                          border: Border.all(color: Colors.grey)),
                                      child:
                                      Center(
                                        child:
                                        new DropdownButtonFormField(
                                          decoration: InputDecoration.collapsed(
                                              hintText: ''  ),
                                          isExpanded: true,
                                          items: banks.map((item) {
                                            return new DropdownMenuItem(
                                              child: new Text(item['bank_name']),
                                              value: item,
                                            );
                                          }).toList(),
                                          onChanged: (newVal) {
                                            setState(() {
                                              bankId  = newVal['bank_id'];
                                              bankName  = newVal['bank_name'];
                                              print(bankName);
                                            });
                                          },
                                          //value: unitPrice,
                                        ),
                                      )
                                  )
                                ],
                              ),
                            ),
                          ]
                      ),
                    ),
                    SizedBox(height: 30,),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Account number',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15, ),
                          ),
                          TextFormField(
                            controller: accountController,
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                return "Invalid BVN";
//                              }
//                              return null;
//                            },
//
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
                            cursorColor: Colors.black,
                            maxLength: 10,
                            decoration: InputDecoration(
                                hintText: '2217161406',
                                hintStyle: TextStyle(color: greyish, fontWeight: FontWeight.bold)
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'BVN number',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15, ),
                          ),
                          TextFormField(
                            controller: bvnController,
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                return "Invalid BVN";
//                              }
//                              return null;
//                            },
//
                            keyboardType: TextInputType.phone,
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
                            cursorColor: Colors.black,
                            maxLength: 11,
                            decoration: InputDecoration(
                                hintText: '22171614064',
                                hintStyle: TextStyle(color: greyish, fontWeight: FontWeight.bold)
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                    Text(
                      'METHOD OF IDENTIFICATION',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15, ),
                    ),
                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Select a method',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15, ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[


                                  Container(
                                      padding: EdgeInsets.only(left: 10),
                                      height: 60.0,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(7.0),
                                          border: Border.all(color: Colors.grey)),
                                      child:
                                      Center(
                                        child: DropdownButtonFormField<String>(

                                          isExpanded: false,
                                          value: identificationType,

                                          onChanged: (String newValue) {
                                            setState(() {
                                              identificationType = newValue;
                                            });
                                          },
                                          decoration: InputDecoration.collapsed(
                                              hintText: ''  ),
                                          items: <String>[
                                            'National ID card',
                                            'Voters card',
                                            'International Passport',
                                          ].map<DropdownMenuItem<String>>(
                                                  (String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(value),
                                                );
                                              }).toList(),
                                        ),
                                      ),
                                  )
                                ],
                              ),
                            ),
                          ]
                      ),
                    ),
                    SizedBox(height: 30,),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'ID number',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15, ),
                          ),
                          TextFormField(
                            controller: idCardController,
                            validator: (value) {
//                              Voters card = 19;
//                              National ID card = 11;
//                              International Passport = 9;
                              if (value.isEmpty || identificationType == 'Voters card'? value.length != 19: identificationType == 'National ID card'? value.length!= 11
                              :value.length != 9) {
                                if(  identificationType == 'Voters card'){
                                  return "Invalid ID number must be 19";
                                }
                               else if(  identificationType == 'National ID card'){
                                  return "Invalid ID number must be 11";
                                }
                                else if(  identificationType == 'International Passport'){
                                  return "Invalid ID number must be 9";
                                }else
                                return "ID number can\'t be empty";
                              }
                              return null;
                            },
                            maxLength:identificationType == 'Voters card'? 19: identificationType == 'National ID card'?11 : 9,
//
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                                hintText: '',
                                hintStyle: TextStyle(color: greyish, fontWeight: FontWeight.bold)
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30,),

                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Role of farmer in the community',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15, ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[


                                  Container(
                                    padding: EdgeInsets.only(left: 10),
                                    height: 60.0,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7.0),
                                        border: Border.all(color: Colors.grey)),
                                    child:
                                    Center(
                                      child: DropdownButtonFormField<String>(

                                        isExpanded: false,
                                        value: farmerRole,

                                        onChanged: (String newValue) {
                                          setState(() {
                                            farmerRole = newValue;
                                          });
                                        },
                                        decoration: InputDecoration.collapsed(
                                            hintText: ''  ),
                                        items: <String>[
                                          'Chairman',
                                          'Secretary',
                                          'Member',
                                        ].map<DropdownMenuItem<String>>(
                                                (String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Text(value),
                                              );
                                            }).toList(),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ]
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child:
                      Padding(
                        padding: EdgeInsets.all(1.0),
                        child: Material(
                          elevation: 3.0,
                          borderRadius: BorderRadius.circular(12.0),
                          child: Container(
                            padding: EdgeInsets.all(15.0),
                            width: deviceWidth,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.white,
                            ),
                            constraints: BoxConstraints(minHeight: 50.0),
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Can read and write English?', style: TextStyle(
                                    color: Colors.black,
                                    fontSize: ScreenUtil().setSp(25),
                                ),),
                                SizedBox(height: 12.0,),
                                CustomSwitch(
                                  activeColor: appTheme,
                                  value: canRead,
                                  onChanged: (value) {
                                    print("VALUE : $value");
                                    setState(() {
                                      canRead = value;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child:
                      Padding(
                        padding: EdgeInsets.all(1.0),
                        child: Material(
                          elevation: 3.0,
                          borderRadius: BorderRadius.circular(12.0),
                          child: Container(
                            padding: EdgeInsets.all(15.0),
                            width: deviceWidth,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.white,
                            ),
                            constraints: BoxConstraints(minHeight: 50.0),
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Have a smart phone?', style: TextStyle(
                                    color: Colors.black,
                                    fontSize: ScreenUtil().setSp(25),
                                ),),
                                SizedBox(height: 12.0,),
                                CustomSwitch(
                                  activeColor: appTheme,
                                  value: smartPhone,
                                  onChanged: (value) {
                                    print("VALUE : $value");
                                    setState(() {
                                      smartPhone = value;
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 30,),
                    Container(
                        padding: EdgeInsets.all(10),
                        height: 80,
                        width: deviceWidth,
                        child:
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius
                                .circular(25.0),
                            //side: BorderSide(color: Colors.red)
                          ),
                          color: appTheme,
                          onPressed: ()async {

                              if (_formKey.currentState.validate()) {
                                setState(() {
                                  _isLoading = true;
                                });
                                await navigateToFarmerHistory(
                                    projectId,
                                    firstName,
                                    lastName,
                                    phoneNo,
                                    dob,
                                    martial,
                                    gender,
                                    numberDependent,
                                    nextKin,
                                    nextKinNo,
                                    cropFarmerProduces,
                                    farmerGroup,
                                    stateId,
                                    localGId,
                                    localG,
                                    pro_image,
                                    pro_image_thumbnail,
                                    farmerImage,
                                    bankId != null? int.parse(bankId):0,
                                    bvnController.text,
                                    identificationType,
                                    idCardController.text,
                                    farmerRole,
                                    canRead.toString(),
                                    smartPhone.toString(),
                                accountController.text, bankName);
                                //  await submit();
                              }

                          },
                          child: _isLoading
                              ? Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          )
                              :  Text('Next', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                        )
                    ),
                  ]
              )
          )
      ),
    );
  }
   navigateToFarmerHistory(String projectId, String firstName, String lastName,String phoneNo,String dob,String martial,
      String gender,int numberDependent,String nextKin,String nextKinNo,String cropFarmerProduces,String farmerGroup,int stateId,int localGId,String localG,
      String pro_image,String pro_image_thumbnail,File farmerImage,
      //From bank and identification
    int bankId,String bvn,String methodIdentification, String idCardNumber,String farmerRole, String writeEnglish, String haveSmartPhone, String accountNumber, String bankName  ) async {
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return FarmingHistory(projectId:projectId, firstName: firstName, lastName: lastName,phoneNo: phoneNo,dob: dob,martial: martial,
        gender: gender,numberDependent: numberDependent,nextKin: nextKin,nextKinNo: nextKinNo,cropFarmerProduces: cropFarmerProduces,farmerGroup: farmerGroup,stateId: stateId,localGId:localGId,localG: localG,
        pro_image: pro_image,pro_image_thumbnail: pro_image_thumbnail,farmerImage: farmerImage,
          //From bank and identification
          bankId: bankId,bvn: bvn,methodIdentification: methodIdentification, idCardNumber: idCardNumber,farmerRole: farmerRole, writeEnglish: writeEnglish, haveSmartPhone: haveSmartPhone, accountNumber:accountNumber, bankName:bankName
      );
    }));
  }
}
