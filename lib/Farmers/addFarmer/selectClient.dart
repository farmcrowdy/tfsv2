
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:grainpointapp/Farmers/addFarmer/farmerProfile.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SelectClient extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<SelectClient> {


  var projects = List();
  String message;
  var data;
  var projectId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProjects();
  }

  void getProjects() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      print(prefs.getString("projects"));
      projects =  json.decode(prefs.getString("projects"));
    });

  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;

    return Scaffold(
        body:
        SingleChildScrollView(
        child:
        Container(

        padding: EdgeInsets.all(20),
    child:
    Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10,),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [

                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back, size: 40,),
                    )
                  ],
                ),
              ),

              SizedBox(height: 30,),

              Text('Select Project', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
              SizedBox(height: 20,),
              Text('Select the Client and Project for the Farmer', style: TextStyle(color: greyish, fontSize: 20,fontWeight: FontWeight.w400),),
              SizedBox(height: 30,),
              Container(

                child:

                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Client',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 20, ),
                      ),
                      SizedBox(height: 20,),
                      Text(
                        clientName != null? '$clientName': 'Not attached to a client yet',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20, ),
                      ),
                    ]
                ),
              ),
              SizedBox(height: 30,),
              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'PROJECT',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15, ),
                      ),
                      SizedBox(height: 30,),

                      Center(
                        child: Container(
                            padding: EdgeInsets.only(left: 10),
                            height: 60.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7.0),
                                border: Border.all(color: Colors.grey)),
                            child: Center(
                              child: new DropdownButtonFormField(
                                decoration:
                                InputDecoration.collapsed(hintText: projects.isEmpty? 'No project available ': ''),
                                isExpanded: true,
                                items: projects.map((item) {
                                  return new DropdownMenuItem(
                                    child: new Text(item['name']),
                                    value: item['id'],
                                  );
                                }).toList(),
                                onChanged: (newVal) {
                                  setState(() {
                                    projectId = newVal;
                                    print(projectId);
                                  });
                                },
                                //value: unitPrice,
                              ),
                            ))
                      ),
                    ]
                ),
              ),

              SizedBox(height: 30,),

              Container(
                  padding: EdgeInsets.all(10),
                  height: 80,
                  width: deviceWidth,
                  child:
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius
                          .circular(25.0),
                      //side: BorderSide(color: Colors.red)
                    ),
                    color: appTheme,
                    onPressed: (){
                      navigateToProfile(projectId);
                    },
                    child: Text('Next', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                  )
              ),

            ]
        ))
        )
    );
  }
  void navigateToProfile(String projectId) async {
    Navigator.push(context, MaterialPageRoute(builder: (context){
      return FarmerProfile(projectId:projectId);
    }));
  }
}
