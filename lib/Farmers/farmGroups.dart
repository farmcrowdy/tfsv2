import 'package:flutter/material.dart';
import 'package:grainpointapp/Farmers/farmerPerGroup.dart';
import 'package:grainpointapp/SQL/groupModel.dart';
import 'package:grainpointapp/SQL/group_database_helper.dart';
import 'dart:async';
import 'package:sqflite/sqflite.dart';

class FarmerGroup extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NoteListState();
  }
}

class NoteListState extends State<FarmerGroup> {
  DatabaseGroupHelper databaseHelper = DatabaseGroupHelper();
  List<Group> groupList;
  int count = 0;

  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;
    if (groupList == null) {
      groupList = List<Group>();
      updateListView();
    }
    return Scaffold(
        body: Container(
            padding: EdgeInsets.all(20),
            child: ListView(children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back,
                        size: 40,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                'Farm groups created',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: deviceHeight / 1.4,
                child: getSchoolListView(),
              )
            ])));
  }

  ListView getSchoolListView() {
    return ListView.builder(
        itemCount: count,
        itemBuilder: (BuildContext context, int position) {
          return Card(
            color: Colors.white,
            elevation: 5.0,
            child: ListTile(
              leading: Icon(Icons.offline_bolt),
              title: Text(
                this.groupList[position].groupName,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              // subtitle: Text(this.groupList[position].id.toString()),
              onTap: () {
                  navigateToDetail(this.groupList[position].groupName);
              },
            ),
          );
        });
  }

  void _delete(BuildContext context, Group note) async {
    int result = await databaseHelper.deleteNote(note.id);
    if (result != 0) {
      _showSnackbar(context, 'Note Deleted Successfully');
      updateListView();
    }
  }

  void _showSnackbar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }


  void navigateToDetail(String  groupName) async {
   bool result = await Navigator.push(context, MaterialPageRoute(builder: (context){
      return FarmerPerGroup(groupName);
    }));
if (result == true) {
  updateListView();
}
  }
  void updateListView() {
    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<Group>> noteListFuture = databaseHelper.getGroupList();

      noteListFuture.then((groupList) {
        setState(() {
          this.groupList = groupList;
          this.count = groupList.length;
        });
      });
    });
  }
}
