

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/SQL/groupModel.dart';
import 'package:grainpointapp/SQL/group_database_helper.dart';
import 'package:grainpointapp/const.dart';

class CreateGroup extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<CreateGroup> {
  TextEditingController groupNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool _isLoading = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  Group grp;
  DatabaseGroupHelper helper = DatabaseGroupHelper();

   save() async {
    grp = Group(
      '',);

    grp.groupName = id+'_'+groupNameController.text;
    print(id+'_'+groupNameController.text);
    int result;
    print(result);
    if (grp.id != null) {
      print(grp.id);
      //Case 1:Update operation
      // result = await helper.updateNote(note);
      _isLoading = false;
      Fluttertoast.showToast(msg: 'Group already exist');


    } else {
      // Case 2: Insert// Operation
      result = await helper.insertNote(grp);
    }

    if (result != 0) {
      //Success
      _isLoading = false;
      Fluttertoast.showToast(msg: 'Group saved successfully to the local database');
      Navigator.pop(context);
    } else {
      // Failure
      _isLoading = false;
      Fluttertoast.showToast(msg: 'Problem Saving group');
    }
  }


  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
      body: Form(
          key: _formKey,
          autovalidate: false,
          child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Create a New Farm Group',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Enter farm group to be created.',
                  style: TextStyle(
                      color: greyish,
                      fontSize: 20,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'GROUP NAME',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                      ),
                      TextFormField(
                        controller: groupNameController,
                        validator: (value) {
                          if (value.isEmpty || value.length < 4) {
                            return "Four characters needed";
                          }
                          return null;
                        },
                        // maxLength: 11,
                        keyboardType: TextInputType.text,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.w500),
                        cursorColor: Colors.black,
                        decoration: InputDecoration(
                            hintText: 'Enter farm group name',
                            hintStyle: TextStyle(
                                color: greyish, fontWeight: FontWeight.bold)),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 100,
                ),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 80,
                    width: deviceWidth,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        //side: BorderSide(color: Colors.red)
                      ),
                      color: appTheme,
                      onPressed: ()async {
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            _isLoading = true;
                          });
                          await save();
                        }
                      },
                      child: _isLoading
                          ? Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                        ),
                      )
                          :  Text(
                        'Create group',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    )),
              ]))),
    );
  }

}
