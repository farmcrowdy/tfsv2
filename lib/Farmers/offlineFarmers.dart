import 'package:flutter/material.dart';
import 'package:grainpointapp/Farmers/editFarmer/editBankIdentification.dart';
import 'package:grainpointapp/Farmers/editFarmer/editFarmerProfile.dart';
import 'package:grainpointapp/Farmers/editFarmer/editFarmingHistory.dart';
import 'package:grainpointapp/Farmers/editFarmer/editSelectClient.dart';
import 'package:grainpointapp/Farmers/viewFullFarmers.dart';
import 'package:grainpointapp/SQL/addFarmerModel.dart';
import 'package:grainpointapp/SQL/database_helper.dart';
import 'package:grainpointapp/SQL/groupModel.dart';
import 'dart:async';
import 'package:sqflite/sqflite.dart';

class OfflineFarmers extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NoteListState();
  }
}

class NoteListState extends State<OfflineFarmers> {
  DatabaseAddFarmerHelper databaseHelper = DatabaseAddFarmerHelper();
  List<Farmers> farmerList;
  int count = 0;
  String editPage;

  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;
    if (farmerList == null) {
      farmerList = List<Farmers>();
      updateListView();
    }
    return Scaffold(
        body: Container(
            padding: EdgeInsets.all(20),
            child: ListView(children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back,
                        size: 40,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                'Offline Farmers',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: deviceHeight / 1.4,
                child: getSchoolListView(),
              )
            ])));
  }

  ListView getSchoolListView() {
    return ListView.builder(
        itemCount: count,
        itemBuilder: (BuildContext context, int position) {
          return Card(
            color: Colors.white,
            elevation: 5.0,
            child: ListTile(
              leading: Icon(Icons.offline_bolt),
              title: Text(
                this.farmerList[position].farmerId,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: this.farmerList[position].synced != 'False'
                  ? Text('Synced')
                  : Text('Not Synced'),
              //subtitle:Text(this.farmerList[position].synced),
              trailing:
              this.farmerList[position].synced == 'False'?

              InkWell(
                onTap: () {
                  _editDialog(message: position);
                },
                child: Icon(
                  Icons.edit,
                  color: Colors.red,
                  size: 30,
                ),
              ):Text(''),
              onTap: () {
                navigateToDetail(this.farmerList[position]);
              },
            ),
          );
        });
  }

  void _delete(BuildContext context, Group note) async {
    int result = await databaseHelper.deleteNote(note.id);
    if (result != 0) {
      _showSnackbar(context, 'Note Deleted Successfully');
      updateListView();
    }
  }

  void _showSnackbar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void navigateToDetail(Farmers farmers) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ViewFullFarmers(farmers);
    }));
    if (result == true) {
      updateListView();
    }
  }

  void navigateToEditProjectId(Farmers farmers) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return EditSelectClient(farmers);
    }));
    if (result == true) {
      updateListView();
    }
  }

  void navigateToEditFarmerProfile(Farmers farmers) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return EditFarmerProfile(farmers);
    }));
    if (result == true) {
      updateListView();
    }
  }

  void navigateToEditBankIdentification(Farmers farmers) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return EditBankIdentification(farmers);
    }));
    if (result == true) {
      updateListView();
    }
  }

  void navigateToEditFarmerHistory(Farmers farmers) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return EditFarmingHistory(farmers);
    }));
    if (result == true) {
      updateListView();
    }
  }

  void updateListView() {
    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<Farmers>> noteListFuture = databaseHelper.getFarmerList();

      noteListFuture.then((farmerList) {
        setState(() {
          this.farmerList = farmerList;
          this.count = farmerList.length;
        });
      });
    });
  }



  _editDialog({int message}) async {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
              title: IntrinsicWidth(
                stepWidth: 56.0,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minWidth: 280.0),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Which part do you want to edit?',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                        Divider(),
                        InkWell(
                          onTap: () {
                            navigateToEditProjectId(this.farmerList[message]);
                          },
                          child: Container(
                              height: 40,
                              child: Row(
                                children: [
                                  Text(
                                    "Edit Project Id",
                                    //textAlign: TextAlign.center,
                                  ),
                                ],
                              )),
                        ),
                        Divider(thickness: 2,),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            navigateToEditFarmerProfile(
                                this.farmerList[message]);
                          },
                          child: Container(
                              height: 40,
                              child: Row(
                                children: [
                                  Text(
                                    "Edit Farmer Profile",
                                    //textAlign: TextAlign.center,
                                  ),
                                ],
                              )),
                        ),
                        Divider(thickness: 2,),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            navigateToEditBankIdentification(
                                this.farmerList[message]);
                          },
                          child: Container(
                              height: 40,
                              child: Row(
                                children: [
                                  Text(
                                    "Edit Bank Identification",
                                    //textAlign: TextAlign.center,
                                  ),
                                ],
                              )),
                        ),
                        Divider(thickness: 2,),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            navigateToEditFarmerHistory(
                                this.farmerList[message]);
                          },
                          child: Container(
                              height: 40,

                              child: Row(
                                children: [
                                  Text(
                                    "Edit Farmer History",
                                    //textAlign: TextAlign.center,
                                  ),
                                ],
                              )),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[]);
        });
  }
}
