import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/SQL/addFarmerModel.dart';
import 'package:grainpointapp/SQL/database_helper.dart';
import 'package:grainpointapp/const.dart';
import 'package:location/location.dart';

class EditFarmingHistory extends StatefulWidget {
  final Farmers farmers;
  const EditFarmingHistory(this.farmers);

  @override
  _MyAppState createState() => _MyAppState(this.farmers);
}

class _MyAppState extends State<EditFarmingHistory> {
  Farmers farmers;

  _MyAppState(
    this.farmers,
  );

  TextEditingController landController = TextEditingController();
  TextEditingController pinController = TextEditingController();
  TextEditingController yearsController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String incomeRange = '0-99,000';
  bool _isLoading = false;
  String farmerId;
  String lastTime =
      DateTime.now().millisecondsSinceEpoch.toString().substring(6);
  var location = Location();
  String lat1;
  String lat2;
  String lat3;
  String lat4;
  String lat5;
  String lat6;
  String long1;
  String long2;
  String long3;
  String long4;
  String long5;
  String long6;
  String fingerPrint;





  DatabaseAddFarmerHelper helper = DatabaseAddFarmerHelper();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pinController.text = farmers.fcmb_pin;
    lat1 = farmers.lat1;
    long1 = farmers.long1;
    farmerId = farmers.farmerId;
    incomeRange = farmers.income_range;
    landController.text = farmers.land_area_farmed;
    yearsController.text = farmers.years_of_experience;
    fingerPrint = farmers.f_print;
    lat2 = farmers.lat2;
    lat3 = farmers.lat3;
    lat4 = farmers.lat4;
    lat5 = farmers.lat5;
    lat6 = farmers.lat6;
    long2 = farmers.long2;
    long3 = farmers.long3;
    long4 = farmers.long4;
    long5 = farmers.long5;
    long6 = farmers.long6;
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Form(
          key: _formKey,
          autovalidate: false,
          child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Edit Farm History',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Enter detail of land area farmed previously.',
                  style: TextStyle(
                      color: greyish,
                      fontSize: 20,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: 'Land area in Hectares',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        TextFormField(
                          controller: landController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Invalid";
                            }
                            return null;
                          },
//
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w500),
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              hintText: '',
                              hintStyle: TextStyle(
                                  color: greyish, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: 'Years of experience',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        TextFormField(
                          controller: yearsController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Invalid Years";
                            }
                            return null;
                          },
//
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w500),
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              hintText: '',
                              hintStyle: TextStyle(
                                  color: greyish, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Income range before this moment',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                height: 60.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7.0),
                                    border: Border.all(color: Colors.grey)),
                                child: Center(
                                  child: DropdownButtonFormField<String>(
                                    isExpanded: false,
                                    value: incomeRange,
                                    onChanged: (String newValue) {
                                      setState(() {
                                        incomeRange = newValue;
                                      });
                                    },
                                    decoration:
                                        InputDecoration.collapsed(hintText: ''),
                                    items: <String>[
                                      '0-99,000',
                                      '100,000-500,000',
                                      '> 1,000,000',
                                    ].map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ]),
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: 'Enter your Pin',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        TextFormField(
                          controller: pinController,
                          validator: (value) {
                            if (value.isEmpty || value.length != 4) {
                              return "Invalid pin";
                            }
                            return null;
                          },
                          maxLength: 4,
                          keyboardType: TextInputType.number,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.w500),
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              hintText: '',
                              hintStyle: TextStyle(
                                  color: greyish, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                ),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: 'Get farm coordinates',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            text:
                                'It requires you to go round the farm land and tap on the icons accordingly.',
                            style: TextStyle(
                                color: greyish,
                                fontWeight: FontWeight.w500,
                                fontSize: 15),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude1();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),

                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point A',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                      ),
                                      lat1 != null
                                          ? Text(
                                              'Lat: $lat1, Long: $long1',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  ),
                                            )
                                          : Text(
                                              'Tap to get point A coordinate',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  ),
                                            ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude2();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point B',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            ),
                                      ),
                                      lat2 != null
                                          ? Text(
                                              'Lat: $lat2, Long: $long2',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                        ),
                                            )
                                          : Text(
                                              'Tap to get point B coordinate',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  ),
                                            ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude3();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point C',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                      ),
                                      lat3 != null
                                          ? Text(
                                              'Lat: $lat3, Long: $long3',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                 ),
                                            )
                                          : Text(
                                              'Tap to get point C coordinate',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                 ),
                                            ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude4();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point D',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                           ),
                                      ),
                                      lat4 != null
                                          ? Text(
                                              'Lat: $lat4, Long: $long4',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                 ),
                                            )
                                          : Text(
                                              'Tap to get point D coordinate',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                 ),
                                            ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude5();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point E',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      lat5 != null
                                          ? Text(
                                        'Lat: $lat5, Long: $long5',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                          : Text(
                                        'Tap to get point E coordinate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap: () {
                            getLatitude6();
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa819aaf),
                                border: Border.all(color: Color(0xff819aaf),width: 2)
                            ),
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Point F',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      lat6 != null
                                          ? Text(
                                        'Lat: $lat6, Long: $long6',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      )
                                          : Text(
                                        'Tap to get point F coordinate',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.my_location,
                                    color: Colors.redAccent,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        InkWell(
                          onTap:_readFingerPrint,

                          child: Container(
                            padding: EdgeInsets.all(10),
                            color: Colors.black,
                            child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Tap to get finger print',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      fingerPrint != null
                                          ? Text(
                                              'Finger print taken',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            )
                                          : Text(
                                              'Fingerprint empty',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                    ],
                                  ),
                                  Icon(
                                    Icons.fingerprint,
                                    color: Colors.white,
                                  )
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ]),
                ),
                SizedBox(
                  height: 100,
                ),
                Container(
                    padding: EdgeInsets.all(10),
                    height: 80,
                    width: deviceWidth,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        //side: BorderSide(color: Colors.red)
                      ),
                      color: appTheme,
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            print(farmers.id);
                            _isLoading = true;
                          });
                          await _getFingerString();
                        }
                      },
                      child: _isLoading
                          ? Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                              ),
                            )
                          : Text(
                              'Update',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                    )),
              ]))),
    );
  }

  void update() {

    farmers.fcmb_pin = pinController.text;
    farmers.timestamp = lastTime;
    farmers.lat1 = lat1;
    farmers.long1 = long1;
    farmers.farmerId = farmerId;
    farmers.income_range = incomeRange;
    farmers.land_area_farmed = landController.text;
    farmers.years_of_experience = yearsController.text;
    farmers.lat2 = lat2;
    farmers.lat3 = lat3;
    farmers.lat4 = lat4;
    farmers.lat5 = lat5;
    farmers.lat6 = lat6;
    farmers.long2 = long2;
    farmers.long3 = long3;
    farmers.long4 = long4;
    farmers.long5= long5;
    farmers.long6 = long6;
    farmers.f_print =  fingerPrint;

    save();
  }



  save() async {
    int result;
    print(result);
    if (farmers.id != null) {
      print(farmers.id);
      //Case 1:Update operation
      result = await helper.updateNote(farmers);
      _isLoading = false;
    } else {
      // Case 2: Insert// Operation
      //result = await helper.insertNote(farmers);
    }

    if (result != 0) {
      //Success
      _isLoading = false;
      Fluttertoast.showToast(
          msg: 'Farmer saved successfully to the local database');
      print(fingerPrint);
      Navigator.pop(context);
    } else {
      // Failure
      _isLoading = false;
      Fluttertoast.showToast(msg: 'Problem Saving Farmer');
    }
  }



  // Getting farmers FingerPrint
  static const platform = const MethodChannel('samples.flutter.dev/readFingerPrint');
  static const platform2 = const MethodChannel('samples.flutter.dev/finger');


  Future<void> _readFingerPrint() async {
    try {

      final String result = await platform.invokeMethod('readFingerPrint');
      setState(() {
        fingerPrint = result;
        print(fingerPrint);
      });

    } on PlatformException catch (e) {

      print( e.message);
    }
  }
  Future<void> _getFingerString() async {
    try {

      final String result = await platform2.invokeMethod('getFingerPrint');
      setState(() {
        fingerPrint = result;
        print(fingerPrint);
        update();
      });
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  Future getLatitude1() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat1 = currentLocation.latitude.toString();
        long1 = currentLocation.longitude.toString();
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }

  Future getLatitude2() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat2 = currentLocation.latitude.toString();
        long2 = currentLocation.longitude.toString();
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }

  Future getLatitude3() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat3 = currentLocation.latitude.toString();
        long3 = currentLocation.longitude.toString();
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }

  Future getLatitude4() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat4 = currentLocation.latitude.toString();
        long4 = currentLocation.longitude.toString();
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }
  Future getLatitude5() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat5 = currentLocation.latitude.toString();
        long5 = currentLocation.longitude.toString();
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }
  Future getLatitude6() async {
    LocationData currentLocation;
    try {
      currentLocation = await location.getLocation();
      setState(() {
        lat6 = currentLocation.latitude.toString();
        long6 = currentLocation.longitude.toString();
      });
    } on Exception {
      currentLocation = null;
      print(e.toString());
      return null;
    }
  }
}
