import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/SQL/addFarmerModel.dart';
import 'package:grainpointapp/SQL/database_helper.dart';
import 'package:grainpointapp/const.dart';
import 'package:shared_preferences/shared_preferences.dart';



class EditBankIdentification extends StatefulWidget {
  final Farmers farmers;

  const EditBankIdentification(this.farmers);

 @override
  _MyAppState createState() => _MyAppState( this.farmers);
}

class _MyAppState extends State<EditBankIdentification> {

  final Farmers farmers;
  _MyAppState(this.farmers);

  TextEditingController bvnController = TextEditingController();
  TextEditingController idCardController = TextEditingController();
  TextEditingController accountController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String identificationType = 'National ID card';
  String farmerRole = 'Chairman';
  var banks = List();
  var bankId;
  var bankName;
  bool _isLoading = false;
  void getBanks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      print(prefs.getString("banks"));
    });
    banks =  json.decode(prefs.getString("banks"));

  }

  DatabaseAddFarmerHelper helper = DatabaseAddFarmerHelper();



  //Http call

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getBanks();
    setState(() {
      print(farmers.id);
      bankName = farmers.bankName;
      accountController.text = farmers.acct_number;
      bvnController.text = farmers.bvnNumber;
      identificationType =  farmers.idType ;
      idCardController.text=farmers.idNumber;
      bankId = farmers.bank_id;

    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body:
      Form(
          key: _formKey,
          autovalidate: false,
          child:
          Container(
              padding: EdgeInsets.all(20),
              child:
              ListView(
                  children: <Widget>[
                    SizedBox(height: 10,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [

                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(Icons.arrow_back, size: 40,),
                          )
                        ],
                      ),
                    ),

                    SizedBox(height: 30,),

                    Text('Edit Bank Identification', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                    SizedBox(height: 20,),
                    Text('Enter detail below to share the market price.', style: TextStyle(color: greyish, fontSize: 20,fontWeight: FontWeight.w400),),
                    SizedBox(height: 30,),

                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Bank Details',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15, ),
                            ),
                            SizedBox(height: 10,),

                            Container(
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  RichText(
                                    text: TextSpan(
                                      text: 'Bank Name',
                                      style: TextStyle(
                                          color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                                    ),

                                  ),
                                  SizedBox(height: 20,),
                                  Container(
                                      padding: EdgeInsets.only(left: 10),
                                      height: 60.0,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(7.0),
                                          border: Border.all(color: Colors.grey)),
                                      child:
                                      Center(
                                        child:
                                        new DropdownButtonFormField(
                                          decoration: InputDecoration.collapsed(
                                              hintText: ''  ),
                                          isExpanded: true,
                                          items: banks.map((item) {
                                            return new DropdownMenuItem(
                                              child: new Text(item['bank_name']),
                                              value: item,
                                            );
                                          }).toList(),
                                          onChanged: (newVal) {
                                            setState(() {
                                              bankId  = newVal['bank_id'];
                                              bankName  = newVal['bank_name'];
                                              print(bankName);
                                            });
                                          },
                                          //value: unitPrice,
                                        ),
                                      )
                                  )
                                ],
                              ),
                            ),
                          ]
                      ),
                    ),
                    SizedBox(height: 30,),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Account number',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15, ),
                          ),
                          TextFormField(
                            controller: accountController,
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                return "Invalid BVN";
//                              }
//                              return null;
//                            },
//
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
                            cursorColor: Colors.black,
                            maxLength: 10,
                            decoration: InputDecoration(
                                hintText: '2217161406',
                                hintStyle: TextStyle(color: greyish, fontWeight: FontWeight.bold)
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'BVN number',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15, ),
                          ),
                          TextFormField(
                            controller: bvnController,
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                return "Invalid BVN";
//                              }
//                              return null;
//                            },
//
                            keyboardType: TextInputType.phone,
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
                            cursorColor: Colors.black,
                            maxLength: 11,
                            decoration: InputDecoration(
                                hintText: '22171614064',
                                hintStyle: TextStyle(color: greyish, fontWeight: FontWeight.bold)
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30,),
                    Text(
                      'METHOD OF IDENTIFICATION',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15, ),
                    ),
                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Select a method',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15, ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[


                                  Container(
                                      padding: EdgeInsets.only(left: 10),
                                      height: 60.0,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(7.0),
                                          border: Border.all(color: Colors.grey)),
                                      child:
                                      Center(
                                        child: DropdownButtonFormField<String>(

                                          isExpanded: false,
                                          value: identificationType,

                                          onChanged: (String newValue) {
                                            setState(() {
                                              identificationType = newValue;
                                            });
                                          },
                                          decoration: InputDecoration.collapsed(
                                              hintText: ''  ),
                                          items: <String>[
                                            'National ID card',
                                            'Voters card',
                                            'International Passport',
                                          ].map<DropdownMenuItem<String>>(
                                                  (String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(value),
                                                );
                                              }).toList(),
                                        ),
                                      ),
                                  )
                                ],
                              ),
                            ),
                          ]
                      ),
                    ),
                    SizedBox(height: 30,),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'ID number',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 15, ),
                          ),
                          TextFormField(
                            controller: idCardController,
                            validator: (value) {
//                              Voters card = 19;
//                              National ID card = 11;
//                              International Passport = 9;
                              if (value.isEmpty || identificationType == 'Voters card'? value.length != 19: identificationType == 'National ID card'? value.length!= 11
                              :value.length != 9) {
                                if(  identificationType == 'Voters card'){
                                  return "Invalid ID number must be 19";
                                }
                               else if(  identificationType == 'National ID card'){
                                  return "Invalid ID number must be 11";
                                }
                                else if(  identificationType == 'International Passport'){
                                  return "Invalid ID number must be 9";
                                }else
                                return "ID number can\'t be empty";
                              }
                              return null;
                            },
                            maxLength:identificationType == 'Voters card'? 19: identificationType == 'National ID card'?11 : 9,
//
                            keyboardType: TextInputType.number,
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                                hintText: '',
                                hintStyle: TextStyle(color: greyish, fontWeight: FontWeight.bold)
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30,),

                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Role of farmer in the community',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15, ),
                            ),
                            SizedBox(height: 10,),
                            Container(
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[


                                  Container(
                                    padding: EdgeInsets.only(left: 10),
                                    height: 60.0,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7.0),
                                        border: Border.all(color: Colors.grey)),
                                    child:
                                    Center(
                                      child: DropdownButtonFormField<String>(

                                        isExpanded: false,
                                        value: farmerRole,

                                        onChanged: (String newValue) {
                                          setState(() {
                                            farmerRole = newValue;
                                          });
                                        },
                                        decoration: InputDecoration.collapsed(
                                            hintText: ''  ),
                                        items: <String>[
                                          'Chairman',
                                          'Secretary',
                                          'Member',
                                        ].map<DropdownMenuItem<String>>(
                                                (String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Text(value),
                                              );
                                            }).toList(),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ]
                      ),
                    ),
//                    Container(
//                      padding: EdgeInsets.all(10),
//                      child:
//                      Padding(
//                        padding: EdgeInsets.all(1.0),
//                        child: Material(
//                          elevation: 5.0,
//                          borderRadius: BorderRadius.circular(12.0),
//                          shadowColor: Colors.teal,
//                          child: Container(
//                            padding: EdgeInsets.all(15.0),
//                            width: deviceWidth,
//                            decoration: BoxDecoration(
//                              borderRadius: BorderRadius.circular(12.0),
//                              color: Colors.white,
//                            ),
//                            constraints: BoxConstraints(minHeight: 50.0),
//                            child:
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                              children: <Widget>[
//                                Text('Can read and write English?', style: TextStyle(
//                                    color: Colors.black,
//                                    fontSize: 15.0
//                                ),),
//                                SizedBox(height: 12.0,),
//                                CustomSwitch(
//                                  activeColor: appTheme,
//                                  value: canRead,
//                                  onChanged: (value) {
//                                    print("VALUE : $value");
//                                    setState(() {
//                                      canRead = value;
//                                    });
//                                  },
//                                ),
//                              ],
//                            ),
//                          ),
//                        ),
//                      ),
//                    ),
//                    Container(
//                      padding: EdgeInsets.all(10),
//                      child:
//                      Padding(
//                        padding: EdgeInsets.all(1.0),
//                        child: Material(
//                          elevation: 5.0,
//                          borderRadius: BorderRadius.circular(12.0),
//                          shadowColor: Colors.teal,
//                          child: Container(
//                            padding: EdgeInsets.all(15.0),
//                            width: deviceWidth,
//                            decoration: BoxDecoration(
//                              borderRadius: BorderRadius.circular(12.0),
//                              color: Colors.white,
//                            ),
//                            constraints: BoxConstraints(minHeight: 50.0),
//                            child:
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                              children: <Widget>[
//                                Text('Have a smart phone?', style: TextStyle(
//                                    color: Colors.black,
//                                    fontSize: 15.0
//                                ),),
//                                SizedBox(height: 12.0,),
//                                CustomSwitch(
//                                  activeColor: appTheme,
//                                  value: smartPhone,
//                                  onChanged: (value) {
//                                    print("VALUE : $value");
//                                    setState(() {
//                                      smartPhone = value;
//                                    });
//                                  },
//                                ),
//                              ],
//                            ),
//                          ),
//                        ),
//                      ),
//                    ),
                    SizedBox(height: 30,),
                    Container(
                        padding: EdgeInsets.all(10),
                        height: 80,
                        width: deviceWidth,
                        child:
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius
                                .circular(25.0),
                            //side: BorderSide(color: Colors.red)
                          ),
                          color: appTheme,
                          onPressed: ()async {

                              if (_formKey.currentState.validate()) {
                                setState(() {
                                  _isLoading = true;
                                });

                                 await update();
                              }

                          },
                          child: _isLoading
                              ? Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.white,
                            ),
                          )
                              :  Text('Update', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                        )
                    ),
                  ]
              )
          )
      ),
    );
  }
    update() {
      farmers.bankName = bankName;
      farmers.acct_number = accountController.text;
      farmers.bvnNumber = bvnController.text;
      farmers.idType = identificationType;
      farmers.idNumber = idCardController.text;
      farmers.bank_id = bankId;
      farmers.role = farmerRole;
     save();
  }

  save() async {

    int result;
    print(result);
    if (farmers.id != null) {
      print(farmers.id);
      //Case 1:Update operation
      result = await helper.updateNote(farmers);
      _isLoading = false;
    } else {
      // Case 2: Insert// Operation
      //result = await helper.insertNote(farmers);
    }

    if (result != 0) {
      //Success
      _isLoading = false;
      Fluttertoast.showToast(
          msg: 'Farmer saved successfully to the local database');
      Navigator.pop(context);
    } else {
      // Failure
      _isLoading = false;
      Fluttertoast.showToast(msg: 'Problem Saving Farmer');
    }
  }

}
