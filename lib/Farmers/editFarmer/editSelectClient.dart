
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/SQL/addFarmerModel.dart';
import 'package:grainpointapp/SQL/database_helper.dart';
import 'package:grainpointapp/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditSelectClient extends StatefulWidget {
final Farmers farmers;

  const EditSelectClient(this.farmers);
  @override
  _MyAppState createState() => _MyAppState(farmers);
}

class _MyAppState extends State<EditSelectClient> {
  final Farmers farmers;
  var clients = List();
  var projects = List();
  Map<String, dynamic> responseData;
  String message;
  var data;
  var clientId;
  var projectId;

  _MyAppState(this.farmers);


  DatabaseAddFarmerHelper helper = DatabaseAddFarmerHelper();


  void getProjects() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      print(prefs.getString("projects"));
      projects =  json.decode(prefs.getString("projects"));
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getProjects();
  }
  String projectID;

  @override
  Widget build(BuildContext context) {

    projectId = farmers.project_id;
    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;
    return Scaffold(
        body:

        Container(
        padding: EdgeInsets.all(20),
    child:
    Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10,),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [

                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back, size: 40,),
                    )
                  ],
                ),
              ),

              SizedBox(height: 30,),

              Text('Edit Select Project', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
              SizedBox(height: 20,),
              Text('Select the Client and Project for the Farmer', style: TextStyle(color: greyish, fontSize: 20,fontWeight: FontWeight.w400),),
              SizedBox(height: 30,),
              Container(

                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Client',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20, ),
                      ),
                      SizedBox(height: 20,),
                      Text(
                        clientName != null? '$clientName': 'Not attached to a client yet',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20, ),
                      ),
                    ]
                ),
              ),
              SizedBox(height: 30,),
              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'PROJECT',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 15, ),
                      ),
                      SizedBox(height: 30,),

                      Center(
                        child: Container(
                            padding: EdgeInsets.only(left: 10),
                            height: 60.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7.0),
                                border: Border.all(color: Colors.grey)),
                            child: Center(
                              child: new DropdownButtonFormField(
                                decoration:
                                InputDecoration.collapsed(hintText: projects.isEmpty? 'No project available ': ''),
                                isExpanded: true,
                                items: projects.map((item) {
                                  return new DropdownMenuItem(
                                    child: new Text(item['name']),
                                    value: item,
                                  );
                                }).toList(),
                                onChanged: (newVal) {
                                  setState(() {
                                    projectId = newVal['id'];
                                    print(projectId);
                                  });
                                },
                                //value: unitPrice,
                              ),
                            ))
                      ),
                    ]
                ),
              ),

              SizedBox(height: 30,),

              Container(
                  padding: EdgeInsets.all(10),
                  height: 80,
                  width: deviceWidth,
                  child:
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius
                          .circular(25.0),
                      //side: BorderSide(color: Colors.red)
                    ),
                    color: appTheme,
                    onPressed: (){
                      save();
                    },
                    child: Text('Update', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                  )
              ),

            ]
        )
        )
    );
  }


  void update() {

    farmers.project_id = projectId;

  }



  save() async {
    update();
    int result;
    if (farmers.id != null) {
      print(farmers.id);
      //Case 1:Update operation
      result = await helper.updateNote(farmers);
    } else {
      // Case 2: Insert// Operation
      //result = await helper.insertNote(farmers);
    }

    if (result != 0) {
      //Success
      Fluttertoast.showToast(
          msg: 'Farmer saved successfully to the local database');
      Navigator.pop(context);

    } else {
      // Failure
      Fluttertoast.showToast(msg: 'Problem Saving Farmer');
    }
  }
}
