//"id": "11",
//"order_id": "2",
//"collection_center_id": "8",
//"assigned_by": "30",
//"deleted_by": null,
//"active": "1",
//"date_created": "2020-08-04 05:39:22",
//"date_updated": null


class OrdersCenterPojo {

  var  id;
  var  orderId;
  var collectionCenterId;
  var assignedBy;
  var deletedBy;
  var active;
  var dateCreated;
  var dateUpdated;



  OrdersCenterPojo({
    this.id,
    this.orderId,
    this.collectionCenterId,
    this.assignedBy,
    this.deletedBy,
    this.active,
    this.dateCreated,
    this.dateUpdated,
  });

  OrdersCenterPojo.fromJson(Map<String, dynamic> doc):
        id =  doc['id'],
        orderId =  doc['order_id'],
        collectionCenterId =  doc['collection_center_id'],
        assignedBy =  doc['assigned_by'],
        deletedBy =  doc['deleted_by'],
        active =  doc['active'],
        dateCreated =  doc['date_created'],
        dateUpdated = doc['date_updated'];
}