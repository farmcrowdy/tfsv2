//"id": "13",
//"crop_id": "1",
//"price_per_unit": "6700",
//"state_id": "25",
//"local_id": "485",
//"client_id": "5",
//"date_created": "2020-05-21 12:22:24",
//"date_updated": null,
//"crop_name": "Maize"

class PriceProducePojo {

  var  id;
  var  cropId;
  var pricePerUnit;
  var stateId;
  var localId;
  var clientId;
  var dateCreated;
  var dateUpdated;
  String cropName;



  PriceProducePojo({
    this.id,
    this.cropId,
    this.pricePerUnit,
    this.stateId,
    this.localId,
    this.clientId,
    this.dateCreated,
    this.dateUpdated,
    this.cropName,
  });

  PriceProducePojo.fromJson(Map<String, dynamic> doc):
        id =  doc['id'],
        cropId =  doc['crop_id'],
        pricePerUnit =  doc['price_per_unit'],
        stateId =  doc['state_id'],
        localId =  doc['local_id'],
        clientId =  doc['client_id'],
        dateCreated =  doc['date_created'],
        dateUpdated = doc['date_updated'],
        cropName =  doc['crop_name'];

}
