//"id": "8",
//"buyer_id": "8",
//"client_id": "5",
//"crop_id": "1",
//"desired_quantity": "50000",
//"state_id": "11",
//"local_id": "202",
//"moisture_level": "12",
//"purity_level": "10",
//"demand_price": "500000",
//"aggregation_price": "439999",
//"shipping_address": "Ebonyi, Afikpo North",
//"delivery_date": "2020-06-05 00:00:00",
//"shipping_cost": "1",
//"quantity_purchased": "0",
//"notes": "I want it well packaged. Thank you",
//"status": "approved",
//"created_by": "30",
//"edited_by": "0",
//"date_created": "2020-05-21 12:42:36",
//"date_updated": "2020-05-21 12:43:45"

class OrderPlacedPojo {
  var id;
  var buyerId;
  var clientId;
  var cropId;
  var desiredQuantity;
  var stateId;
  var localId;
  var moistureLevel;
  var purityLevel;
  var demandPrice;
  var aggregationPrice;
  var shippingAddress;
  var deliveryDate;
  var shippingCost;
  var quantityPurchased;
  var notes;
  var status;
  var createdBy;
  var editedBy;
  var dateCreated;
  var dateUpdated;

  OrderPlacedPojo(
      {this.id,
      this.buyerId,
      this.clientId,
      this.cropId,
      this.desiredQuantity,
      this.stateId,
      this.localId,
      this.moistureLevel,
      this.purityLevel,
      this.demandPrice,
      this.aggregationPrice,
      this.shippingAddress,
      this.deliveryDate,
      this.shippingCost,
      this.quantityPurchased,
      this.notes,
      this.status,
      this.createdBy,
      this.editedBy,
      this.dateCreated,
      this.dateUpdated});

  OrderPlacedPojo.fromJson(Map<String, dynamic> doc)
      : id = doc['id'],
        buyerId = doc['buyer_id'],
        clientId = doc['client_id'],
        cropId = doc['crop_id'],
        desiredQuantity = doc['desired_quantity'],
        stateId = doc['state_id'],
        localId = doc['local_id'],
        moistureLevel = doc['moisture_level'],
        purityLevel = doc['purity_level'],
        demandPrice = doc['demand_price'],
        aggregationPrice = doc['aggregation_price'],
        shippingAddress = doc['shipping_address'],
        deliveryDate = doc['delivery_date'],
        shippingCost = doc['shipping_cost'],
        quantityPurchased = doc['quantity_purchased'],
        notes = doc['notes'],
        status = doc['status'],
        createdBy = doc['created_by'],
        editedBy = doc['edited_by'],
        dateCreated = doc['date_created'],
        dateUpdated = doc['date_updated'];
}
