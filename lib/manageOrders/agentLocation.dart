//
//GET api/agent_assigned_locations/{agent_id}
//This gets the location where an agent is assigned by the Admin.
//Required parameters
//agent_id (integer)
//
//GET api/prices_in_collection_center/{client_id}/{collection_center_id}
//This endpoint gets the prices of produce in a collection center.
//Required parameters
//client_id (integer)
//collection_center_id (integer)
//
//GET api/orders_in_collection_center/{collection_center_id}
//This endpoint gets all the orders assigned to a collection center by an Admin.
//Required parameters
//collection_center_id (integer)
//
//GET api/orders_by_client_id/{client_id}
//This gives you all the orders placed by a given client such as Novus Agro.
//Required parameters
//client_id (integer)
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/Input/Collectverification.dart';
import 'package:grainpointapp/const.dart';
import 'package:http/http.dart' as http;


int txCode;
class AgentLocation extends StatefulWidget {
  final String agentId;

  const AgentLocation({Key key, this.agentId}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState(agentId);
}

class _MyAppState extends State<AgentLocation> {
  TextEditingController quantityController = TextEditingController();
  TextEditingController moistureController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String agentId;
  Map<String, dynamic> responseData;
  String message;
  var data;
  var body;
  var postId;
  var centerId;
  var assignedBy;
  var dateCreated;
  


  _MyAppState(this.agentId);

  _confirmClientProfile() async {
    body = {
      "phone": '$agentId',
    };

    final response = await http.get(
      baseUrl + 'agent_assigned_locations/$agentId',
      headers: {'api-key': 'aa47f8215c6f30a0dcdb2a36a9f4168e'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        print(responseData);
        postId = (responseData['message'][0]['id']);
        centerId = (responseData['message'][0]['collection_center_id']);
        assignedBy = (responseData['message'][0]['assigned_by']);
        dateCreated = (responseData['message'][0]['date_created']);
        //phoneNo = (responseData['message']['phone']);
        print(postId);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      Fluttertoast.showToast(msg: er);
      Navigator.pop(context);
      print('error$er');
      print(sa);
      //throw Exception('Failed to load internet');
    }
  }





  // ignore: must_call_super
  void initState() {
    // TODO: implement initState
    _confirmClientProfile();
  }


  @override
  Widget build(BuildContext context) {

    if (postId == null) {
      return SpinKitChasingDots(
        size: 50,
        color: appTheme,
      );
    }
    return Scaffold(
      body: Form(
          key: _formKey,
          autovalidate: true,
          child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Agent Location',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                SizedBox(
                  height: 20,
                ),

                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    Text(
                     'ID  ' ,
                      style: TextStyle(fontSize: 15, color: greyish, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      postId,
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
          ),
                Row(
                  children: [
                    Text(
                      'Center Id',
                      style: TextStyle(fontSize: 15,color: greyish, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      centerId,
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Text(
                      'Assigned By',
                      style: TextStyle(fontSize: 15,color: greyish, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      assignedBy,
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Text(
                      'Date Created',
                      style: TextStyle(fontSize: 15,color: greyish, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      dateCreated,
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),

              ]))),
    );
  }
  toCollect(BuildContext context, { String phoneNo,}) {
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            pageBuilder: (_, __, ___) =>
                CollectOTP( phoneNo: phoneNo,)));
  }
}
