import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/const.dart';
import 'package:grainpointapp/manageOrders/model/ordersPlacedPojo.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderPlaced extends StatefulWidget {

  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<OrderPlaced> {
  String token;
  var data;
  var body;
  Map<String, dynamic> responseData;
  String message;
  var result;
  var userId;
  var id;
  String firstName;
  String lastName;
  String phoneNo;
  String email;
  String gender;
  String wallet;
  bool resultOk = false;
  TextEditingController numberController = TextEditingController();
  final oCcy = new NumberFormat("#,##0.00", "en_US");

  _Pickup();

  void getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      userId = prefs.getString('userId');
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
    getOrderPlaced();
  }

  //Http call
  Future<List<OrderPlacedPojo>> getOrderPlaced() async {
    print(data);
    var body = json.encode(data);
    final response = await http.get(
      baseUrl + 'orders_by_client_id/$clientId',
      headers: {'api-key': 'aa47f8215c6f30a0dcdb2a36a9f4168e'},
    );
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      // message = responseData["message"];
      result = responseData["message"];
      setState(() {
        resultOk = true;
      });

      print(responseData);
      print(body);
      print(message);
      final items = result.cast<Map<String, dynamic>>();
      List<OrderPlacedPojo> listOfUsers = items.map<OrderPlacedPojo>((json) {
        return OrderPlacedPojo.fromJson(json);
      }).toList();
      return listOfUsers;
      print(responseData);
      print(body);
      print(message);
      //Fluttertoast.showToast(msg: message);
    } else {
      responseData = json.decode(response.body);
      print('error$responseData');
      var er = responseData['message'];
      print(er);
      Fluttertoast.showToast(msg: er);
      Navigator.pop(context);
      //throw Exception('Failed to load internet');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (resultOk == false) {
      return SpinKitChasingDots(
        size: 50,
        color: appTheme,
      );
    }
    final deviceWidth = MediaQuery.of(context).size.width;
    final deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
        body: ListView(children: <Widget>[
      SizedBox(
        height: 10,
      ),
      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                size: 40,
              ),
            )
          ],
        ),
      ),
      SizedBox(
        height: 30,
      ),
      Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                'Client Orders',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'This gives you all the orders placed by a given client.',
                style: TextStyle(
                    color: greyish, fontSize: 20, fontWeight: FontWeight.w400),
              ),
            ],
          )),
      SizedBox(
        height: 30,
      ),
      Column(
        children: <Widget>[
          Flex(direction: Axis.horizontal, children: [
            Expanded(
              child: FutureBuilder<List<OrderPlacedPojo>>(
                  future: getOrderPlaced(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Column(children: <Widget>[
                        Container(
                            width: double.infinity,
                            height: 40,
                            padding: EdgeInsets.symmetric(horizontal: 24.0),
                            child: Text('')),
                      ]);
                    } else if (snapshot.data.isNotEmpty) {
                      return Container(
                        height: deviceHeight/1.5,
                        child: ListView(
                          //scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          children: snapshot.data
                              .map(
                                (feed) => Padding(
                                  padding: EdgeInsets.all(5.0),
                                  child: Material(
                                    elevation: 3.0,
                                    borderRadius: BorderRadius.circular(12.0),
                                    shadowColor: Colors.blueAccent,
                                    child: Container(
                                        padding: EdgeInsets.all(10.0),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(12.0),
                                          color: Colors.white,
                                        ),
                                        constraints:
                                            BoxConstraints(minHeight: 60.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'Buyer Id: ' + feed.buyerId,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Crop Id: ' + feed.cropId,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Desired Quantity: ' +
                                                  feed.desiredQuantity +
                                                  'kg',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Moisture level: ' +
                                                  feed.moistureLevel,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Purity level: ' +
                                                  feed.purityLevel,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Demand price: \u{20A6}${oCcy.format(int.parse(feed.demandPrice))}',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Aggregation price: \u{20A6}${oCcy.format(int.parse(feed.aggregationPrice))}',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Shipping Address: ' +
                                                  feed.shippingAddress,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            FittedBox(
                                              child: Text(
                                                'Delivery date: ' +
                                                    feed.deliveryDate,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 15.0,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Shipping cost: ' +
                                                  feed.shippingCost,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Quantity purchased: ' +
                                                  feed.quantityPurchased,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            FittedBox(
                                              child: Text(
                                                'Notes: ' + feed.notes,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 15.0,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Status: ' + feed.status,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            SizedBox(height: 5,),
                                            Text(
                                              'Created By: ' + feed.createdBy,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        )),
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Column(
                        children: <Widget>[
                          SizedBox(
                            height: 100,
                          ),
                          Text(
                            'Network error',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Column(
                            children: <Widget>[
                              Text("${'Network error'}"),
                              Text("${'Network error'}"),
                            ],
                          ),
                          SizedBox(
                            height: 100,
                          ),
                        ],
                      );
                    } else
                      return Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 250,
                            ),
                            Text(
                              'Network error',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Column(
                              children: <Widget>[
                                Text("${'Network error'}"),
                                Text("${'Network error'}"),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      );
                  }),
            ),
          ]),
        ],
      ),
    ]));
  }
}
