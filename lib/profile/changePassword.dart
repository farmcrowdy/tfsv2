import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/const.dart';
import 'package:http/http.dart' as http;

class ChangePassword extends StatefulWidget {
  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<ChangePassword> {
  TextEditingController passwordController = TextEditingController();
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  Map<String, dynamic> responseData;
  var locations = List();
  String message;
  var data;
  var token;
  bool passwordVisible;
  bool passwordVisible2;
  String name;
  String id;
  String status;
  String approved;
  String accessLevel;
  var body;

  // Alert dialog
  _showErrorDialog({String message = ""}) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error Resetting pin"),
            content: Text(message),
            shape:
            RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15)),
            actions: <Widget>[
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }
  //Http call
  reset() async {
    body = {
      "sourcePhone": phoneNo,
      "pin": oldPasswordController.text.trim(),
      "newPin": newPasswordController.text.trim(),
    };
    print(body);
    final response = await http.post(baseUrl + 'ResetPIN',
        headers: {'api-key': apiKey},
        body: body);
    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
//      status = responseData["status"];
//      message = responseData["message"];
//      approved = responseData['approved'];
//      accessLevel = responseData['access_level'];
//      id = responseData['client_id'];
//      email = responseData['data']['user']['email'];
//      await prefs.setString('name', name);
//      await prefs.setString('phoneNo', phoneNo);
//      await prefs.setString('userId', '$id');
//      await prefs.setString('email', '$email');
      // print('token $token and $name');
      print(responseData);
      print(body);
      print(status);
      setState(() {
        _isLoading = false;
      });
    } else {
      setState(() {
        _isLoading = false;
      });
      responseData = json.decode(response.body);
     // var er = responseData['message'];
      print('error$responseData');
      _showErrorDialog(message: 'Error! couldn\'t change your password');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    passwordVisible = true;
    passwordVisible2 = true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    return
      Scaffold(

          body:
        Form(
        key: _formKey,
        autovalidate: false,
        child: Container(
        padding: EdgeInsets.all(20),
    child:
                  ListView(

                      children: <Widget>[
                        SizedBox(height: 10,),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [

                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Icon(Icons.arrow_back, size: 40,),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 30,),

                        Text('Password', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                        SizedBox(height: 20,),
                        Text('Change password', style: TextStyle(color: greyish, fontSize: 20,fontWeight: FontWeight.w400),),
                        SizedBox(height: 30,),

                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'CURRENT PASSWORD',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                              TextFormField(
                                controller: oldPasswordController,
                                validator: (value) {
                                  if (value.isEmpty ) {
                                    return "Invalid Password";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      // Based on passwordVisible state choose the icon
                                      passwordVisible
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: Colors.black,
                                    ),
                                    onPressed: () {
                                      // Update the state i.e. toogle the state of passwordVisible variable
                                      setState(() {
                                        passwordVisible = !passwordVisible;
                                      });
                                    },
                                  ),
                                  hintText: '******',
                                  hintStyle: TextStyle(
                                    color: Colors.black45,
                                  ),
                                  labelStyle: TextStyle(color: Colors.blue),
                                ),
                                obscureText: passwordVisible,
                                keyboardType: TextInputType.visiblePassword,
                                style: TextStyle(color: Colors.black),
                                cursorColor: Colors.black,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'NEW PASSWORD',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                              TextFormField(
                                controller: passwordController,
                                validator: (value) {
                                  if (value.isEmpty ) {
                                    return "Invalid Password";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      // Based on passwordVisible state choose the icon
                                      passwordVisible
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: Colors.black,
                                    ),
                                    onPressed: () {
                                      // Update the state i.e. toogle the state of passwordVisible variable
                                      setState(() {
                                        passwordVisible = !passwordVisible;
                                      });
                                    },
                                  ),

                                  hintText: '******',
                                  hintStyle: TextStyle(
                                    color: Colors.black45,
                                  ),
                                  labelStyle: TextStyle(color: Colors.blue),
                                ),
                                obscureText: passwordVisible,
                                keyboardType: TextInputType.visiblePassword,
                                style: TextStyle(color: Colors.black),
                                cursorColor: Colors.black,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'CONFIRM PASSWORD',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                              TextFormField(
                                controller: newPasswordController,
                                validator: (value) {
                                  if (value.isEmpty || passwordController.text != newPasswordController.text ) {
                                    return "Invalid Password";
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      // Based on passwordVisible state choose the icon
                                      passwordVisible
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: Colors.black,
                                    ),
                                    onPressed: () {
                                      // Update the state i.e. toogle the state of passwordVisible variable
                                      setState(() {
                                        passwordVisible = !passwordVisible;
                                      });
                                    },
                                  ),

                                  hintText: '******',
                                  hintStyle: TextStyle(
                                    color: Colors.black45,
                                  ),
                                  labelStyle: TextStyle(color: Colors.blue),
                                ),
                                obscureText: passwordVisible,
                                keyboardType: TextInputType.visiblePassword,
                                style: TextStyle(color: Colors.black),
                                cursorColor: Colors.black,
                              ),
                            ],
                          ),
                        ),

                        SizedBox(height: 100,),
                            Container(
                                padding: EdgeInsets.all(10),
                                height: 80,
                                width: deviceWidth,
                                child:
                                RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius
                                        .circular(25.0),
                                    //side: BorderSide(color: Colors.red)
                                  ),
                                  color: appTheme,
                                  onPressed: () async{
                                    if (_formKey.currentState.validate()) {
                                      setState(() {
                                        _isLoading = true;
                                      });
                                      await     reset();
                                    }
                                  },
                                  child: _isLoading
                                      ? Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: CircularProgressIndicator(
                                      backgroundColor: Colors.white,
                                    ),
                                  )
                                      :  Text('Update Password', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                                )
                            ),
                          ],
                        )
                )
        ),
      );
  }
}