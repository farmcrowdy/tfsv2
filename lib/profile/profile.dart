import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:grainpointapp/const.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Profile extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<Profile> {
  TextEditingController groupNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: false);

    return Scaffold(
      body:
      Form(
          key: _formKey,
          autovalidate: false,
          child:
          Container(
              padding: EdgeInsets.all(20),
              child:
              ListView(
                  children: <Widget>[
                    SizedBox(height: 10,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [

                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(Icons.arrow_back, size: 40,),
                          )
                        ],
                      ),
                    ),

                    SizedBox(height: 30,),

                    Text('Profile', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                    Container(
                        padding: EdgeInsets.all(10),
                        height: 90,
                        width: deviceWidth,
                        child:
                        FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius
                                  .circular(25.0),
                              //side: BorderSide(color: Colors.red)
                            ),
                            color: Colors.grey[200],
                            onPressed: (){
                               Navigator.pushNamed(context, '/UserProfile');
                            },
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                    children: [
                                      Icon(
                                          Icons.perm_identity
                                      ),
                                      SizedBox(width: 30,),
                                      Text('Profile Details', style: TextStyle(fontSize:    ScreenUtil().setSp(25), fontWeight: FontWeight.w500, color: Colors.black),),
                                    ]
                                ),

                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    color: appTheme,
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),

                                  child:Icon(
                                    Icons.arrow_forward_ios, color: Colors.white,
                                  ) ,
                                )

                              ],
                            )


                        )
                    ),
                    Container(
                        padding: EdgeInsets.all(10),
                        height: 90,
                        width: deviceWidth,
                        child:
                        FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius
                                  .circular(25.0),
                              //side: BorderSide(color: Colors.red)
                            ),
                            color: Colors.grey[200],
                            onPressed: (){
                               Navigator.pushNamed(context, '/ChangePassword');
                            },
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                    children: [
                                      Icon(
                                          Icons.perm_identity
                                      ),
                                      SizedBox(width: 30,),
                                      Text('Change Password', style: TextStyle(fontSize:    ScreenUtil().setSp(25), fontWeight: FontWeight.w500, color: Colors.black),),
                                    ]
                                ),

                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    color: appTheme,
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),

                                  child:Icon(
                                    Icons.arrow_forward_ios, color: Colors.white,
                                  ) ,
                                )

                              ],
                            )


                        )
                    ),

SizedBox(height: 200,),
                    Container(
                        padding: EdgeInsets.all(10),
                        height: 80,
                        width: deviceWidth,
                        child:
                        RaisedButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius
                                .circular(25.0),
                            //side: BorderSide(color: Colors.red)
                          ),
                          color: appTheme,
                          onPressed: () async{
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            prefs.remove('id');
                            prefs.remove('phoneNo');
                            Navigator.pushReplacementNamed(context, '/Login');
                          },
                          child: Text('Logout', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                        )
                    ),

                  ]
              )
          )
      ),
    );
  }
}
