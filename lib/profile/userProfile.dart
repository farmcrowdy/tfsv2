import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:grainpointapp/Engine/httpDetails.dart';
import 'package:grainpointapp/Home/home.dart';
import 'package:grainpointapp/const.dart';
import 'package:http/http.dart' as http;

class UserProfile extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<UserProfile> {
  TextEditingController groupNameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
String firstName;
  String lastName;
  String email;
  String phoneNo;
  String clientId;
  String gender;
  Map<String, dynamic> responseData;
  String message;
  var data;
  var token;
  String name;
  String status;
  String approved;
  String accessLevel;
  var body;
  String quantity = '1';
  int total;
  String cropId;
  var tx_code;

  // This fetches the user profile

  _confirmClientProfile() async {


    final response = await http.get(
      baseUrl + 'user/$id',
      headers: {'api-key': 'aa47f8215c6f30a0dcdb2a36a9f4168e'},
    );

    if (response.statusCode == 200) {
      responseData = json.decode(response.body);
      setState(() {
        print(responseData);
        firstName = (responseData['message']['firstname']);
        lastName = (responseData['message']['surname']);
        email = (responseData['message']['email']);
        phoneNo = (responseData['message']['uphone']);
        clientId = (responseData['message']['client_id']);
        gender = (responseData['message']['gender']);
      });
    } else {
      var sa = response.body;
      responseData = json.decode(response.body);
      var er = responseData['message'];
      Fluttertoast.showToast(msg: er);
      Navigator.pop(context);
      print('error$er');
      print(sa);
      throw Exception('Failed to load internet');
    }
  }

  @override
  void initState() {
    super.initState();
    _confirmClientProfile();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334, allowFontScaling: false);


    if (firstName == null) {
      return SpinKitChasingDots(
        size: 50,
        color: appTheme,
      );
    }
    return Scaffold(
      body: Form(
          key: _formKey,
          autovalidate: false,
          child: Container(
              padding: EdgeInsets.all(20),
              child: ListView(children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Icon(
                          Icons.arrow_back,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'My profile',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left:ScreenUtil().setWidth(40), right: ScreenUtil().setWidth(40)),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'First Name',
                              style: TextStyle(
                                color: greyish,
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                              ),
                            ),
                            Text(
                              '$firstName',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Last Name',
                              style: TextStyle(
                                color: greyish,
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                              ),
                            ),
                            Text(
                              '$lastName',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left:ScreenUtil().setWidth(40), right: ScreenUtil().setWidth(40)),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Email',
                              style: TextStyle(
                                color: greyish,
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                              ),
                            ),
                            Text(
                              '$email',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Phone Number',
                              style: TextStyle(
                                color: greyish,
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                              ),
                            ),
                            Text(
                              '$phoneNo',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left:ScreenUtil().setWidth(40), right: ScreenUtil().setWidth(40)),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Client Id',
                              style: TextStyle(
                                color: greyish,
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                              ),
                            ),
                            Text(
                              '$clientId',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Gender',
                              style: TextStyle(
                                color: greyish,
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                              ),
                            ),
                            Text(
                              '$gender',
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),

              ]))),
    );
  }
}
